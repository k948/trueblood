\babel@toc {croatian}{}\relax 
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{6}{chapter.2}%
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{13}{chapter.3}%
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{13}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{16}{subsection.3.1.1}%
\contentsline {subsubsection}{Opis obrazaca uporabe}{16}{subsubsection*.2}%
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{24}{subsubsection*.3}%
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{28}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{36}{section.3.2}%
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{37}{chapter.4}%
\contentsline {section}{\numberline {4.1}Baza podataka}{39}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{40}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{44}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Dijagram razreda}{45}{section.4.2}%
\contentsline {section}{\numberline {4.3}Dijagram stanja}{51}{section.4.3}%
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{52}{section.4.4}%
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{54}{section.4.5}%
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{55}{chapter.5}%
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{55}{section.5.1}%
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{57}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{57}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Ispitivanje sustava}{64}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{72}{section.5.3}%
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{73}{section.5.4}%
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{76}{chapter.6}%
\contentsline {chapter}{Popis literature}{78}{chapter*.4}%
\contentsline {chapter}{Indeks slika i dijagrama}{80}{chapter*.5}%
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{81}{chapter*.6}%
