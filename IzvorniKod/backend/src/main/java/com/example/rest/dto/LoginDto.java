package com.example.rest.dto;

public class LoginDto {

    private Long username;
    private String password;

    public LoginDto(Long username, String password) {
        this.username = username;
        this.password = password;
    }

    public Long getUsername() {
        return username;
    }

    public void setUsername(Long username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
