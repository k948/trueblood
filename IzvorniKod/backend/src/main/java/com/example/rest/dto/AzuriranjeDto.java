package com.example.rest.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.sql.Date;

public class AzuriranjeDto {

    //bla
    private String lozinka;


    @NotNull(message = "Id je obavezan")
    private Long id;

    @NotBlank(message = "Ime je obavezno")
    private String ime;
    @NotBlank(message = "Prezime je obavezno")
    private String prezime;
    @Pattern(regexp = "^\\d{11}$", message = "Oib mora sadržavati 11 znamenki")
    private String oib;
    @NotBlank(message = "Spol je obavezan")
    private String spol;
    @NotBlank(message = "Email je obavezan")
    @Email(message = "Neispravan format email adrese")
    private String email;

    private String mjestoRodenja;
    private String mjestoZaposlenja;
    private String adresaStanovanja;

    private String telefonPrivatni;
    private String telefonPoslovni;

    //@NotNull(message = "Datum rođenja je obavezan")
    private Date datumRodenja;


    public AzuriranjeDto(@NotNull(message = "Lozinka je obavezna") String lozinka, @NotNull(message = "Id je obavezan") Long id, @NotBlank(message = "Ime je obavezno") String ime, @NotBlank(message = "Prezime je obavezno") String prezime, @Pattern(regexp = "^\\d{11}$", message = "Oib mora sadržavati 11 znamenki") String oib, @NotBlank(message = "Spol je obavezan") String spol, @NotBlank(message = "Email je obavezan") @Email(message = "Neispravan format email adrese") String email, String mjestoRodenja, String mjestoZaposlenja, String adresaStanovanja, String telefonPrivatni, String telefonPoslovni, @NotNull(message = "Datum rođenja je obavezan") Date datumRodenja) {
        this.lozinka = lozinka;
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.oib = oib;
        this.spol = spol;
        this.email = email;
        this.mjestoRodenja = mjestoRodenja;
        this.mjestoZaposlenja = mjestoZaposlenja;
        this.adresaStanovanja = adresaStanovanja;
        this.telefonPrivatni = telefonPrivatni;
        this.telefonPoslovni = telefonPoslovni;
        this.datumRodenja = datumRodenja;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMjestoRodenja() {
        return mjestoRodenja;
    }

    public void setMjestoRodenja(String mjestoRodenja) {
        this.mjestoRodenja = mjestoRodenja;
    }

    public String getMjestoZaposlenja() {
        return mjestoZaposlenja;
    }

    public void setMjestoZaposlenja(String mjestoZaposlenja) {
        this.mjestoZaposlenja = mjestoZaposlenja;
    }

    public String getAdresaStanovanja() {
        return adresaStanovanja;
    }

    public void setAdresaStanovanja(String adresaStanovanja) {
        this.adresaStanovanja = adresaStanovanja;
    }

    public String getTelefonPrivatni() {
        return telefonPrivatni;
    }

    public void setTelefonPrivatni(String telefonPrivatni) {
        this.telefonPrivatni = telefonPrivatni;
    }

    public String getTelefonPoslovni() {
        return telefonPoslovni;
    }

    public void setTelefonPoslovni(String telefonPoslovni) {
        this.telefonPoslovni = telefonPoslovni;
    }

    public Date getDatumRodenja() {
        return datumRodenja;
    }

    public void setDatumRodenja(Date datumRodenja) {
        this.datumRodenja = datumRodenja;
    }
}
