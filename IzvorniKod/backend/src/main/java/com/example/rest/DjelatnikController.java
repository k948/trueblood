package com.example.rest;


import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.rest.dto.DjelatnikRegisterDonorDto;
import com.example.rest.dto.DonacijaPodatciDto;
import com.example.rest.dto.DonacijaProvedenaDto;
import com.example.rest.dto.PotrosnjaDto;
import com.example.security.UserDetail;
import com.example.service.DarivanjeService;
import com.example.service.DonorService;
import com.example.service.KrvnaGrupaService;
import com.example.service.PotrosnjaService;
import com.example.service.impl.RegistrationService;
import com.github.lambdaexpression.annotation.RequestBodyParam;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/djelatnik")
public class DjelatnikController { //myb napravit djelatnikSerice i izbacit ove autowirese

    @Autowired
    DarivanjeService darivanjeService;

    @Autowired
    RegistrationService registrationService;

    @Autowired
    PotrosnjaService potrosnjaService;




    @PostMapping("donacija")
    public ResponseEntity<Object> provjeriId(@RequestBodyParam String donorId) {

        Donor donor = darivanjeService.checkDonor(Long.parseLong(donorId));
        return new ResponseEntity<>(donor, HttpStatus.OK);
    }


    @PostMapping("donacija-podatci")
    public ResponseEntity<Object>  provjeriPodatke(@Valid @RequestBody DonacijaPodatciDto donacijaPodatciDto, BindingResult bindingResult){

        Utils.handleValidation(bindingResult);
        //provjerava sve uvjete i baca greške
       double trenutnaZaliha = darivanjeService.handleDonacijaForm(donacijaPodatciDto);
       String ret = "Trenutna zaliha donorove krvne grupe je "+ trenutnaZaliha + "dcl. Maksimalni kapacitet za pohranu je 100dcl.";

        Map<String, String> props = new HashMap<>();
        props.put("message", ret);
        return new ResponseEntity<>(props, HttpStatus.OK);

    }

    @PostMapping("provedena-donacija")
    public ResponseEntity<Object>  zabiljeziDonaciju(@Valid @RequestBody DonacijaProvedenaDto donacijaProvedenaDto, BindingResult bindingResult){

        Utils.handleValidation(bindingResult);
        Korisnik djelatnik =  ((UserDetail)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getKorisnik();
        darivanjeService.saveSucceded(djelatnik, donacijaProvedenaDto.getDonorId(),
                donacijaProvedenaDto.getLokacija(), donacijaProvedenaDto.getKolicina());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("register-donor")
    public ResponseEntity<Object>  registrirajDonora(@Valid @RequestBody DjelatnikRegisterDonorDto djelatnikRegisterDonorDto, BindingResult bindingResult){

        Utils.handleValidation(bindingResult);
        Donor donor = Utils.donorDtoToDonor(djelatnikRegisterDonorDto.getDonorDto());
        registrationService.djelatnikRegisterDonor(donor,
                djelatnikRegisterDonorDto.getTrajniZdravstveniPodatciDto(),
                djelatnikRegisterDonorDto.getOznakaKgrupe());

        return new ResponseEntity<>(HttpStatus.OK);
  }

    @PostMapping("transfer-blood")
    public ResponseEntity<Object>  prebaciKrv(@Valid @RequestBody PotrosnjaDto potrosnjaDto, BindingResult bindingResult){

        Utils.handleValidation(bindingResult);
        Korisnik djelatnik =  ((UserDetail)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getKorisnik();
        potrosnjaService.zabiljeziPotrosnju(djelatnik, potrosnjaDto.getOznakaKgrupe(), potrosnjaDto.getKolicina());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("")
    public String getUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
