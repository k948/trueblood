package com.example.rest.dto;

import com.example.rest.TrueBloodException;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DonacijaProvedenaDto {
    @NotNull(message = "Id je obavezan")
    private Long donorId;

    @NotBlank(message = "Opis lokacije je obavezan")
    private String lokacija;

    @NotNull(message = "Navedite volumen donirane krvi")
    private Double kolicina;


    public Long getDonorId() {
        return donorId;
    }

    public void setDonorId(String donorId) {
        this.donorId = Long.parseLong(donorId);
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public Double getKolicina() {
        return kolicina;
    }

    public void setKolicina(String kolicina) {
        try {
            this.kolicina = Double.parseDouble(kolicina);
            if(this.kolicina < 0)
                throw new Exception();
        }
        catch(Exception e){
            throw new TrueBloodException("Količina donirane krvi mora biti pozitivnan broj.", HttpStatus.BAD_REQUEST);
        }
    }
}
