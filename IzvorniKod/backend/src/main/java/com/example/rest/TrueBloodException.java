package com.example.rest;

import org.springframework.http.HttpStatus;

public class TrueBloodException extends RuntimeException{
    private HttpStatus status;

    public TrueBloodException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
