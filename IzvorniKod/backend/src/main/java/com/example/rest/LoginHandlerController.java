package com.example.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/postlog")
public class LoginHandlerController {

    @RequestMapping(value = "/fail", method = RequestMethod.GET)
    public ResponseEntity<Object> onFailureLogin() {
        throw new TrueBloodException("Neispravno korisničko ime ili lozinka!", HttpStatus.BAD_REQUEST);
        //return new ResponseEntity<>("Neispravno korisničko ime ili lozinka!", HttpStatus.BAD_REQUEST);
    }
}
