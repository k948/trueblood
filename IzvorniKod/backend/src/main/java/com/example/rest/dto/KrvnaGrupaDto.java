package com.example.rest.dto;

public class KrvnaGrupaDto {

    private String oznakaKgrupe;
    private Double zaliha;
    private Double donjaGranica;
    private Double gornjaGranica;

    public KrvnaGrupaDto(String oznakaKgrupe, Double zaliha, Double donjaGranica, Double gornjaGranica) {
        this.oznakaKgrupe = oznakaKgrupe;
        this.zaliha = zaliha;
        this.donjaGranica = donjaGranica;
        this.gornjaGranica = gornjaGranica;
    }

    public KrvnaGrupaDto() {

    }

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }

    public Double getZaliha() {
        return zaliha;
    }

    public void setZaliha(Double zaliha) {
        this.zaliha = zaliha;
    }

    public Double getDonjaGranica() {
        return donjaGranica;
    }

    public void setDonjaGranica(Double donjaGranica) {
        this.donjaGranica = donjaGranica;
    }

    public Double getGornjaGranica() {
        return gornjaGranica;
    }

    public void setGornjaGranica(Double gornjaGranica) {
        this.gornjaGranica = gornjaGranica;
    }
}
