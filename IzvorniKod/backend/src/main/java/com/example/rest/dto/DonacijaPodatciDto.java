package com.example.rest.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class DonacijaPodatciDto {

    @Valid
    private DonacijaDonorDto donacijaDonorDto;

    private TrenutniZdravstveniPodatciDto trenutniZdravstveniPodatciDto;
    private TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto;

    @NotBlank(message = "Oznaka krvne grupe je obavezna")
    private String oznakaKgrupe;
    @NotBlank(message = "Opis lokacije je obavezan")
    private String lokacija;

    public DonacijaDonorDto getDonacijaDonorDto() {
        return donacijaDonorDto;
    }

    public void setDonacijaDonorDto(DonacijaDonorDto donacijaDonorDto) {
        this.donacijaDonorDto = donacijaDonorDto;
    }

    public TrenutniZdravstveniPodatciDto getTrenutniZdravstveniPodatciDto() {
        return trenutniZdravstveniPodatciDto;
    }

    public void setTrenutniZdravstveniPodatciDto(TrenutniZdravstveniPodatciDto trenutniZdravstveniPodatciDto) {
        this.trenutniZdravstveniPodatciDto = trenutniZdravstveniPodatciDto;
    }

    public TrajniZdravstveniPodatciDto getTrajniZdravstveniPodatciDto() {
        return trajniZdravstveniPodatciDto;
    }

    public void setTrajniZdravstveniPodatciDto(TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto) {
        this.trajniZdravstveniPodatciDto = trajniZdravstveniPodatciDto;
    }

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }
}

