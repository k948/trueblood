package com.example.rest.dto;

public class TrajniZdravstveniPodatciDto {

    //Osoba je bolovale ili sada boluju od teških kroničnih bolesti dišnog i probavnog sustava
    private String kronicneBolesti;
    //Osoba boluje od bolesti srca i krvnih žila, zloćudnih bolesti, bolesti jetre, AIDS-a, šećerne bolesti (osobe liječene inzulinskom terapijom), živčanih i duševnih bolesti
    private String izabraneTeskeBolesti;
    //Osoba je ovisnik o alkoholu ili drogama
    private String ovisnost;
    //Muškarci koji su u životu imali spolne odnose s drugim muškarcima
    private String ModnosM;
    //Žene i muškarci koji su imali spolni odnos s prostitutkama
    private String odnosProstucija;
    //Osobe koje često mijenjaju seksualne partnere (promiskuitetne osobe)
    private String promiskuitet;
    //Osobe koje su uzimale drogu intravenskim putem
    private String drogaIntravenski;
    //Osobe koje su liječene zbog spolno prenosivih bolesti (sifilis, gonoreja)
    private String spolnoPrenosiveBolesti;
    //Osobe koje su HIV-pozitivne
    private String HivPozitivan;
    //Seksualni partneri gore navedenih osoba
    private String seksPartnnerRizSkupine;

    public String getKronicneBolesti() {
        return kronicneBolesti;
    }

    public void setKronicneBolesti(String kronicneBolesti) {
        this.kronicneBolesti = kronicneBolesti;
    }

    public String getIzabraneTeskeBolesti() {
        return izabraneTeskeBolesti;
    }

    public void setIzabraneTeskeBolesti(String izabraneTeskeBolesti) {
        this.izabraneTeskeBolesti = izabraneTeskeBolesti;
    }

    public String getOvisnost() {
        return ovisnost;
    }

    public void setOvisnost(String ovisnost) {
        this.ovisnost = ovisnost;
    }

    public String getModnosM() {
        return ModnosM;
    }

    public void setModnosM(String modnosM) {
        ModnosM = modnosM;
    }

    public String getOdnosProstucija() {
        return odnosProstucija;
    }

    public void setOdnosProstucija(String odnosProstucija) {
        this.odnosProstucija = odnosProstucija;
    }

    public String getPromiskuitet() {
        return promiskuitet;
    }

    public void setPromiskuitet(String promiskuitet) {
        this.promiskuitet = promiskuitet;
    }

    public String getDrogaIntravenski() {
        return drogaIntravenski;
    }

    public void setDrogaIntravenski(String drogaIntravenski) {
        this.drogaIntravenski = drogaIntravenski;
    }

    public String getSpolnoPrenosiveBolesti() {
        return spolnoPrenosiveBolesti;
    }

    public void setSpolnoPrenosiveBolesti(String spolnoPrenosiveBolesti) {
        this.spolnoPrenosiveBolesti = spolnoPrenosiveBolesti;
    }

    public String getHivPozitivan() {
        return HivPozitivan;
    }

    public void setHivPozitivan(String hivPozitivan) {
        HivPozitivan = hivPozitivan;
    }

    public String getSeksPartnnerRizSkupine() {
        return seksPartnnerRizSkupine;
    }

    public void setSeksPartnnerRizSkupine(String seksPartnnerRizSkupine) {
        this.seksPartnnerRizSkupine = seksPartnnerRizSkupine;
    }

}
