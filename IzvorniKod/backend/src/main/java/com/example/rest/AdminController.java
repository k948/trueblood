package com.example.rest;

import com.example.domain.Korisnik;
import com.example.domain.enums.Role;
import com.example.rest.dto.DjelatnikDto;
import com.example.rest.dto.DonorDto;
import com.example.rest.dto.KrvnaGrupaGraniceDto;
import com.example.security.UserDetail;
import com.example.service.KorisnikService;
import com.example.service.KrvnaGrupaService;
import com.example.service.impl.RegistrationService;
import com.github.lambdaexpression.annotation.RequestBodyParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    RegistrationService registrationService;

    @Autowired
    KrvnaGrupaService krvnaGrupaService;



    @RequestMapping(value = "register-djelatnik", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> registerDjelatnik(@Valid @RequestBody DjelatnikDto djelatnikDto, BindingResult bindingResult) {

        Utils.handleValidation(bindingResult);

        registrationService.registerDjelatnik(Utils.djelatnikDtoToDjelatnik(djelatnikDto));
        return new ResponseEntity<>("Check your mail!", HttpStatus.OK);
    }

    @GetMapping("")
    public String getUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }


    @GetMapping("/accounts")
    public ResponseEntity<Object> getAllActiveAccounts() {
        List<Korisnik> korisnici =  korisnikService.findAllAccounts();
        korisnici = korisnici.stream().filter(k -> k.isEnabled()).toList();
        return new ResponseEntity<>(korisnici, HttpStatus.OK);
    }

    @PostMapping("/deactivate-account") // x-url-encoded...
    public ResponseEntity<Object> deactivateAccount(@RequestBodyParam String korisnikId) {
        System.out.println(korisnikId);
        Korisnik korisnik = korisnikService.findById(Long.parseLong(korisnikId));

        if(korisnik.getRole() == Role.ADMIN){
            throw new TrueBloodException("Ne možete deaktivirati račun administratora!", HttpStatus.NOT_ACCEPTABLE);
        }

        korisnik.setEnabled(false);
        korisnikService.save(korisnik);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/update-boundary") // x-url-encoded...
    public ResponseEntity<Object> updateBoundary(@RequestBody KrvnaGrupaGraniceDto krvnaGrupaGraniceDto, BindingResult bindingResult) {

        Utils.handleValidation(bindingResult);
        krvnaGrupaService.updateBloodBoundary(krvnaGrupaGraniceDto.getOznakaKgrupe(),
                krvnaGrupaGraniceDto.getDonjaGranica(), krvnaGrupaGraniceDto.getGornjaGranica());

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
