package com.example.rest.dto;

public class TrenutniZdravstveniPodatciDto {

    //Tjelesna temperatura iznad 37°C
    private String tezina;
    //Tjelesna temperatura iznad 37°C
    private String temperatura;
    //Krvni tlak: sistolični ispod 100 ili iznad 180 mm Hg, dijastolični ispod 60 ili iznad 110 mm Hg
    private String tlak;
    //Puls: ispod 50 ili iznad 100 otkucaja u minuti
    private String puls;
    //Hemoglobin: muškarci ispod 135 g/L, žene ispod 125 g/L
    private String hemoglobin;
    //Osoba trenutno uzima antibiotike ili druge lijekove
    private String uzimaLijekove;
    //Osoba je konzumirale alkoholna pića unutar 8 sati prije darivanja krvi
    private String konzumacijaAlkohola;
    //Osoba s lakšim akutnim bolesnim stanjima (prehlade, poremetnje probavnog sustava, smanjenog željeza u krvi i sl.)
    private String bolesnoStanje;
    //Žene za vrijeme menstruacije, trudnoće i dojenja
    private String mensTrudDoj;
    //Osoba tog dana obavlja opasne poslove (rad na visini, dubini)
    private String opasniPoslovi;

    public String getTezina() {
        return tezina;
    }

    public void setTezina(String tezina) {
        this.tezina = tezina;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getTlak() {
        return tlak;
    }

    public void setTlak(String tlak) {
        this.tlak = tlak;
    }

    public String getPuls() {
        return puls;
    }

    public void setPuls(String puls) {
        this.puls = puls;
    }

    public String getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(String hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public String getUzimaLijekove() {
        return uzimaLijekove;
    }

    public void setUzimaLijekove(String uzimaLijekove) {
        this.uzimaLijekove = uzimaLijekove;
    }

    public String getKonzumacijaAlkohola() {
        return konzumacijaAlkohola;
    }

    public void setKonzumacijaAlkohola(String konzumacijaAlkohola) {
        this.konzumacijaAlkohola = konzumacijaAlkohola;
    }

    public String getBolesnoStanje() {
        return bolesnoStanje;
    }

    public void setBolesnoStanje(String bolesnoStanje) {
        this.bolesnoStanje = bolesnoStanje;
    }

    public String getMensTrudDoj() {
        return mensTrudDoj;
    }

    public void setMensTrudDoj(String mensTrudDoj) {
        this.mensTrudDoj = mensTrudDoj;
    }

    public String getOpasniPoslovi() {
        return opasniPoslovi;
    }

    public void setOpasniPoslovi(String opasniPoslovi) {
        this.opasniPoslovi = opasniPoslovi;
    }
}
