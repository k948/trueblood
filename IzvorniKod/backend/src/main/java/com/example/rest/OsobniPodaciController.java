package com.example.rest;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.enums.Role;
import com.example.rest.dto.AzuriranjeDto;
import com.example.service.KorisnikService;
import com.example.service.OsobniPodaciService;
import com.github.lambdaexpression.annotation.RequestBodyParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/osobni-podaci")

public class OsobniPodaciController {


    @Autowired
    KorisnikService korisnikService;

    @Autowired
    OsobniPodaciService osobniPodaciService;

    @GetMapping("pregled/{Id}")
    public ResponseEntity<Object> pregledOsobnihPodataka(@PathVariable String Id) {

        boolean check = osobniPodaciService.checkIfGivenAndLoggedInAreEqual(Id);

        Korisnik adminORdjelatnikORdonor = korisnikService.findById(Long.parseLong(Id));

        return new ResponseEntity<>(adminORdjelatnikORdonor, HttpStatus.OK);

    }


    @PostMapping("azuriraj")
    public ResponseEntity<Object> azuriranjeOsobnihPodataka(@Valid @RequestBody AzuriranjeDto azuriranjeDto, BindingResult bindingResult) {
        Utils.handleValidation(bindingResult);

        String Id = azuriranjeDto.getId().toString();
        boolean check = osobniPodaciService.checkIfGivenAndLoggedInAreEqual(Id);

        Korisnik azuriranKorisnik =  osobniPodaciService.updateKorisnik(azuriranjeDto);





        return new ResponseEntity<>(azuriranKorisnik, HttpStatus.OK);

    }



}
