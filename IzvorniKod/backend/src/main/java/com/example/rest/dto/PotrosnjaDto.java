package com.example.rest.dto;

import com.example.rest.TrueBloodException;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PotrosnjaDto {

    @NotBlank(message = "Navedite oznaku transferirane krvne grupe")
    private String oznakaKgrupe;
    @NotNull(message = "Navedite volumen donirane krvi")
    private Double kolicina;

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }

    public Double getKolicina() {
        return kolicina;
    }

    public void setKolicina(Double kolicina) {
        if(kolicina < 0){
            throw new TrueBloodException("Količina poslane krvi mora biti pozitivna.", HttpStatus.BAD_REQUEST);
        }
        this.kolicina = kolicina;
    }
}
