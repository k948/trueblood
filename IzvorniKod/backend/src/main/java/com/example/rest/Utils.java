package com.example.rest;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.domain.enums.Role;
import com.example.rest.dto.*;
import com.example.service.KrvnaGrupaService;
import com.example.service.RegistrationService;
import com.example.service.impl.RegistrationServiceJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    private static RegistrationServiceJpa registrationService;

    public static KrvnaGrupa krvnaGrupaDtoToKrvnaGrupa(KrvnaGrupaDto krvnaGrupaDto) {
        KrvnaGrupa krvnaGrupa = new KrvnaGrupa(krvnaGrupaDto.getOznakaKgrupe(), krvnaGrupaDto.getZaliha(), krvnaGrupaDto.getDonjaGranica(), krvnaGrupaDto.getGornjaGranica());
        return krvnaGrupa;
    }

    public static Donor donorDtoToDonor(DonorDto donorDto){
        Donor donor = new Donor(donorDto.getOib(),
                donorDto.getSpol(),donorDto.getEmail(),
                donorDto.getIme(),donorDto.getPrezime(), false);
        donor.setEnabled(false);
        donor.setAdresaStanovanja(donorDto.getAdresaStanovanja());
        donor.setMjestoRodenja(donorDto.getMjestoRodenja());
        donor.setPotpunoRegistriran(false);
        donor.setTelefonPoslovni(donorDto.getTelefonPoslovni());
        donor.setTelefonPrivatni(donorDto.getTelefonPrivatni());
        donor.setDatumRodenja(donorDto.getDatumRodenja());
        donor.setRole(Role.DONOR);
        donor.setMjestoZaposlenja(donorDto.getMjestoZaposlenja());
        return donor;
    }

//    public static Donor donorDtoToFullDonor(DonorDto donorDto) {
//        Donor donor = new Donor(donorDto.getOib(),
//                donorDto.getSpol(),donorDto.getEmail(),
//                donorDto.getIme(),donorDto.getPrezime(), false);
//
//        donor.setEnabled(false);
//        donor.setDatumRodenja(donorDto.getDatumRodenja());
//        donor.setMjestoRodenja(donorDto.getMjestoRodenja());
//        donor.setMjestoZaposlenja(donorDto.getMjestoZaposlenja());
//        donor.setAdresaStanovanja(donorDto.getAdresaStanovanja());
//        donor.setTelefonPrivatni(donorDto.getTelefonPrivatni());
//        donor.setTelefonPoslovni(donorDto.getTelefonPoslovni());
//
//        donor.setKronicneBolesti(donorDto.getTrajniZdravstveniPodatciDto().getKronicneBolesti());
//        donor.setIzabraneTeskeBolesti(donorDto.getTrajniZdravstveniPodatciDto().getIzabraneTeskeBolesti());
//        donor.setOvisnost(donorDto.getTrajniZdravstveniPodatciDto().getOvisnost());
//        donor.setModnosM(donorDto.getTrajniZdravstveniPodatciDto().getModnosM());
//        donor.setOdnosProstucija(donorDto.getTrajniZdravstveniPodatciDto().getOdnosProstucija());
//        donor.setPromiskuitet(donorDto.getTrajniZdravstveniPodatciDto().getPromiskuitet());
//        donor.setDrogaIntravenski(donorDto.getTrajniZdravstveniPodatciDto().getDrogaIntravenski());
//        donor.setSpolnoPrenosiveBolesti(donorDto.getTrajniZdravstveniPodatciDto().getSpolnoPrenosiveBolesti());
//        donor.setHivPozitivan(donorDto.getTrajniZdravstveniPodatciDto().getHivPozitivan());
//        donor.setSeksPartnnerRizSkupine(donorDto.getTrajniZdravstveniPodatciDto().getSeksPartnnerRizSkupine());
//
//        donor.setKrvnaGrupa(krvnaGrupaDtoToKrvnaGrupa(donorDto.getKrvnaGrupaDto()));
//
//        donor.setRole(Role.DONOR);
//        System.out.println("IM HERE");
//        try {
//            if(!donor.getKronicneBolesti().equals("NE") ||
//                    !donor.getDrogaIntravenski().equals("NE") ||
//                    !donor.getIzabraneTeskeBolesti().equals("NE") ||
//                    !donor.getOvisnost().equals("NE")||
//                    !donor.getModnosM().equals("NE") ||
//                    !donor.getOdnosProstucija().equals("NE") ||
//                    !donor.getPromiskuitet().equals("NE") ||
//                    !donor.getSpolnoPrenosiveBolesti().equals("NE") ||
//                    !donor.getHivPozitivan().equals("NE") ||
//                    !donor.getSeksPartnnerRizSkupine().equals("NE")) {
//
//                donor.setZabrana(true);
//            }
//        } catch(Exception ex) {
//            throw new TrueBloodException("Nisu uneseni svi zdravstveni podatci.", HttpStatus.NOT_ACCEPTABLE);
//        }
//
//        //donor.setZabrana(registrationService.checkTrajnaZabrana(donorDto.getTrajniZdravstveniPodatciDto()));
//        donor.setPotpunoRegistriran(true);
//
//        return donor;
//    }


    public static void handleValidation(BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            List<FieldError> errors = bindingResult.getFieldErrors();
            String result = errors.stream().map(e -> e.getDefaultMessage()).
                    collect(Collectors.joining("\n"));

            throw new TrueBloodException(result, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    public static Korisnik djelatnikDtoToDjelatnik(DjelatnikDto djelatnikDto) {
        Korisnik korisnik = new Korisnik(djelatnikDto.getOib(),
                djelatnikDto.getSpol(),djelatnikDto.getEmail(),
                djelatnikDto.getIme(),djelatnikDto.getPrezime());
        korisnik.setEnabled(false);
        korisnik.setAdresaStanovanja(djelatnikDto.getAdresaStanovanja());
        korisnik.setMjestoRodenja(djelatnikDto.getMjestoRodenja());
        korisnik.setTelefonPoslovni(djelatnikDto.getTelefonPoslovni());
        korisnik.setTelefonPrivatni(djelatnikDto.getTelefonPrivatni());
        korisnik.setDatumRodenja(djelatnikDto.getDatumRodenja());
        korisnik.setRole(Role.DJELATNIK);
        korisnik.setMjestoZaposlenja(djelatnikDto.getMjestoZaposlenja());
        return korisnik;
    }
}
