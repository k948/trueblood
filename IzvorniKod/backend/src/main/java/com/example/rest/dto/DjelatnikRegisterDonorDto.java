package com.example.rest.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

public class DjelatnikRegisterDonorDto {

    @Valid
    private DonorDto donorDto;

    private TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto;

    @NotBlank(message = "Oznaka krvne grupe je obavezna")
    private String oznakaKgrupe;

    public DonorDto getDonorDto() {
        return donorDto;
    }

    public void setDonorDto(DonorDto donorDto) {
        this.donorDto = donorDto;
    }

    public TrajniZdravstveniPodatciDto getTrajniZdravstveniPodatciDto() {
        return trajniZdravstveniPodatciDto;
    }

    public void setTrajniZdravstveniPodatciDto(TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto) {
        this.trajniZdravstveniPodatciDto = trajniZdravstveniPodatciDto;
    }

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }
}
