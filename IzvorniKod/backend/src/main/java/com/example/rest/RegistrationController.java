package com.example.rest;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.VerificationToken;
import com.example.rest.dto.DonorDto;
import com.example.rest.dto.LozinkaDTO;
import com.example.service.DonorService;
import com.example.service.KorisnikService;
import com.example.service.KrvnaGrupaService;
import com.example.service.VerificationTokenService;
import com.example.service.impl.EmailService;
import com.example.service.impl.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/register")
public class RegistrationController { //refactoriraj code

    @Autowired
    public KorisnikService korisnikService;
    @Autowired
    public VerificationTokenService verificationTokenService;
    @Autowired
    public KrvnaGrupaService krvnaGrupaService;
    @Autowired
    public EmailService emailService;
    @Autowired
    DonorService donorService;

    @Autowired
    RegistrationService registrationService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registerDonor(@Valid @RequestBody DonorDto donorDto, BindingResult bindingResult) {

        Utils.handleValidation(bindingResult);

        registrationService.registerDonor(Utils.donorDtoToDonor(donorDto));
        return new ResponseEntity<>("Check your mail!", HttpStatus.OK);
    }

    @RequestMapping(value = "/confirm-account/res", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> confirmUserAccount(@RequestParam("token") String confirmationToken, @RequestBody LozinkaDTO lozinkaDTO){
        String lozinka = lozinkaDTO.getLozinka();
        registrationService.confirmAccount(lozinka, confirmationToken);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

