package com.example.rest;

import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.example.domain.Darivanje;
import com.example.service.impl.EmailService;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import org.springframework.beans.factory.annotation.Autowired;

public class PdfExporter {
    private Darivanje darivanje;

    public PdfExporter(Darivanje darivanje) {
        this.darivanje = darivanje;
    }

    private void writeTableHeader1(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("Ime", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Prezime", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Donor ID", font));
        table.addCell(cell);

    }

    private void writeTableHeader2(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLACK);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("Djelatnik ID", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Datum i vrijeme", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Uspjesno:", font));
        table.addCell(cell);

    }

    private void writeTableHeader3(PdfPTable table) {
        PdfPCell cell = new PdfPCell();
        cell.setBackgroundColor(Color.BLUE);
        cell.setPadding(5);

        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(Color.WHITE);

        cell.setPhrase(new Phrase("Lokacija", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Broj darivanja", font));
        table.addCell(cell);

        cell.setPhrase(new Phrase("Kolicina", font));
        table.addCell(cell);

    }

    private void writeTableData1(PdfPTable table) {
        table.addCell(darivanje.getDonor().getIme());
        table.addCell(darivanje.getDonor().getPrezime());
        table.addCell(darivanje.getDonor().getId().toString());
    }

    private void writeTableData2(PdfPTable table) {
        table.addCell(darivanje.getDjelatnik().getId().toString());
        table.addCell(darivanje.getDatumVrijeme().toString());
        table.addCell(darivanje.getUspjesno().toString());
    }

    private void writeTableData3(PdfPTable table) {
        table.addCell(darivanje.getLokacija());
        table.addCell(darivanje.getBrojDarivanja().toString());
        table.addCell(darivanje.getKolicina().toString());
    }


    public ByteArrayInputStream export() throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        PdfWriter.getInstance(document, out);


        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLUE);

        Paragraph p = new Paragraph("Potvrda o darivanju", font);
        p.setAlignment(Paragraph.ALIGN_CENTER);

        document.add(p);

        PdfPTable table1 = new PdfPTable(3);
        table1.setWidthPercentage(100f);
        table1.setWidths(new float[] {3.0f,3.0f,3.0f});
        table1.setSpacingBefore(10);

        writeTableHeader1(table1);
        writeTableData1(table1);

        document.add(table1);

        PdfPTable table2 = new PdfPTable(3);
        table2.setWidthPercentage(100f);
        table2.setWidths(new float[] {3.0f,3.0f,3.0f});
        table2.setSpacingBefore(10);

        writeTableHeader2(table2);
        writeTableData2(table2);

        document.add(table2);

        PdfPTable table3 = new PdfPTable(3);
        table3.setWidthPercentage(100f);
        table3.setWidths(new float[] {3.0f,3.0f,3.0f});
        table3.setSpacingBefore(10);

        writeTableHeader3(table3);
        writeTableData3(table3);

        document.add(table3);

        document.close();

        return new ByteArrayInputStream(out.toByteArray());
    }
}
