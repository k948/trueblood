package com.example.rest;


import com.example.domain.Darivanje;
import com.example.service.DarivanjeService;
import com.example.service.impl.EmailService;
import com.github.lambdaexpression.annotation.RequestBodyParam;
import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/donor")
public class DonorContoller {

    @Autowired
    DarivanjeService darivanjeService;

    @Autowired
    EmailService emailService;

    @GetMapping("")
    public String getUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("/povijest-darivanja")
    public ResponseEntity<Object> getDonations(@RequestBodyParam String donorId) {
        List<Darivanje> darivanja = darivanjeService.findByDonorId(Long.parseLong(donorId));
        return new ResponseEntity<>(darivanja, HttpStatus.OK);
    }

    @GetMapping("/povijest-darivanja/pdf")
    public ResponseEntity<InputStreamResource> exportToPDF(@RequestParam("sifra") String sifraDarivanja, HttpServletResponse response) throws DocumentException, IOException {

        List<Darivanje> darivanja = darivanjeService.findAll();
        Darivanje darivanjeBySifra = null;
        for(Darivanje darivanje : darivanja) {
            if(darivanje.getSifraDarivanja() == Long.parseLong(sifraDarivanja)) {
                darivanjeBySifra = darivanje;
            }
        }

        if(darivanjeBySifra == null ||
                darivanjeBySifra.getDonor().getId() !=
                        Long.parseLong(SecurityContextHolder.getContext().getAuthentication().getName()) ||
        darivanjeBySifra.getUspjesno() == false){
            throw new TrueBloodException("Traženi resurs ne postoji ili nemate pravo na njega!", HttpStatus.BAD_REQUEST);
        }
        PdfExporter exporter = new PdfExporter(darivanjeBySifra);

        ByteArrayInputStream bis = exporter.export();

        String filename = "potvrda_o_darivanju.pdf";

        HttpHeaders headers = new HttpHeaders();


        headers.setContentType(MediaType.APPLICATION_PDF);
        response.setHeader("Content-Disposition", "attachment; filename=" + filename);

        return new ResponseEntity<>(new InputStreamResource(bis), headers, HttpStatus.OK);
    }

}
