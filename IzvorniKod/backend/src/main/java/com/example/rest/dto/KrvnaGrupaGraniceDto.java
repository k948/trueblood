package com.example.rest.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class KrvnaGrupaGraniceDto {

    @NotBlank(message = "Krvna grupa je obavezna")
    private String oznakaKgrupe;
    @NotNull(message = "Donja granica je obavezna")
    private Double donjaGranica;
    @NotNull(message = "Gornja granica je obavezna")
    private Double gornjaGranica;

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }

    public Double getDonjaGranica() {
        return donjaGranica;
    }

    public void setDonjaGranica(String donjaGranica) {
        this.donjaGranica = Double.parseDouble(donjaGranica);
    }

    public Double getGornjaGranica() {
        return gornjaGranica;
    }

    public void setGornjaGranica(String gornjaGranica) {
        this.gornjaGranica = Double.parseDouble(gornjaGranica);
    }
}
