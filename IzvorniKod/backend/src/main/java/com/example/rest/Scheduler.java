package com.example.rest;

import com.example.domain.Darivanje;
import com.example.domain.Donor;
import com.example.service.DarivanjeService;
import com.example.service.impl.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Component
public class Scheduler {

    @Autowired
    DarivanjeService darivanjeService;
    @Autowired
    EmailService emailService;

    @Scheduled(cron="@daily")
    public void donationReady() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        List<Darivanje> darivanja = darivanjeService.findAll();
        Donor donor = null;
        String spol = null;
        int razlika = 0;
        for(Darivanje darivanje : darivanja) {
            donor = darivanje.getDonor();
            spol = donor.getSpol();
            razlika = (int)((now.getTime() - darivanje.getDatumVrijeme().getTime()) / (1000*60*60*24));
            if((razlika >= 90 && spol.equals("M")) || (razlika >= 120 && spol.equals("Ž"))) {
                emailService.sendDonationPossibleMail(donor.getEmail());
            }
        }
    }
}

