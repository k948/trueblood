package com.example.rest;

import com.example.domain.KrvnaGrupa;
import com.example.service.DonorService;
import com.example.service.KrvnaGrupaService;
import com.example.service.impl.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HelperController {

    @Autowired
    DonorService donorService;

    @Autowired
    EmailService emailService;

    @Autowired
    KrvnaGrupaService krvnaGrupaService;

    @GetMapping("/user")
    public ResponseEntity<Object> getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<>(principal, HttpStatus.OK);
    }

    @GetMapping("/blood-groups")
    public ResponseEntity<Object> showBloodGroups() {
        List<KrvnaGrupa> krvnaGrupaList = krvnaGrupaService.findAll();
        return new ResponseEntity<>(krvnaGrupaList, HttpStatus.OK);
    }


        /*@GetMapping("/{id}")
        @PreAuthorize("hasRole('ROLE_DONOR')")
        public Donor getDonor(@PathVariable("id") long id) {

            return donorService.fetch(id);
        }*/

     /*   @GetMapping("/login")
        public ResponseEntity<String> err(@RequestParam(required = false) String error){
            if(error != null){
                return new ResponseEntity<>("Neuspjela prijava", HttpStatus.UNAUTHORIZED);
            }
            else{
                return new ResponseEntity<>("Šalji POST sa user i pass na /login", HttpStatus.OK);
            }
        } */

    /*@CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registerDonor( @RequestBody Object donorDto, BindingResult bindingResult) {
        System.out.println("USO SAAAAAAAAAAAM");
        if (bindingResult.hasErrors()) {
            List<FieldError> errors = bindingResult.getFieldErrors();
            String result = errors.stream().map(e -> e.getDefaultMessage()).
                    collect(Collectors.joining("\n"));
            return new ResponseEntity<>(result, HttpStatus.NOT_ACCEPTABLE);
        }
         /*try {
             Donor donor = Utils.donorDtoToDonor(donorDto);
             //donor.setKrvnaGrupa(krvnaGrupaService.findByOznakaKgrupe(donorDto.getOznakaKGrupe()));
             donorService.save(donor);
             VerificationToken verificationToken = new VerificationToken(donor);
             verificationTokenService.save(verificationToken);
             emailService.sendConfirmationEmail(donor.getEmail(), verificationToken.getToken(), donor.getId());
             return new ResponseEntity<>("Check your mail!", HttpStatus.OK); //
         }
         catch (Exception e){
             return new ResponseEntity<>("Email i oib moraju biti jedinstveni", HttpStatus.BAD_REQUEST);
         }
     }*/

    }

