package com.example.rest.dto;

import com.example.rest.TrueBloodException;
import org.springframework.http.HttpStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class DonacijaDonorDto {

    @NotNull(message = "Id je obavezan")
    private Long donorId;
    @NotBlank(message = "Ime je obavezno")
    private String ime;
    @NotBlank(message = "Prezime je obavezno")
    private String prezime;
    @Pattern(regexp = "^\\d{11}$", message = "Oib mora sadržavati 11 znamenki")
    private String oib;
    private String mjestoRodenja;
    private String mjestoZaposlenja;
    private String adresaStanovanja;
    private String telefonPrivatni;
    private String telefonPoslovni;

    public Long getDonorId() {
        return donorId;
    }

    public void setDonorId(String donorId) {

        try {
            this.donorId = Long.parseLong(donorId);
        }
        catch(Exception e){
            throw new TrueBloodException("Id se sastoji isključivo od znamenki", HttpStatus.BAD_REQUEST);
        }
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getMjestoRodenja() {
        return mjestoRodenja;
    }

    public void setMjestoRodenja(String mjestoRodenja) {
        this.mjestoRodenja = mjestoRodenja;
    }

    public String getMjestoZaposlenja() {
        return mjestoZaposlenja;
    }

    public void setMjestoZaposlenja(String mjestoZaposlenja) {
        this.mjestoZaposlenja = mjestoZaposlenja;
    }

    public String getAdresaStanovanja() {
        return adresaStanovanja;
    }

    public void setAdresaStanovanja(String adresaStanovanja) {
        this.adresaStanovanja = adresaStanovanja;
    }

    public String getTelefonPrivatni() {
        return telefonPrivatni;
    }

    public void setTelefonPrivatni(String telefonPrivatni) {
        this.telefonPrivatni = telefonPrivatni;
    }

    public String getTelefonPoslovni() {
        return telefonPoslovni;
    }

    public void setTelefonPoslovni(String telefonPoslovni) {
        this.telefonPoslovni = telefonPoslovni;
    }
}
