package com.example.dao;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DonorRepository extends JpaRepository<Donor, Long> {
    public Optional<Donor> findById(long donorId);

    public Donor findByEmail(String email);

    public Donor save(Donor donor);

    boolean existsByEmailAndIdNot(String email, Long id);

    boolean existsByOibAndIdNot(String oib, Long id);
}
