package com.example.dao;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository
        extends JpaRepository<VerificationToken, Long> {

   public VerificationToken findByToken(String token);

   public VerificationToken findByKorisnik(Korisnik korisnik);

   public VerificationToken save(VerificationToken token);
}
