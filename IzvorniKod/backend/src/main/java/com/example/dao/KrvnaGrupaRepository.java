package com.example.dao;

import com.example.domain.KrvnaGrupa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KrvnaGrupaRepository extends JpaRepository<KrvnaGrupa, String> {

   public Optional<KrvnaGrupa> findByOznakaKgrupe(String oznaka);
}
