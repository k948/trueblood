package com.example.dao;

import com.example.domain.Darivanje;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DarivanjeRepository extends JpaRepository<Darivanje, Long> {

    public Darivanje save(Darivanje darivanje);

    @Query("SELECT COUNT (donor_id)  FROM Darivanje where uspjesno = TRUE and donor_id = ?1")
    public long countSuccessByDonor(Long donorId);

    @Query(value = "SELECT datum_vrijeme from Darivanje where uspjesno = TRUE and donor_id = ?1 order by datum_vrijeme desc limit 1", nativeQuery = true)
    public List<Timestamp> lastSuccessfullDonation(Long donorId);
}
