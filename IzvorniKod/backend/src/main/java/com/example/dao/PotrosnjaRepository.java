package com.example.dao;

import com.example.domain.Potrosnja;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PotrosnjaRepository extends JpaRepository<Potrosnja, Long> {
    public Potrosnja save(Potrosnja potrosnja);
}
