package com.example.dao;

import com.example.domain.Korisnik;
import com.example.domain.Donor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {
    public Korisnik findByEmail(String email);
    public Optional<Korisnik> findById(long id);

    public Korisnik save(Korisnik korisnik);

    @Query("SELECT email FROM Korisnik where role = 'DJELATNIK'")
    List<String> findDjelatnikEmails();

    @Query("SELECT email FROM Donor where oznaka_kgrupe = ?1") // radi, nije error
    public List<String> findDonorEmails(String oznakaKgrupe);

    boolean existsByOib(String oib);

    boolean existsByEmail(String email);

    List<Korisnik> findAll();

    boolean existsByOibAndIdNot(String oib, Long id);

    boolean existsByEmailAndIdNot(String email, Long id);

    Korisnik findByOib(String Oib);
}
