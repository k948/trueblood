package com.example.service;

import com.example.domain.Donor;
import com.example.rest.dto.DonacijaDonorDto;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface DonorService {


    public Donor save(Donor donor);

    public Optional<Donor> findByDonorId(long donorId);

    public Donor fetch(long donorId);

    public Donor setDonorKgrupa(Donor donorForm, String oznakaKgrupe);

    public Donor registerNewDonorAccount(Donor donor);

    public Donor storePassword(Donor donor, String password);

    void updateDonor(Donor donor, DonacijaDonorDto donacijaDonorDto);
}
