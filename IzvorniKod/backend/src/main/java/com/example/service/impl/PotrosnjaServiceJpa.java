package com.example.service.impl;

import com.example.dao.PotrosnjaRepository;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.domain.Potrosnja;
import com.example.service.KrvnaGrupaService;
import com.example.service.PotrosnjaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PotrosnjaServiceJpa implements PotrosnjaService {

    @Autowired
    KrvnaGrupaService krvnaGrupaService;

    @Autowired
    PotrosnjaRepository potrosnjaRepository;


    @Override
    public void zabiljeziPotrosnju(Korisnik djelatnik, String oznakaKgrupe, Double kolicina) {
        KrvnaGrupa krvnaGrupa = krvnaGrupaService.findByOznakaKgrupe(oznakaKgrupe);
        krvnaGrupaService.updateVolume(oznakaKgrupe, -kolicina);
        Potrosnja potrosnja = new Potrosnja(djelatnik, krvnaGrupa, kolicina);
        potrosnjaRepository.save(potrosnja);
    }
}
