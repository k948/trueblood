package com.example.service;

import com.example.domain.Korisnik;
import org.springframework.stereotype.Service;

@Service
public interface PotrosnjaService {
    void zabiljeziPotrosnju(Korisnik djelatnik, String oznakaKgrupe, Double kolicina);
}
