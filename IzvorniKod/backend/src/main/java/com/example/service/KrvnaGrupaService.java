package com.example.service;

import com.example.domain.KrvnaGrupa;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KrvnaGrupaService {
    public KrvnaGrupa findByOznakaKgrupe(String oznaka);

    void updateVolume(String oznakaKgrupe, Double kolicina);

    void updateBloodBoundary(String oznakaKgrupe, Double donjaGranica, Double gornjaGranica);

    List<KrvnaGrupa> findAll();
}
