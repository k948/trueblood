package com.example.service;

import com.example.domain.Darivanje;
import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.rest.dto.DonacijaPodatciDto;
import com.example.rest.dto.TrajniZdravstveniPodatciDto;
import com.example.rest.dto.TrenutniZdravstveniPodatciDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DarivanjeService {
    boolean checkTrajnaZabrana(TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto, Donor donor);

    boolean checkPrivremenaZabrana(TrenutniZdravstveniPodatciDto trenutniZdravstveniPodatciDto);

    void saveFailed(Korisnik djelatnik, Donor donor, String lokacija);

    void saveSucceded(Korisnik djelatnik, Long donorId, String lokacija, Double kolicina);

    Donor checkDonor(Long donorId) ;

    double handleDonacijaForm(DonacijaPodatciDto donacijaPodatciDto);

    List<Darivanje> findAll();

    Darivanje findById(Long donorId);

    List<Darivanje> findByDonorId(Long donorId);
}
