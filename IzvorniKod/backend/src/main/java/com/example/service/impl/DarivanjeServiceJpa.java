package com.example.service.impl;

import com.example.dao.DarivanjeRepository;
import com.example.domain.Darivanje;
import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.rest.PdfExporter;
import com.example.rest.TrueBloodException;
import com.example.rest.dto.DonacijaPodatciDto;
import com.example.rest.dto.TrajniZdravstveniPodatciDto;
import com.example.rest.dto.TrenutniZdravstveniPodatciDto;
import com.example.security.UserDetail;
import com.example.service.DarivanjeService;
import com.example.service.DonorService;
import com.example.service.KrvnaGrupaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.sql.Timestamp;
import java.util.*;

@Service
public class DarivanjeServiceJpa implements DarivanjeService {

    @Autowired
    DarivanjeRepository darivanjeRepository;

    @Autowired
    DonorService donorService;

    @Autowired
    KrvnaGrupaService krvnaGrupaService;

    @Autowired
    EmailService emailService;

    @Override
    public boolean checkTrajnaZabrana(TrajniZdravstveniPodatciDto zdravPodatci, Donor donor) {
        if(     !zdravPodatci.getKronicneBolesti().equals("NE") ||
                !zdravPodatci.getDrogaIntravenski().equals("NE") ||
                !zdravPodatci.getIzabraneTeskeBolesti().equals("NE") ||
                !zdravPodatci.getOvisnost().equals("NE")||
                !zdravPodatci.getModnosM().equals("NE") ||
                !zdravPodatci.getOdnosProstucija().equals("NE") ||
                !zdravPodatci.getPromiskuitet().equals("NE") ||
                !zdravPodatci.getSpolnoPrenosiveBolesti().equals("NE") ||
                !zdravPodatci.getHivPozitivan().equals("NE") ||
                !zdravPodatci.getSeksPartnnerRizSkupine().equals("NE")) {

            donor.setZabrana(true);
            return true;
        }
        return false;
    }
    @Override
    public boolean checkPrivremenaZabrana(TrenutniZdravstveniPodatciDto zdravPodatci) {
        if(     !zdravPodatci.getTezina().equals("NE") ||
                !zdravPodatci.getBolesnoStanje().equals("NE") ||
                !zdravPodatci.getTemperatura().equals("NE") ||
                !zdravPodatci.getTlak().equals("NE") ||
                !zdravPodatci.getPuls().equals("NE") ||
                !zdravPodatci.getUzimaLijekove().equals("NE") ||
                !zdravPodatci.getHemoglobin().equals("NE") ||
                !zdravPodatci.getKonzumacijaAlkohola().equals("NE") ||
                !zdravPodatci.getBolesnoStanje().equals("NE")||
                !zdravPodatci.getMensTrudDoj().equals("NE")||
                !zdravPodatci.getOpasniPoslovi().equals("NE")){

            return true;
        }
        else
            return false;
    }

    @Override
    public void saveFailed(Korisnik djelatnik, Donor donor, String lokacija) {
        Darivanje darivanje = new Darivanje(djelatnik, donor, false, lokacija);
        darivanjeRepository.save(darivanje);
    }

    @Override
    public void saveSucceded(Korisnik djelatnik, Long donorId, String lokacija, Double kolicina) {

        Donor donor = checkDonor(donorId);
        if(donor.getKrvnaGrupa() == null){
            throw new TrueBloodException("Donor nema zabilježenu krvnu grupu", HttpStatus.BAD_REQUEST);
        }

        if(kolicina < 0){
            throw new TrueBloodException("Količina donirane krvi mora biti poziitivna.", HttpStatus.BAD_REQUEST);
        }
        if(donor.getSpol().equals("M") && kolicina > 5){
            throw new TrueBloodException("Količina donirane krvi mora biti iz intervala [0, 5]dcl.", HttpStatus.BAD_REQUEST);
        }
        if(donor.getSpol().equals("Ž") && kolicina > 3.5){
            throw new TrueBloodException("Količina donirane krvi mora biti iz intervala [0, 3.5]dcl.", HttpStatus.BAD_REQUEST);
        }
        krvnaGrupaService.updateVolume(donor.getKrvnaGrupa().getOznakaKgrupe(), kolicina);

        Darivanje darivanje = new Darivanje(djelatnik, donor, true, lokacija);
        long brojDarivanja = darivanjeRepository.countSuccessByDonor(donorId) + 1;
        darivanje.setKolicina(kolicina);
        darivanje.setBrojDarivanja((int)brojDarivanja);

        darivanjeRepository.save(darivanje);


        PdfExporter exporter = new PdfExporter(darivanje);
        try{
            ByteArrayInputStream bis = exporter.export();
            emailService.sendDonationPdf(bis, donor.getEmail());
        }
        catch (Exception e){
            //mail nije poslan - do nothing :(
        }

    }

    @Override
    public Donor checkDonor(Long donorId) {
        Donor donor = donorService.fetch(donorId);

        if(donor.getKrvnaGrupa() != null){
            if(donor.getSpol().equals("M") && donor.getKrvnaGrupa().getZaliha() > 95 ||
                    donor.getSpol().equals("Ž") && donor.getKrvnaGrupa().getZaliha() > 96.5){
                throw new TrueBloodException("Kapaciteti krvne grupe su trenutno popunjeni.", HttpStatus.BAD_REQUEST);
            }
        }
        if(donor.getZabrana())
            throw new TrueBloodException("Donor ima trajnu zabranu darivanja.", HttpStatus.NOT_ACCEPTABLE);

        checkBirthDate(donor);
        checkLastDonationDate(donor);

        return donor;
    }

    private void checkLastDonationDate(Donor donor) {
        List<Timestamp> successfullDates = darivanjeRepository.lastSuccessfullDonation(donor.getId());
        if(successfullDates.size() == 0)
            return;

        Timestamp lastDonationDate = successfullDates.iterator().next();
        Calendar cal = Calendar.getInstance();
        cal.setTime(lastDonationDate);

        if(donor.getSpol().equals("M")){
            cal.add(Calendar.MONTH, 3);
            if((cal.getTime()).after(new Date())){
                throw new TrueBloodException("Nije prošlo dovoljno vremena od posljednjeg darivanja", HttpStatus.BAD_REQUEST);
            }
        }
        if(donor.getSpol().equals("Ž")){
            cal.add(Calendar.MONTH, 4);
            if((cal.getTime()).after(new Date())){
                throw new TrueBloodException("Nije prošlo dovoljno vremena od posljednjeg darivanja", HttpStatus.BAD_REQUEST);
            }
        }
    }

    private void checkBirthDate(Donor donor) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(donor.getDatumRodenja());
        cal.add(Calendar.YEAR, 18);
        if((cal.getTime()).after(new Date())){
            throw new TrueBloodException("Donor mora biti punoljetan", HttpStatus.BAD_REQUEST);
        }

        cal = Calendar.getInstance();
        cal.setTime(donor.getDatumRodenja());
        cal.add(Calendar.YEAR, 65);
        //Date datePom = new Date(cal.getTime().getTime());
        if((cal.getTime()).before(new Date())){
            throw new TrueBloodException("Donor ne smije biti stariji od 65 godina", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public double handleDonacijaForm(DonacijaPodatciDto donacijaPodatciDto){

        Donor donor = donorService.fetch(donacijaPodatciDto.getDonacijaDonorDto().getDonorId());
        donorService.updateDonor(donor, donacijaPodatciDto.getDonacijaDonorDto());
        checkTrajnaZabrana(donacijaPodatciDto.getTrajniZdravstveniPodatciDto(), donor);

        if(donor.getKrvnaGrupa()==null){
            donorService.setDonorKgrupa(donor, donacijaPodatciDto.getOznakaKgrupe());
            donor.setPotpunoRegistriran(true);
        }
        donorService.save(donor);

        checkDonor(donor.getId());
        if(checkPrivremenaZabrana(donacijaPodatciDto.getTrenutniZdravstveniPodatciDto())){
            Korisnik djelatnik =  ((UserDetail)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getKorisnik();
            saveFailed(djelatnik, donor, donacijaPodatciDto.getLokacija());
            throw new TrueBloodException("Privremeno odbijen", HttpStatus.NOT_ACCEPTABLE);
        }
        return donor.getKrvnaGrupa().getZaliha();
    }

    @Override
    public List<Darivanje> findAll() {
        return darivanjeRepository.findAll();
    }

    @Override
    public Darivanje findById(Long donorId) {
        Darivanje darivanje = darivanjeRepository.findById(donorId).orElseThrow(
                () -> new RuntimeException("Neispravno korisničko ime.")
        );
        return darivanje;
    }

    @Override
    public List<Darivanje> findByDonorId(Long donorId) {
        List<Darivanje> darivanja = new ArrayList<>();
        for(Darivanje darivanje: findAll()) {
            if(darivanje.getDonor().getId().equals(donorId)) {
                darivanja.add(darivanje);
            }
        }
        return darivanja;
    }
}
