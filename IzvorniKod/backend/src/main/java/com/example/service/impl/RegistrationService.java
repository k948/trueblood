package com.example.service.impl;

import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.VerificationToken;
import com.example.rest.TrueBloodException;
import com.example.rest.Utils;
import com.example.rest.dto.DjelatnikRegisterDonorDto;
import com.example.rest.dto.TrajniZdravstveniPodatciDto;
import com.example.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Date;

@Service
public class RegistrationService {

    @Autowired
    DonorService donorService;
    @Autowired
    VerificationTokenService verificationTokenService;
    @Autowired
    EmailService emailService;
    @Autowired
    DarivanjeService darivanjeService;

    @Autowired
    KorisnikService korisnikService;

    public void djelatnikRegisterDonor(Donor donor, TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto, String oznakaKgrupe) {
        donor = donorService.setDonorKgrupa(donor, oznakaKgrupe);
        darivanjeService.checkTrajnaZabrana(trajniZdravstveniPodatciDto, donor);
        donor.setPotpunoRegistriran(true);
        registerDonor(donor);
    }

    public void registerDonor(Donor donor) {
        if(korisnikService.existsByOib(donor.getOib()))
            throw new TrueBloodException(
                    "Donor s OIB-om:  " + donor.getOib() + " već postoji. Oib mora biti jedinstven!",
                    HttpStatus.NOT_ACCEPTABLE);
        if(korisnikService.existsByEmail(donor.getEmail()))
            throw new TrueBloodException(
                    "Donor s email-om:  " + donor.getEmail() + " već postoji. Email mora biti jedinstven!",
                    HttpStatus.NOT_ACCEPTABLE);

        donorService.save(donor);
        //dodao
        Long newlyRegistredDonorId = korisnikService.findByOib(donor.getOib());
        VerificationToken verificationToken = new VerificationToken(donor);
        verificationTokenService.save(verificationToken);
        emailService.sendConfirmationEmail(donor.getEmail(), verificationToken.getToken(), newlyRegistredDonorId);

    }

    public void  confirmAccount(String lozinka, String confirmationToken){
        VerificationToken token = verificationTokenService.findByToken(confirmationToken);
        if(token != null && token.getDateEx().after(new Date())) {
            Korisnik korisnik = token.getKorisnik();
            korisnikService.setLozinka(lozinka, korisnik);
            korisnik.setEnabled(true);
            korisnikService.save(korisnik);
            verificationTokenService.delete(token);
        }
        else
        {
            if(token!=null){
                verificationTokenService.delete(token);
            }
            throw new TrueBloodException("Neispravan token!", HttpStatus.BAD_REQUEST);
        }
    }

    public void registerDjelatnik(Korisnik djelatnik) {
        if(korisnikService.existsByOib(djelatnik.getOib()))
            throw new TrueBloodException(
                    "Djelatnik s OIB-om:  " + djelatnik.getOib() + " već postoji. Oib mora biti jedinstven!",
                     HttpStatus.NOT_ACCEPTABLE);
        if(korisnikService.existsByEmail(djelatnik.getEmail()))
            throw new TrueBloodException(
                    "Djelatnik s email-om:  " + djelatnik.getEmail() + " već postoji. Email mora biti jedinstven!",
                    HttpStatus.NOT_ACCEPTABLE);

        korisnikService.save(djelatnik);
        VerificationToken verificationToken = new VerificationToken(djelatnik);
        verificationTokenService.save(verificationToken);
        emailService.sendConfirmationEmail(djelatnik.getEmail(), verificationToken.getToken(), djelatnik.getId());
    }
}

