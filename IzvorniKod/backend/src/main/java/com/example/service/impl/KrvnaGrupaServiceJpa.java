package com.example.service.impl;

import com.example.dao.KrvnaGrupaRepository;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.rest.TrueBloodException;
import com.example.service.DonorService;
import com.example.service.KorisnikService;
import com.example.service.KrvnaGrupaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class KrvnaGrupaServiceJpa implements KrvnaGrupaService {

    @Autowired
    KrvnaGrupaRepository krvnaGrupaRepository;

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    DonorService donorService;

    @Autowired
    EmailService emailService;

    @Override
    public KrvnaGrupa findByOznakaKgrupe(String oznaka) {
        Optional<KrvnaGrupa> OptKrvnaGrupa = krvnaGrupaRepository.findByOznakaKgrupe(oznaka);
        if(OptKrvnaGrupa.isPresent()) {
            return OptKrvnaGrupa.get();
        }
        else
            throw new TrueBloodException("Nepostojeća krvna grupa.", HttpStatus.BAD_REQUEST);
    }

    @Override
    public void updateVolume(String oznakaKgrupe, Double kolicina) {
        KrvnaGrupa krvnaGrupa = findByOznakaKgrupe(oznakaKgrupe);
        Double oldValue = krvnaGrupa.getZaliha();
        Double newValue = oldValue + kolicina;
        if(newValue < 0){
            throw new TrueBloodException("Trenutno je najviše dostupno " + oldValue + "dcl " + oznakaKgrupe + " krvne grupe.", HttpStatus.NOT_ACCEPTABLE);
        }
        if(newValue > 100){
            throw new TrueBloodException("Trenutna zaliha krvne grupe je" + oldValue + "dcl. Maksimalan kapacitet za pohranu je 100dcl.", HttpStatus.BAD_REQUEST);
        }
        krvnaGrupa.setZaliha(newValue);
        krvnaGrupaRepository.save(krvnaGrupa);

        System.out.println("1");

        if(newValue > krvnaGrupa.getGornjaGranica() && oldValue <= krvnaGrupa.getGornjaGranica()){

            String msg = "Razina "+ oznakaKgrupe + " je iznad gornje granice.\nhttps://kivi-trueblood-frontend.herokuapp.com/";
            List<String> emails = korisnikService.findDjelatnikEmails();
            emailService.sendNotificationEmails(emails, msg);
        }
        else if(newValue < krvnaGrupa.getDonjaGranica() && oldValue >= krvnaGrupa.getDonjaGranica()){
            String msg = "Razina "+ oznakaKgrupe + " je iznad gornje granice.\nhttps://kivi-trueblood-frontend.herokuapp.com/";
            List<String> emails = korisnikService.findDjelatnikEmails();
            List<String> emails2 = korisnikService.findDonorEmails(oznakaKgrupe);
            emails.addAll(emails2);

            emailService.sendNotificationEmails(emails, msg);
        }
    }

    @Override
    public void updateBloodBoundary(String oznakaKgrupe, Double donjaGranica, Double gornjaGranica) {
        KrvnaGrupa krvnaGrupa = findByOznakaKgrupe(oznakaKgrupe);


        if (gornjaGranica > 100.0 || donjaGranica > 100.0 || gornjaGranica < 0.0 || donjaGranica < 0.0) {
            throw new TrueBloodException("Optimalne granice moraju biti unutar raspona [0-100]", HttpStatus.BAD_REQUEST);
        }
        if (gornjaGranica.equals(donjaGranica)) {
            throw new TrueBloodException("Donja i gornja optimalna granica ne smiju biti jednake", HttpStatus.BAD_REQUEST);
        }


        if(donjaGranica > gornjaGranica){
            throw new TrueBloodException("Gornja granica mora biti viša od donje granice", HttpStatus.BAD_REQUEST);
        }
        Double zaliha = krvnaGrupa.getZaliha();

        if(krvnaGrupa.getGornjaGranica() > zaliha && gornjaGranica < zaliha){
            String msg = "Razina "+ oznakaKgrupe + " je iznad gornje granice.\nhttps://kivi-trueblood-frontend.herokuapp.com/";
            List<String> emails = korisnikService.findDjelatnikEmails();
            emailService.sendNotificationEmails(emails, msg);
        }
        else if(krvnaGrupa.getDonjaGranica() < zaliha && donjaGranica > zaliha){
            String msg = "Razina "+ oznakaKgrupe + " je iznad gornje granice.\nhttps://kivi-trueblood-frontend.herokuapp.com/";
            List<String> emails = korisnikService.findDjelatnikEmails();
            List<String> emails2 = korisnikService.findDonorEmails(oznakaKgrupe);
            emails.addAll(emails2);

            emailService.sendNotificationEmails(emails, msg);
        }

        krvnaGrupa.setDonjaGranica(donjaGranica);
        krvnaGrupa.setGornjaGranica(gornjaGranica);
        krvnaGrupaRepository.save(krvnaGrupa);
    }

    @Override
    public List<KrvnaGrupa> findAll() {
        return krvnaGrupaRepository.findAll();
    }
}
