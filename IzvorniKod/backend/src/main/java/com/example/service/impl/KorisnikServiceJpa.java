package com.example.service.impl;

import com.example.dao.KorisnikRepository;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.rest.TrueBloodException;
import com.example.service.KorisnikService;
import com.example.service.KrvnaGrupaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import java.util.Collection;
import java.util.List;


@Service
public class KorisnikServiceJpa implements KorisnikService {

    @Autowired
    KorisnikRepository korisnikRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public Korisnik findById(Long username) {
        Korisnik korisnik = korisnikRepository.findById(username).orElseThrow(
                () -> new TrueBloodException("Neispravno korisničko ime.", HttpStatus.NOT_ACCEPTABLE)
        );
        return korisnik;
    }


    @Override
    public Korisnik findByEmail(String email) {
        return korisnikRepository.findByEmail(email);
    }

    @Override
    public Korisnik fetch(long id) {
        return findById(id);
    }

    @Override
    public void save(Korisnik korisnik) {
        korisnikRepository.save(korisnik);
    }

    @Override
    public Korisnik registerNewKorisnikAccount(Korisnik korisnik) {
        return null;
    }

    @Override
    public List<String> findDjelatnikEmails() {
        return korisnikRepository.findDjelatnikEmails();
    }

    @Override
    public List<String> findDonorEmails(String oznakaKgrupe) {
        return korisnikRepository.findDonorEmails(oznakaKgrupe);
    }

    @Override
    public boolean existsByOib(String oib) {
        return korisnikRepository.existsByOib(oib);
    }

    @Override
    public boolean existsByEmail(String email) {
        return korisnikRepository.existsByEmail(email);
    }

    @Override
    public void setLozinka(String lozinka, Korisnik korisnik) {
        String encodedLozinka = encoder.encode(lozinka);
        korisnik.setPassword(encodedLozinka);
        korisnikRepository.save(korisnik);
    }


    @Override
    public List<Korisnik> findAllAccounts() {
        return korisnikRepository.findAll();
    }

    @Override
    public boolean existsByOibAndIdNot(String oib, Long id) {
        return korisnikRepository.existsByOibAndIdNot(oib, id);
    }

    @Override
    public Long findByOib(String oib) {
        Korisnik korisnik = korisnikRepository.findByOib(oib);
        return korisnik.getId();
    }


}
