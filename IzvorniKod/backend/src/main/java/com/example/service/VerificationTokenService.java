package com.example.service;

import com.example.domain.Korisnik;
import com.example.domain.VerificationToken;
import org.springframework.stereotype.Service;

@Service
public interface VerificationTokenService {
    public VerificationToken findByToken(String token);

    public VerificationToken findByKorisnik(Korisnik korisnik);

    public void delete(VerificationToken verificationToken);

    public VerificationToken save(VerificationToken verificationToken);
}
