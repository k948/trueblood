package com.example.service;

import com.example.domain.Korisnik;
import com.example.rest.dto.AzuriranjeDto;
import org.springframework.stereotype.Service;

@Service
public interface OsobniPodaciService {

    public boolean checkIfGivenAndLoggedInAreEqual(String id);


    public Korisnik updateKorisnik(AzuriranjeDto azuriranjeDto);
}
