package com.example.service;


import com.example.domain.Korisnik;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface KorisnikService {


    Korisnik findById(Long username);

    Korisnik findByEmail(String email);

    void setLozinka(String lozinka, Korisnik korisnik);

    void save(Korisnik korisnik);

    public Korisnik fetch(long id);

    public Korisnik registerNewKorisnikAccount(Korisnik korisnik);


    public List<String> findDjelatnikEmails();

    public List<String> findDonorEmails(String oznakaKgrupe);

    boolean existsByOib(String oib);

    boolean existsByEmail(String email);

    List<Korisnik> findAllAccounts();

    boolean existsByOibAndIdNot(String oib, Long id);

    Long findByOib(String oib);
}
