package com.example.service.impl;

import com.example.dao.VerificationTokenRepository;
import com.example.domain.Korisnik;
import com.example.domain.VerificationToken;
import com.example.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenServiceJpa implements VerificationTokenService {
    @Autowired
    VerificationTokenRepository verificationTokenRepository;


    @Override
    public VerificationToken findByToken(String token) {
        return verificationTokenRepository.findByToken(token);
    }

    @Override
    public VerificationToken findByKorisnik(Korisnik korisnik) {
        return verificationTokenRepository.findByKorisnik(korisnik);
    }

    @Override
    public void delete(VerificationToken verificationToken) {
        verificationTokenRepository.delete(verificationToken);
    }

    @Override
    public VerificationToken save(VerificationToken verificationToken) {
        return verificationTokenRepository.save(verificationToken);
    }



}
