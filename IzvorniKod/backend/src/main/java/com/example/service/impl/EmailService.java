package com.example.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service("emailService")
public class EmailService {

    private JavaMailSender javaMailSender;

    @Autowired
    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendConfirmationEmail(String email, String token, Long id){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Dovršite registraciju!");
        mailMessage.setFrom("truebloodregister@gmail.com");
        mailMessage.setText("Poštovani,\n" + "zaprimili smo Vašu registraciju.\n" +
                "Dodijeljeno Vam je korisničko ime za prijavu: "+ id +"\nKako biste potvrdili svoj korisnički račun, molimo Vas " +
                "kliknite ovdje: "
                + "https://kivi-trueblood-frontend.herokuapp.com/confirm-account?token=" + token);

        sendEmail(mailMessage);
    }

    public void sendNotificationEmails(List<String> emails, String msg){
        for(String email: emails){
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(email);
            mailMessage.setSubject("Provjeri razine krvi!");
            mailMessage.setFrom("truebloodregister@gmail.com");
            mailMessage.setText(msg);
            sendEmail(mailMessage);
        }
    }

    public void sendDonationPossibleMail(String email) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Darivanje krvi");
        mailMessage.setFrom("truebloodregister@gmail.com");
        mailMessage.setText("Poštovani, ovim putem Vas obaviještavamo da je proteklo dovoljno vremena od prošlog darivanja! Vidimo se!");

        sendEmail(mailMessage);
    }

    public void sendDonationPdf(ByteArrayInputStream bis, String email) {

        sendPdfEmail(new MimeMessagePreparator() {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

                helper.setTo(email);
                helper.setSubject("Potvrda o darivanju");
                helper.setFrom("truebloodregister@gmail.com");
                helper.setText("Hvala Vam na doniranju krvi!\nZa sve informacije o doniranju krvi te zalihama krvnih grupa, posjetite našu stranicu:\nhttps://kivi-trueblood-frontend.herokuapp.com/");
                helper.addAttachment("potvrda_o_darivanju.pdf", new ByteArrayResource(bis.readAllBytes()), "application/pdf");
            }
        });
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        javaMailSender.send(email);
    }

    @Async
    public void sendPdfEmail(MimeMessagePreparator email) {
        javaMailSender.send(email);
    }


}