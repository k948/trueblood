package com.example.service;

import com.example.domain.Donor;
import com.example.rest.dto.TrajniZdravstveniPodatciDto;
import org.springframework.stereotype.Service;

@Service
public interface RegistrationService {
    boolean checkTrajnaZabrana(TrajniZdravstveniPodatciDto trajniZdravstveniPodatciDto);
}
