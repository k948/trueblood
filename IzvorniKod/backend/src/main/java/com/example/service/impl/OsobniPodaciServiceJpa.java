package com.example.service.impl;

import com.example.dao.DonorRepository;
import com.example.dao.KorisnikRepository;
import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.enums.Role;
import com.example.rest.TrueBloodException;
import com.example.rest.dto.AzuriranjeDto;
import com.example.service.DonorService;
import com.example.service.KorisnikService;
import com.example.service.OsobniPodaciService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class OsobniPodaciServiceJpa implements OsobniPodaciService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    KorisnikService korisnikService;

    @Autowired
    DonorService donorService;

    @Autowired
    KorisnikRepository korisnikRepository;

    @Autowired
    DonorRepository donorRepository;

    @Override
    public boolean checkIfGivenAndLoggedInAreEqual(String Id) {

        String trenutnoUlogiranId = getLoggedInUserId();

        if (!trenutnoUlogiranId.equals(Id)) {
            throw new TrueBloodException("Korisnik smije pregledavati i ažurirati isključivo vlastite podatke!", HttpStatus.FORBIDDEN);
        } else {
            return true;
        }
    }


    private String getLoggedInUserId() {
       return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @Override
    public Korisnik updateKorisnik(AzuriranjeDto azuriranjeDto) {

        Korisnik korisnik = korisnikService.findById(azuriranjeDto.getId());
        if (korisnik.getRole() == Role.ADMIN ||korisnik.getRole() == Role.DJELATNIK) {
            if (korisnikRepository.existsByOibAndIdNot(azuriranjeDto.getOib(), azuriranjeDto.getId())) {
                throw new TrueBloodException(
                        "Korisnik sa OIB-om:  " + azuriranjeDto.getOib() + " već postoji. OIB mora biti jedinstven!",
                        HttpStatus.CONFLICT
                );

            }
            if (korisnikRepository.existsByEmailAndIdNot(azuriranjeDto.getEmail(), azuriranjeDto.getId())) {
                throw new TrueBloodException(
                        "Korisnik sa email-om:  " + azuriranjeDto.getEmail() + " već postoji. Email mora biti jedinstven!",
                        HttpStatus.CONFLICT
                );
            }
            Korisnik korisnikUpdated = korisnikUpdate(azuriranjeDto, korisnik);
            return korisnikRepository.save(korisnikUpdated);

        } else {

            if (korisnikRepository.existsByOibAndIdNot(azuriranjeDto.getOib(), azuriranjeDto.getId())) {

                throw new TrueBloodException(
                        "Donor sa OIB-om:  " + azuriranjeDto.getOib() + " već postoji. OIB mora biti jedinstven!",
                        HttpStatus.CONFLICT
                );
            }
            if (korisnikRepository.existsByEmailAndIdNot(azuriranjeDto.getEmail(), azuriranjeDto.getId())) {
                throw new TrueBloodException(
                        "Donor sa email-om:  " + azuriranjeDto.getEmail() + " već postoji. Email mora biti jedinstven!",
                        HttpStatus.CONFLICT
                );
            }
            Korisnik korisnikUpdated = korisnikUpdate(azuriranjeDto, korisnik);

            Donor donor = donorService.fetch(azuriranjeDto.getId());
            Donor donorUpdated = donorUpdate(azuriranjeDto, donor);
            donorRepository.save(donorUpdated);

            return korisnikUpdated;

        }


    }

    private Donor donorUpdate(AzuriranjeDto azuriranjeDto, Donor donor) {
        if (azuriranjeDto.getLozinka() == "") {
            return new Donor(
                    azuriranjeDto.getId(),
                    azuriranjeDto.getDatumRodenja(),
                    azuriranjeDto.getMjestoRodenja(),
                    azuriranjeDto.getMjestoZaposlenja(),
                    azuriranjeDto.getAdresaStanovanja(),
                    azuriranjeDto.getTelefonPrivatni(),
                    azuriranjeDto.getTelefonPoslovni(),
                    donor.isEnabled(),
                    azuriranjeDto.getOib(),
                    azuriranjeDto.getSpol(),
                    azuriranjeDto.getEmail(),
                    azuriranjeDto.getIme(),
                    donor.getRole(),
                    azuriranjeDto.getPrezime(),
                    donor.getPassword(),
                    donor.getKrvnaGrupa(),
                    donor.getZabrana(),
                    donor.getPotpunoRegistriran()
            );
        }
        return new Donor(
                azuriranjeDto.getId(),
                azuriranjeDto.getDatumRodenja(),
                azuriranjeDto.getMjestoRodenja(),
                azuriranjeDto.getMjestoZaposlenja(),
                azuriranjeDto.getAdresaStanovanja(),
                azuriranjeDto.getTelefonPrivatni(),
                azuriranjeDto.getTelefonPoslovni(),
                donor.isEnabled(),
                azuriranjeDto.getOib(),
                azuriranjeDto.getSpol(),
                azuriranjeDto.getEmail(),
                azuriranjeDto.getIme(),
                donor.getRole(),
                azuriranjeDto.getPrezime(),
                passwordEncoder.encode(azuriranjeDto.getLozinka()),
                donor.getKrvnaGrupa(),
                donor.getZabrana(),
                donor.getPotpunoRegistriran()
        );
    }

    private Korisnik korisnikUpdate(AzuriranjeDto azuriranjeDto, Korisnik korisnik) {
        if (azuriranjeDto.getLozinka() == "") {
            return new Korisnik(
                    azuriranjeDto.getId(),
                    azuriranjeDto.getDatumRodenja(),
                    azuriranjeDto.getMjestoRodenja(),
                    azuriranjeDto.getMjestoZaposlenja(),
                    azuriranjeDto.getAdresaStanovanja(),
                    azuriranjeDto.getTelefonPrivatni(),
                    azuriranjeDto.getTelefonPoslovni(),
                    korisnik.isEnabled(),
                    azuriranjeDto.getOib(),
                    azuriranjeDto.getSpol(),
                    azuriranjeDto.getEmail(),
                    azuriranjeDto.getIme(),
                    korisnik.getRole(),
                    azuriranjeDto.getPrezime(),
                    korisnik.getPassword()
            );
        }
        return new Korisnik(
                azuriranjeDto.getId(),
                azuriranjeDto.getDatumRodenja(),
                azuriranjeDto.getMjestoRodenja(),
                azuriranjeDto.getMjestoZaposlenja(),
                azuriranjeDto.getAdresaStanovanja(),
                azuriranjeDto.getTelefonPrivatni(),
                azuriranjeDto.getTelefonPoslovni(),
                korisnik.isEnabled(),
                azuriranjeDto.getOib(),
                azuriranjeDto.getSpol(),
                azuriranjeDto.getEmail(),
                azuriranjeDto.getIme(),
                korisnik.getRole(),
                azuriranjeDto.getPrezime(),
                passwordEncoder.encode(azuriranjeDto.getLozinka())
                );

    }
}
