package com.example.service.impl;

import com.example.domain.Donor;
import com.example.rest.dto.TrajniZdravstveniPodatciDto;
import com.example.service.RegistrationService;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceJpa implements RegistrationService {
    @Override
    public boolean checkTrajnaZabrana(TrajniZdravstveniPodatciDto zdravPodatci) {
        System.out.println(zdravPodatci.getKronicneBolesti());
        System.out.println(zdravPodatci.getDrogaIntravenski());
        System.out.println(zdravPodatci.getIzabraneTeskeBolesti());
        System.out.println(zdravPodatci.getOvisnost());
        System.out.println(zdravPodatci.getModnosM());
        System.out.println(zdravPodatci.getOdnosProstucija());
        System.out.println(zdravPodatci.getPromiskuitet());
        System.out.println(zdravPodatci.getSpolnoPrenosiveBolesti());
        System.out.println(zdravPodatci.getHivPozitivan());
        System.out.println(zdravPodatci.getSeksPartnnerRizSkupine());
        if(!zdravPodatci.getKronicneBolesti().equals("NE") ||
                !zdravPodatci.getDrogaIntravenski().equals("NE") ||
                !zdravPodatci.getIzabraneTeskeBolesti().equals("NE") ||
                !zdravPodatci.getOvisnost().equals("NE")||
                !zdravPodatci.getModnosM().equals("NE") ||
                !zdravPodatci.getOdnosProstucija().equals("NE") ||
                !zdravPodatci.getPromiskuitet().equals("NE") ||
                !zdravPodatci.getSpolnoPrenosiveBolesti().equals("NE") ||
                !zdravPodatci.getHivPozitivan().equals("NE") ||
                !zdravPodatci.getSeksPartnnerRizSkupine().equals("NE")) {

            return true;
        }
        return false;
    }
}
