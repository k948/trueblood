package com.example.service.impl;

import com.example.dao.DonorRepository;
import com.example.dao.KrvnaGrupaRepository;
import com.example.domain.Donor;
import com.example.domain.KrvnaGrupa;
import com.example.rest.TrueBloodException;
import com.example.rest.dto.DonacijaDonorDto;
import com.example.service.DonorService;
import com.example.service.KorisnikService;
import com.example.service.KrvnaGrupaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DonorServiceJpa implements DonorService {

    @Autowired
    private DonorRepository donorRepository;

    @Autowired
    private KrvnaGrupaService krvnaGrupaService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    KorisnikService korisnikService;


    @Override
    public Donor save(Donor donor) {
        return donorRepository.save(donor);
    }
    @Override
    public Donor storePassword(Donor donor, String password) {
        donor.setPassword(passwordEncoder.encode(password));
        return donorRepository.save(donor);
    }

    @Override
    public void updateDonor(Donor donor, DonacijaDonorDto donorPodatci) {
        donor.setIme(donorPodatci.getIme());
        donor.setPrezime(donorPodatci.getPrezime());

        if (korisnikService.existsByOibAndIdNot(donorPodatci.getOib(), donor.getId())) {
            throw new TrueBloodException(
                    "Korisnik sa OIB-om:  " + donorPodatci.getOib() + " već postoji. OIB mora biti jedinstven!",
                    HttpStatus.CONFLICT
            );
        }
        else
            donor.setOib(donorPodatci.getOib());

        donor.setMjestoZaposlenja(donorPodatci.getMjestoZaposlenja());
        donor.setMjestoRodenja(donorPodatci.getMjestoRodenja());
        donor.setAdresaStanovanja(donor.getAdresaStanovanja());
        donor.setTelefonPoslovni(donorPodatci.getTelefonPoslovni());
        donor.setTelefonPrivatni(donorPodatci.getTelefonPrivatni());
    }

    @Override
    public Optional<Donor> findByDonorId(long donorId) {
        return donorRepository.findById(donorId);
    }

    @Override
    public Donor fetch(long donorId) {
        return findByDonorId(donorId).orElseThrow(
                () -> new TrueBloodException("Ne postoji traženi donor ID!", HttpStatus.NOT_FOUND)
        );
    }
    @Override
    public Donor registerNewDonorAccount(Donor donor) {
        donorRepository.save(donor);
        return donor;
    }
    @Override
    public Donor setDonorKgrupa(Donor donor, String oznaka){

        KrvnaGrupa krvnaGrupa = krvnaGrupaService.findByOznakaKgrupe(oznaka);
        donor.setKrvnaGrupa(krvnaGrupa);
            return donor;
    }


}
