package com.example;

import com.example.dao.DarivanjeRepository;
import com.example.dao.DonorRepository;
import com.example.dao.KorisnikRepository;
import com.example.dao.KrvnaGrupaRepository;
import com.example.domain.Darivanje;
import com.example.domain.Donor;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.domain.enums.Role;
import com.example.service.DonorService;
import com.example.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.sql.Date;

@SpringBootApplication
@EnableScheduling
public class TrueBloodApplication implements CommandLineRunner{
	public static void main(String[] args) {
		SpringApplication.run(TrueBloodApplication.class, args);
	}

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	KorisnikRepository korisnikRepository;

	@Autowired
	DonorRepository donorRepository;

	@Autowired
	KrvnaGrupaRepository krvnaGrupaRepository;

	@Autowired
	KorisnikService korisnikService;

	@Autowired
	DonorService donorService;

	@Autowired
	DarivanjeRepository darivanjeRepository;

	@Override
	public void run(String... args) throws Exception {
/*

		this.krvnaGrupaRepository.save(new KrvnaGrupa("A+", 0.0, 10.0, 80.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("A-", 0.0, 20.0, 70.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("B+", 20.0, 30.0, 60.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("B-", 50.0, 10.0, 70.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("AB+", 30.0, 15.0, 75.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("AB-", 20.0, 30.0, 70.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("0+", 10.0, 10.0, 80.0));
		this.krvnaGrupaRepository.save(new KrvnaGrupa("0-", 80.0, 10.0, 70.0));


		//ONLY ADMIN HARDCODED HERE !
		Korisnik admin = new Korisnik("11111311111", "Ž", "admin@hrt.hr", "Lovro", "Juric");
		admin.setPassword(passwordEncoder.encode("admin"));
		admin.setRole(Role.ADMIN);
		korisnikRepository.save(admin);

		Donor donor = new Donor("11111111111", "M", "mislav.peric7@gmail.com", "Sanković", "Ivan", false);
		donor.setPassword(passwordEncoder.encode("donor"));
		donor.setRole(Role.DONOR);
		donor.setDatumRodenja(Date.valueOf("1970-2-2"));
		donorService.setDonorKgrupa(donor, "B+");
		donorRepository.save(donor);

		Donor donor2 = new Donor("11111111112", "M", "boledn@hrt.hr", "Jurica", "Babic", false);
		donor2.setPassword(passwordEncoder.encode("jurica"));
		donor2.setRole(Role.DONOR);
		donorService.setDonorKgrupa(donor2, "A+");
		donor2.setDatumRodenja(Date.valueOf("2000-2-2"));
		donorRepository.save(donor2);

		Korisnik djelatnik = new Korisnik("12345678901", "M", "matej.durin@fer.hr", "Matej", "Durin");
		djelatnik.setPassword(passwordEncoder.encode("djelatnik"));
		djelatnik.setRole(Role.DJELATNIK);
		korisnikRepository.save(djelatnik);

		darivanjeRepository.save(new Darivanje(djelatnik,donor, false, "Odranska 8"));
		darivanjeRepository.save(new Darivanje(djelatnik,donor, false, "Vukovarska 3"));
		darivanjeRepository.save(new Darivanje(djelatnik,donor, false, "Unska 3"));

*/
	}
}
