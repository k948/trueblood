package com.example.domain.enums;

public enum Role {
    DONOR, ADMIN, DJELATNIK;

    public static String toString(Role role) {
        return switch (role) {
            case ADMIN -> "ADMIN";
            case DONOR -> "DONOR";
            default -> "DJELATNIK";
        };

    }
}
