package com.example.domain;

import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity
public class VerificationToken {

    @javax.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sif;

    private String token;

    @ManyToOne(targetEntity = Korisnik.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "korisnik_id")
    private Korisnik korisnik;

    //private Date expiryDate;
    //java.sql.Timestamp sqlTS = new java.sql.Timestamp(expiryDate.getTime());
    private java.sql.Timestamp dateEx;

    public VerificationToken() {

    }

    private Timestamp calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);   //kasnije handleati slucaj da se obrise korisnik koji nije zavrsio registraciju nakon sto istekne vrijeme
        Date datePom = new Date(cal.getTime().getTime());
        return new Timestamp(datePom.getTime());
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik user) {
        this.korisnik = user;
    }

    public Timestamp getDateEx() {
        return dateEx;
    }

    public void setDateEx(Timestamp dateEx) {
        this.dateEx = dateEx;
    }

    public VerificationToken(String token, Korisnik korisnik) {
        this.token = token;
        this.korisnik = korisnik;
    }
    public VerificationToken(Korisnik korisnik) {
        this.korisnik = korisnik;
        this.token = UUID.randomUUID().toString();
        dateEx = calculateExpiryDate();
    }
}