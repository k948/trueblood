package com.example.domain;

import com.example.domain.enums.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;

@Entity
@PrimaryKeyJoinColumn(name = "donorId")
public class Donor extends Korisnik {


    @ManyToOne
    @JoinColumn(name = "oznakaKgrupe", nullable = true)
    private KrvnaGrupa krvnaGrupa;

    //@0neToOne
    //private String oznakaKgrupe;


    private Boolean zabrana;

    private Boolean potpunoRegistriran;

    //ovo sve između ta dva komentara će ići u entitet Korisnik (prvo treba promjenit remote bazu, napravit će Matej)

    //private java.util.Date datumRodenja;

    //private String mjestoRodenja;

    //private String mjestoZaposlenja;

    //private String adresaStanovanja;

    //private String telefonPrivatni;

    //private String telefonPoslovni;

//    public java.util.Date getDatumRodenja() {
//        return datumRodenja;
//    }
//
//    public void setDatumRodenja(Date datumRodenja) {
//        this.datumRodenja = datumRodenja;
//    }
//
//    public String getMjestoRodenja() {
//        return mjestoRodenja;
//    }
//
//    public void setMjestoRodenja(String mjestoRodenja) {
//        this.mjestoRodenja = mjestoRodenja;
//    }
//
//    public String getMjestoZaposlenja() {
//        return mjestoZaposlenja;
//    }
//
//    public void setMjestoZaposlenja(String mjestoZaposlenja) {
//        this.mjestoZaposlenja = mjestoZaposlenja;
//    }
//
//    public String getAdresaStanovanja() {
//        return adresaStanovanja;
//    }
//
//    public void setAdresaStanovanja(String adresaStanovanja) {
//        this.adresaStanovanja = adresaStanovanja;
//    }
//
//    public String getTelefonPrivatni() {
//        return telefonPrivatni;
//    }
//
//    public void setTelefonPrivatni(String telefonPrivatni) {
//        this.telefonPrivatni = telefonPrivatni;
//    }
//
//    public String getTelefonPoslovni() {
//        return telefonPoslovni;
//    }
//
//    public void setTelefonPoslovni(String telefonPoslovni) {
//        this.telefonPoslovni = telefonPoslovni;
//    }

    //komentar


    public Donor(Long id, Date datumRodenja, String mjestoRodenja, String mjestoZaposlenja, String adresaStanovanja, String telefonPrivatni, String telefonPoslovni, boolean enabled, @NotNull @Size(min = 11, max = 11) String oib, @NotNull String spol, @NotNull @Email String email, @NotNull String ime, Role role, @NotNull String prezime, String password, KrvnaGrupa krvnaGrupa, Boolean zabrana, Boolean potpunoRegistriran) {
        super(id, datumRodenja, mjestoRodenja, mjestoZaposlenja, adresaStanovanja, telefonPrivatni, telefonPoslovni, enabled, oib, spol, email, ime, role, prezime, password);
        this.krvnaGrupa = krvnaGrupa;
        this.zabrana = zabrana;
        this.potpunoRegistriran = potpunoRegistriran;
    }

    public Donor(String oib, String spol, String email, String ime, String prezime, Boolean zabrana) {
        super(oib, spol, email, ime, prezime);
        this.zabrana = zabrana;
    }
    public Donor(){

    }


    public void setKrvnaGrupa(KrvnaGrupa krvnaGrupa) {
        this.krvnaGrupa = krvnaGrupa;
    }

    public KrvnaGrupa getKrvnaGrupa() {
        return krvnaGrupa;
    }

    public Boolean getZabrana() {
        return zabrana;
    }

    public void setZabrana(Boolean zabrana) {
        this.zabrana = zabrana;
    }

    public Boolean getPotpunoRegistriran() {
        return potpunoRegistriran;
    }

    public void setPotpunoRegistriran(Boolean potpunoRegistriran) {
        this.potpunoRegistriran = potpunoRegistriran;
    }



}

