package com.example.domain;

import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class KrvnaGrupa {

    @javax.persistence.Id
    @Id
    private String oznakaKgrupe;


    @NotNull
    Double zaliha;

    @NotNull
    Double donjaGranica;

    @NotNull
    Double gornjaGranica;

    public KrvnaGrupa(String oznakaKgrupe, Double zaliha, Double donjaGranica, Double gornjaGranica) {
        this.oznakaKgrupe = oznakaKgrupe;
        this.zaliha = zaliha;
        this.donjaGranica = donjaGranica;
        this.gornjaGranica = gornjaGranica;
    }

    public KrvnaGrupa() {

    }

    public String getOznakaKgrupe() {
        return oznakaKgrupe;
    }

    public void setOznakaKgrupe(String oznakaKgrupe) {
        this.oznakaKgrupe = oznakaKgrupe;
    }

    public Double getZaliha() {
        return zaliha;
    }

    public void setZaliha(Double zaliha) {
        this.zaliha = zaliha;
    }

    public Double getDonjaGranica() {
        return donjaGranica;
    }

    public void setDonjaGranica(Double donjaGranica) {
        this.donjaGranica = donjaGranica;
    }

    public Double getGornjaGranica() {
        return gornjaGranica;
    }

    public void setGornjaGranica(Double gornjaGranica) {
        this.gornjaGranica = gornjaGranica;
    }
}
