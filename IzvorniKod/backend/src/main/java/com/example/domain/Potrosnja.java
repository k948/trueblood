package com.example.domain;

import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Potrosnja {
    @javax.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sifraPotrosnje;

    @ManyToOne
    @JoinColumn(name = "djelatnikId")
    private Korisnik djelatnik;

    @ManyToOne
    @JoinColumn(name = "oznakaKgrupe")
    private KrvnaGrupa krvnaGrupa;

    private Timestamp datumVrijeme;

    private Double kolicina;

    public Potrosnja(Korisnik djelatnik, KrvnaGrupa krvnaGrupa, Double kolicina) {
        Calendar cal = Calendar.getInstance();
        datumVrijeme = new Timestamp( (new Date(cal.getTime().getTime())).getTime());

        this.djelatnik = djelatnik;
        this.krvnaGrupa = krvnaGrupa;
        this.kolicina = kolicina;
    }
}
