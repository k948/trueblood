package com.example.domain;

import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

@Entity
public class Darivanje {
    @javax.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sifraDarivanja;

    @ManyToOne
    @JoinColumn(name="djelatnikId")
    private Korisnik djelatnik;

    @ManyToOne
    @JoinColumn(name="donorId")
    private Donor donor;

    private Timestamp datumVrijeme;

    private Boolean uspjesno;

    private String lokacija;

    //koji put po redu je donor uspješno donirao krv
    private Integer brojDarivanja;

    private Double kolicina;

    public Darivanje(){
        Calendar cal = Calendar.getInstance();
        datumVrijeme = new Timestamp( (new Date(cal.getTime().getTime())).getTime());
        uspjesno = false;
        kolicina = 0.0;
        brojDarivanja = -1;
    }

    public Darivanje(Korisnik djelatnik, Donor donor, Boolean uspjesno, String lokacija) {
        this();
        this.djelatnik = djelatnik;
        this.donor = donor;
        this.uspjesno = uspjesno;
        this.lokacija = lokacija;
    }

    public Long getSifraDarivanja() {
        return sifraDarivanja;
    }

    public void setSifraDarivanja(Long sifraDarivanja) {
        this.sifraDarivanja = sifraDarivanja;
    }

    public Korisnik getDjelatnik() {
        return djelatnik;
    }

    public void setDjelatnik(Korisnik djelatnik) {
        this.djelatnik = djelatnik;
    }

    public Donor getDonor() {
        return donor;
    }

    public void setDonor(Donor donor) {
        this.donor = donor;
    }

    public Timestamp getDatumVrijeme() {
        return datumVrijeme;
    }

    public void setDatumVrijeme(Timestamp datumVrijeme) {
        this.datumVrijeme = datumVrijeme;
    }

    public Boolean getUspjesno() {
        return uspjesno;
    }

    public void setUspjesno(Boolean uspjesno) {
        this.uspjesno = uspjesno;
    }

    public String getLokacija() {
        return lokacija;
    }

    public void setLokacija(String lokacija) {
        this.lokacija = lokacija;
    }

    public Integer getBrojDarivanja() {
        return brojDarivanja;
    }

    public void setBrojDarivanja(Integer brojDarivanja) {
        this.brojDarivanja = brojDarivanja;
    }

    public Double getKolicina() {
        return kolicina;
    }

    public void setKolicina(Double kolicina) {
        this.kolicina = kolicina;
    }
}
