package com.example.domain;


import com.example.domain.enums.Role;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Korisnik {

    @javax.persistence.Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date datumRodenja;

    private String mjestoRodenja;

    private String mjestoZaposlenja;

    private String adresaStanovanja;

    private String telefonPrivatni;

    private String telefonPoslovni;

    public Date getDatumRodenja() {
        return datumRodenja;
    }

    public void setDatumRodenja(Date datumRodenja) {
        this.datumRodenja = datumRodenja;
    }

    public String getMjestoRodenja() {
        return mjestoRodenja;
    }

    public void setMjestoRodenja(String mjestoRodenja) {
        this.mjestoRodenja = mjestoRodenja;
    }

    public String getMjestoZaposlenja() {
        return mjestoZaposlenja;
    }

    public void setMjestoZaposlenja(String mjestoZaposlenja) {
        this.mjestoZaposlenja = mjestoZaposlenja;
    }

    public String getAdresaStanovanja() {
        return adresaStanovanja;
    }

    public void setAdresaStanovanja(String adresaStanovanja) {
        this.adresaStanovanja = adresaStanovanja;
    }

    public String getTelefonPrivatni() {
        return telefonPrivatni;
    }

    public void setTelefonPrivatni(String telefonPrivatni) {
        this.telefonPrivatni = telefonPrivatni;
    }

    public String getTelefonPoslovni() {
        return telefonPoslovni;
    }

    public void setTelefonPoslovni(String telefonPoslovni) {
        this.telefonPoslovni = telefonPoslovni;
    }

    private boolean enabled;

    @Column(unique=true, nullable=false)
    @NotNull
    @Size(min=11, max=11)
    private String oib;

    @NotNull
    private String spol;

    @Column(unique=true, nullable=false)
    @NotNull
    @Email
    private String email;

    @NotNull
    private String ime;

    @Enumerated(EnumType.STRING)
    private Role role;

    @NotNull
    private String prezime;

    private String password;

    public Korisnik(String oib, String spol, String email, String ime, String prezime) {
        this.oib = oib;
        this.spol = spol;
        this.email = email;
        this.ime = ime;
        this.prezime = prezime;
        this.enabled = true;
    }

    public Korisnik(Long id, Date datumRodenja, String mjestoRodenja, String mjestoZaposlenja, String adresaStanovanja, String telefonPrivatni, String telefonPoslovni, boolean enabled, @NotNull @Size(min = 11, max = 11) String oib, @NotNull String spol, @NotNull @Email String email, @NotNull String ime, Role role, @NotNull String prezime, String password) {
        this.id = id;
        this.datumRodenja = datumRodenja;
        this.mjestoRodenja = mjestoRodenja;
        this.mjestoZaposlenja = mjestoZaposlenja;
        this.adresaStanovanja = adresaStanovanja;
        this.telefonPrivatni = telefonPrivatni;
        this.telefonPoslovni = telefonPoslovni;
        this.enabled = enabled;
        this.oib = oib;
        this.spol = spol;
        this.email = email;
        this.ime = ime;
        this.role = role;
        this.prezime = prezime;
        this.password = password;
    }

    public Korisnik(){
        this.enabled = true;
    }




    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public Role getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOib() {
        return oib;
    }

    public void setOib(String oib) {
        this.oib = oib;
    }

    public String getSpol() {
        return spol;
    }

    public void setSpol(String spol) {
        this.spol = spol;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setEnabled(boolean b) {
        this.enabled = b;
    }

    public boolean isEnabled() {
        return enabled;
    }

}
