package com.example.security;

import com.example.domain.Korisnik;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
public class UserDetail implements UserDetails {

    Korisnik korisnik;

    public UserDetail(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList("ROLE_"+this.korisnik.getRole());
    }

    @Override
    public String getPassword() {
        return this.korisnik.getPassword();
    }

    @Override
    public String getUsername() {
        return this.korisnik.getId().toString();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    //tu pripazit oko toga
    @Override
    public boolean isEnabled() {
        return korisnik.isEnabled();
    }
    // Korisnik korisnik = ((UserDetail)(SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getKorisnik();
}
