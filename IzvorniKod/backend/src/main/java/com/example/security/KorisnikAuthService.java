package com.example.security;


import com.example.dao.KorisnikRepository;
import com.example.domain.Korisnik;
import com.example.rest.TrueBloodException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class KorisnikAuthService implements UserDetailsService {

    @Autowired
    KorisnikRepository korisnikRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        try {
            Korisnik korisnik = korisnikRepository.findById(Long.parseLong(username)).orElseThrow(
                    () -> new UsernameNotFoundException("Korisnik s traženim username-om ne postoji!")
            );
            return new UserDetail(korisnik);
        }
        catch(Exception e) {
            throw new TrueBloodException("Provjerite format unosa! Id se sastoji isključivo od znamenki", HttpStatus.BAD_REQUEST);
        }
    }

}