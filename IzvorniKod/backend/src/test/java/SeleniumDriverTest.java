
import static org.junit.Assert.*;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumDriverTest {

    @Test
    public void loginGoodCredentialsTest() {

        WebDriver driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://localhost:3000/");


        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("1");

        element = driver.findElement(By.name("password"));
        element.sendKeys("admin");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        String redirectURL = driver.getCurrentUrl();

        WebElement elementOnNextSite = driver.findElement(By.id("1"));

        assertEquals(elementOnNextSite.isDisplayed(), true);


        driver.quit();

    }

    @Test
    public void testLoginBadCredentials() {

        System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Chrome Driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://r2d2.zemris.fer.hr/PROGIapp/");


        WebElement element = driver.findElement(By.name("username"));
        element.sendKeys("testadmin");

        element = driver.findElement(By.name("pass"));
        element.sendKeys("22345");

        driver.findElement(By.cssSelector("input[type='submit']")).click();

        String redirURL = driver.getCurrentUrl();

        boolean compRes= redirURL.contains("coordinatorDashboard");

        assertEquals(compRes, false);


        driver.quit();

    }

}
