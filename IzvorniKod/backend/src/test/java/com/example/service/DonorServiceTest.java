package com.example.service;


import com.example.dao.DonorRepository;
import com.example.dao.KorisnikRepository;
import com.example.domain.Donor;
import com.example.domain.KrvnaGrupa;
import com.example.rest.TrueBloodException;
import com.example.rest.dto.DonacijaDonorDto;
import com.example.service.DonorService;
import com.example.service.impl.DonorServiceJpa;
import com.example.service.impl.KorisnikServiceJpa;
import com.example.service.impl.KrvnaGrupaServiceJpa;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Optional;

import static java.time.LocalDateTime.now;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DonorServiceTest {

    @Mock
    private DonorRepository donorRepository;

    @Mock
    private KorisnikRepository korisnikRepository;

    @Mock
    private KorisnikServiceJpa korisnikService;

    @Mock
    private KrvnaGrupaServiceJpa krvnaGrupaService;

    @InjectMocks
    private DonorService donorService = new DonorServiceJpa();




    @Test
    public void saveDonor() {

        Donor donor = new Donor();

        when(donorRepository.save(donor)).thenReturn(donor);

        assertEquals(donor, donorService.save(donor));
    }


    @Test
    public void whenExisitingDonorIdGiven_thenReturnDonor() {

        Donor donor = new Donor();

        when(donorRepository.findById(1L)).thenReturn(of(donor));

        assertEquals(of(donor), donorService.findByDonorId(1L));

    }

    @Test
    public void whenNonExisitingDonorIdGiven_thenReturnNull() {

        when(donorRepository.findById(1L)).thenReturn(null);

        assertEquals(null, donorService.findByDonorId(1L));

    }

    @DisplayName("Should throw TrueBloodException because OIB is not unique")
    @Test(expected = TrueBloodException.class)
    public void whenUpdatingDonorProfileAndOibTaken_thenThrowsException() {

        Donor donor = new Donor();
        donor.setId(1L);
        DonacijaDonorDto donacijaDonorDto = new DonacijaDonorDto();
        donacijaDonorDto.setOib("99999999999");

        when(korisnikService.existsByOibAndIdNot("99999999999", 1L)).thenReturn(true);

        donorService.updateDonor(donor, donacijaDonorDto);


    }

    @DisplayName("Should return donor with blood group information")
    @Test
    public void whenUpdatingDonorBloodGroup_thenReturnsDonor() {

        Donor donor = new Donor();

        Donor donor_with_bloodGroup = new Donor();
        donor_with_bloodGroup.setKrvnaGrupa(new KrvnaGrupa("A+", 100.0, 10.0, 90.0));

        String oznaka = "A+";
        when(krvnaGrupaService.findByOznakaKgrupe(oznaka)).thenReturn(
                new KrvnaGrupa("A+", 100.0, 10.0, 90.0)
        );

        var result =  donorService.setDonorKgrupa(donor, "A+").getKrvnaGrupa();

        assertEquals(donor_with_bloodGroup.getKrvnaGrupa().getOznakaKgrupe(), result.getOznakaKgrupe());
        assertEquals(donor_with_bloodGroup.getKrvnaGrupa().getDonjaGranica(), result.getDonjaGranica());
        assertEquals(donor_with_bloodGroup.getKrvnaGrupa().getGornjaGranica(), result.getGornjaGranica());
        assertEquals(donor_with_bloodGroup.getKrvnaGrupa().getZaliha(), result.getZaliha());
    }

    @DisplayName("Should throw TrueBloodException because given blood type is not valid")
    @Test(expected = TrueBloodException.class)
    public void whenUpdatingDonorBloodGroupNotValid_thenThrowsException() {

        Donor donor = new Donor();
        String oznaka = "notValid";

        when(krvnaGrupaService.findByOznakaKgrupe(oznaka)).thenThrow(TrueBloodException.class);

        donorService.setDonorKgrupa(donor, oznaka);
    }











}
