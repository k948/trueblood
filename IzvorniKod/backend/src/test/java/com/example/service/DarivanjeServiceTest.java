package com.example.service;

import com.example.dao.*;
import com.example.domain.*;
import com.example.service.impl.*;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.util.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DarivanjeServiceTest {

    @Mock
    DonorService donorService;

    @Mock
    DarivanjeRepository darivanjeRepository;

    @InjectMocks
    DarivanjeService darivanjeService = new DarivanjeServiceJpa();


    @DisplayName("Should return list of all donations")
    @Test
    public void findAllDarivanja() {
        when(darivanjeRepository.findAll()).thenReturn(newArrayList(new Darivanje(), new Darivanje()));
        assertEquals(2, darivanjeService.findAll().size());


    }


}
