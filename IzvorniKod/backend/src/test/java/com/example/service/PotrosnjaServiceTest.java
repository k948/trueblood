package com.example.service;

import com.example.dao.KrvnaGrupaRepository;
import com.example.dao.PotrosnjaRepository;
import com.example.domain.Korisnik;
import com.example.domain.KrvnaGrupa;
import com.example.domain.Potrosnja;
import com.example.rest.TrueBloodException;
import com.example.service.impl.KrvnaGrupaServiceJpa;
import com.example.service.impl.PotrosnjaServiceJpa;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PotrosnjaServiceTest {

    @Mock
    KrvnaGrupaServiceJpa krvnaGrupaService;

    @Mock
    PotrosnjaRepository potrosnjaRepository;

    @Mock
    KrvnaGrupaRepository krvnaGrupaRepository;

    @InjectMocks
    PotrosnjaService potrosnjaService = new PotrosnjaServiceJpa();


    @DisplayName("Should throw TrueBloodException because available blood quantity is not enough for sending it")
    @Test(expected = TrueBloodException.class)
    public void whenRequestedQuantityGreaterThanAvailable_thenThrowsException() {

        Potrosnja potrosnja = mock(Potrosnja.class);
        Korisnik djelatnik = mock(Korisnik.class);
        String oznakaKgrupe = "A+";
        double kolicina = 80.0;
        KrvnaGrupa krvnaGrupa = new KrvnaGrupa("A+", 70.0, 10.0, 60.0);

        when(krvnaGrupaService.findByOznakaKgrupe(oznakaKgrupe)).thenReturn(krvnaGrupa);


        doThrow (TrueBloodException.class).when(krvnaGrupaService).updateVolume(oznakaKgrupe, -kolicina);

        verify(potrosnjaRepository, times(0)).save(potrosnja);

        potrosnjaService.zabiljeziPotrosnju(djelatnik, oznakaKgrupe , kolicina);






    }

    @DisplayName("Should send wanted blood quantity")
    @Test
    public void whenRequestedQuantityLowerThanAvailable_thenSendRequestedQuantity() {

        Korisnik djelatnik = mock(Korisnik.class);
        String oznakaKgrupe = "A+";
        double kolicina = 80.0;
        KrvnaGrupa krvnaGrupa = new KrvnaGrupa("A+", 100.0, 10.0, 60.0);

        when(krvnaGrupaService.findByOznakaKgrupe(oznakaKgrupe)).thenReturn(krvnaGrupa);
        doNothing().when(krvnaGrupaService).updateVolume(oznakaKgrupe, -kolicina);

        potrosnjaService.zabiljeziPotrosnju(djelatnik, oznakaKgrupe , kolicina);

        verify(potrosnjaRepository, times(1)).save(any());


    }


}
