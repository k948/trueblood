package com.example.controller;

import com.example.service.impl.KorisnikServiceJpa;
import com.example.service.impl.OsobniPodaciServiceJpa;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class OsobniPodaciControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OsobniPodaciServiceJpa osobniPodaciService;


    @MockBean
    KorisnikServiceJpa korisnikService;

    @Test
    public void whenUserWithoutAuthorities_thanReturns401() throws Exception{

        mvc.perform(get("/osobni-podaci/pregled/3")
                .accept(MediaType.APPLICATION_JSON)
                .content("100")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());


    }




}
