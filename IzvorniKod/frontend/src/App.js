import {Route, Routes, BrowserRouter} from "react-router-dom"
import Layout from './components/layout/Layout'
import Home from './pages/Home'
import Admin from './pages/Admin'
import Djelatnik from './pages/Djelatnik'
import Donor from './pages/Donor'
import Login from './components/home/Login' 
import Register from './components/home/Register' 
import { useContext, useEffect, useState } from "react"
import CurrentUserContext from "./components/store/CurrentUserContext"
import { useNavigate } from "react-router-dom";
import LoginBasicAuth from "./components/home/LoginBasicAuth"
import RegFinish from "./pages/RegFinish"
import PrivateRoute from "./components/store/PrivateRoute"
import Donacija from "./pages/Donacija"
import SendDonorId from "./components/novoDoniranje/SendDonorId"
import MjestoDonacije from "./components/novoDoniranje/MjestoDonacije"
import DjelatnikRegDonor from "./pages/DjelatnikRegDonor"
import Obavijest from "./pages/Obavijest"
import SlanjeKolicina from "./components/novoDoniranje/SlanjeKolicina"
import SlanjeKrvi from "./pages/SlanjeKrvi"
import Granice from "./components/admin/Granice"
import AdminRegDjelatnik from "./components/admin/AdminRegDjelatnik"
import Deaktiviraj from "./components/admin/Deaktiviraj"
import Povijest from "./components/donor/Povijest"
import DonorPodaci from "./components/donor/DonorPodaci"
import FadeLoader from "react-spinners/FadeLoader";
import { css } from "@emotion/react";
import './App.css'

import RequireAuth from "./components/store/RequireAuth"


//implementirati neki security u okviru routera da ne dopusta npr da donor napise /admin i da ude
function App() {
  const userContext = useContext(CurrentUserContext);
  const navigate = useNavigate();

  const [state, setState] = useState(false);
  
  const [loading, setLoading] = useState(true);

  useEffect(() => {                       //OTKOMENTIRATI NAKON STO SPOJIMO S BACKENDOM
    fetch(process.env.REACT_APP_API +'/user', {    
        method: 'GET',                        
        credentials: 'include'
      }
    ).then(function(response) {
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.json();
      })
    .then((data) => {
      userContext.setUserType(data.korisnik.role); //ovo rjesiti da sprema kontekst, jebe nas to 
      userContext.setCurrentUserName(data.korisnik.ime + " " + data.korisnik.prezime);
      userContext.setCurrentUserId(data.korisnik.id);
      if (data.korisnik.role=="DONOR") {
        userContext.setDonorId(data.korisnik.id);
      }
      //userContext.setAuthCheckAllowed(true);
      setLoading(false);
    })
    .catch(function(error) {
      setLoading(false);
      //navigate('/');
    })

    //dohvacam krvne grupe da mogu sredit onu poruku sto se vrti u navbaru 
    fetch(process.env.REACT_APP_API +'/blood-groups', {    
      method: 'GET',                        
      credentials: 'include'
    }).then(function(response) {
      if (!response.ok) {
          throw Error(response.statusText);
      }
      return response.json();
    }).then((data)=>{
      userContext.setThisKGrupe(data);
      setState(true);
    })
  }, []);
                                        //u slucaju da ovo ne radi, onda umjesto navigate replace to samo staviti komponentu i tjt


                             //provjeri relativnost redirectTo ako se sjebe nesto
  if(state==false){
    return(
      <div className="back">
        <div className="back-2">
          <FadeLoader color="white" loading={true} size={150}/>
        </div>
      </div>
    )
  }   

  return (
      <Layout>
          <Routes>
            <Route path='/' element={<Home/>} />    
            <Route path='/admin' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="ADMIN"><Admin/></RequireAuth>}/>
            <Route path='/djelatnik' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><Djelatnik/></RequireAuth>}/>
            <Route path='/donor' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DONOR"><Donor/></RequireAuth>}/>
            <Route path='/prijava' element={<RequireAuth userMustNotBeLoggedIn={true} loadingStill={loading} permittedUser=""><Login/></RequireAuth>}/>
            <Route path='/confirm-account' element={<RequireAuth userMustNotBeLoggedIn={true} loadingStill={loading} permittedUser=""><RegFinish/></RequireAuth>}/>
            <Route path='/registracija' element={<RequireAuth userMustNotBeLoggedIn={true} loadingStill={loading} permittedUser=""><Register/></RequireAuth>}/>
            <Route path='/djelatnik/donacija' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><Donacija/></RequireAuth>}/>
            <Route path='/djelatnik/mjesto-donacije' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><MjestoDonacije/></RequireAuth>}/>
            <Route path='/djelatnik/reg-donor' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><DjelatnikRegDonor/></RequireAuth>}/>
            <Route path='/obavijest' element={<Obavijest/>}/>
            <Route path='/djelatnik/slanje-kolicina' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><SlanjeKolicina/></RequireAuth>}/>
            <Route path='/djelatnik/slanje-krvi' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DJELATNIK"><SlanjeKrvi/></RequireAuth>}/>
            <Route path='/admin/granice' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="ADMIN"><Granice/></RequireAuth>}/>
            <Route path='/admin/reg-djelatnik' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="ADMIN"><AdminRegDjelatnik/></RequireAuth>}/>
            <Route path='/admin/racuni' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="ADMIN"><Deaktiviraj/></RequireAuth>}/>
            <Route path='/donor/povijest-darivanja' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DONOR"><Povijest/></RequireAuth>}/>
            <Route path='/osobni-podaci' element={<RequireAuth userMustNotBeLoggedIn={false} loadingStill={loading} permittedUser="DONOR DJELATNIK"><DonorPodaci/></RequireAuth>}/>
          </Routes>
      </Layout>
  );
}

export default App;

