import React from 'react';
import { useContext } from 'react';
import CurrentUserContext from '../store/CurrentUserContext';
import * as Yup from 'yup'
import { useFormik} from "formik";
import { useNavigate } from "react-router-dom";


function SendDonorId (){
    const navigate = useNavigate();
    const loginCtx = useContext(CurrentUserContext)

    const formik = useFormik({
        initialValues:{
            donorId: ""
        },
        validationSchema: Yup.object({
            donorId: Yup.string().required("Molimo unesite donorov ID") //number
        }),
        onSubmit: (e) => {
            //prvi fetch da provjerim da taj id uopce postoji 
            /*fetch(process.env.REACT_APP_API + '/osobni-podaci/pregled/' + e.donorId,
            {
                method: 'GET',
                credentials: "include"
            }).then(function(response){
                if (!response.ok) {   
                    throw response;  
                }
            })
            .catch(function(error){
                error.json().then(err => {alert(err.message);})
            });*/

            const fetchURL = process.env.REACT_APP_API + '/djelatnik/donacija';

            sessionStorage.setItem("donorId", e.donorId);
            
            var object ={
                donorId: formik.values.donorId
            }
    
            var formBody = [];
            for (var property in object) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(object[property]);
            formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            
            fetch(fetchURL, 
            {
                method: 'POST',
                credentials:'include',
                body: formBody,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'  
                }
            }).then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response.json();
            }}).then((data)=>{
                    sessionStorage.setItem("donorObject", JSON.stringify(data));
                    loginCtx.setDonorId(data.id);
                    loginCtx.setIme(data.ime);
                    loginCtx.setPrezime(data.prezime);
                    loginCtx.setOib(data.oib);
                    loginCtx.setSpol(data.spol);
                    loginCtx.setMjestoRodenja(data.mjestoRodenja);
                    loginCtx.setMjestoZaposlenja(data.mjestoZaposlenja);
                    loginCtx.setAdresaStanovanja(data.adresaStanovanja);
                    loginCtx.setTelefonPrivatni(data.telefonPrivatni);
                    loginCtx.setTelefonPoslovni(data.telefonPoslovni);
                    loginCtx.setDatumRodenja(data.datumRodenja);
                    if(data.krvnaGrupa!=null){
                        loginCtx.setKGrupa(data.krvnaGrupa.oznakaKgrupe);
                    }
                    navigate('/djelatnik/donacija')
            })
            .catch(function(error){
                error.json().then(err => {alert(err.message)})
            });
        }
    });

    return (  
        <div className="card w-50">
        <div className="card-body">
        <h5 class="card-title text-center">NOVO DONIRANJE</h5>
        <form onSubmit={formik.handleSubmit}>
            <div>
                <input
                className="form-control"
                    id="donorId"
                    name="donorId"
                    type="string"
                    placeholder="donor ID"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.donorId}
                />
            </div>
                {formik.errors.donorId && formik.touched.donorId ? <p className='error'>{formik.errors.donorId}</p> : null}
                <br></br>
            <div>   
                <br></br>
                 <button  className="btn btn-primary center-btn-2" type="submit">
                    Pošalji
                </button>
            </div>
            <br></br>
        </form>
        </div>
        </div>
    )
}

export default SendDonorId;