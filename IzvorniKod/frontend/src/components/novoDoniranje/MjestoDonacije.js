import { Button } from 'bootstrap'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import { useNavigate } from 'react-router'
import { useContext, useEffect} from 'react'
import CurrentUserContext from '../store/CurrentUserContext'
import Aos from 'aos';
import 'aos/dist/aos.css';


function MjestoDonacije(){
    const loginCtx = useContext(CurrentUserContext)
    
    var error=null;
    const navigate = useNavigate();
    
    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const formik = useFormik({
        initialValues:{
            mjestoDonacije: "" 
        },
        validationSchema: Yup.object({
            mjestoDonacije: Yup.string().required("Molimo unesite lokaciju doniranja")

        }),
        onSubmit: (e) => {
            console.log(formik.values);
            fetch(process.env.REACT_APP_API + '/djelatnik/donacija-mjesto',  //promjeni u /djelatnik/provedena-donacija i preimenuj file u ProvedenaDonacija.js
            {
                method: 'POST',
                credentials: "include",
                body: JSON.stringify(e),
                headers: {
                    'Content-Type': 'application/json' ,
                }   
                
            }).then(function(response){
                if (!response.ok) {   
                    throw Error(response.body);  
            } else {
                return response.json;
            }}).then(()=>{
                navigate('/success');
            })
            .catch(function(error){
                console.log(error)
            }); 
        }
    });

    return (
        <div className="card w-50 "  data-aos="fade-up">
        <div className="card-body">
        <h5 class="card-title text-center">MJESTO DONACIJE</h5>
        <form onSubmit={formik.handleSubmit}>
            <div className="form-group">
                <input
                    className="form-control"
                    id="mjestoDonacije"
                    name="mjestoDonacije"
                    type="text"
                    placeholder="Mjesto donacije"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoDonacije}
                />
            </div>
                {formik.errors.mjestoDonacije && formik.touched.mjestoDonacije ? <p className='error'>{formik.errors.mjestoDonacije}</p> : null}
            
            
             
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Pošalji
                </button>
                {error && <p className='error'>{error}</p>}
            </div>
        </form>
        </div>
        </div>
    )
};

export default MjestoDonacije;