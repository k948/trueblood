import React from 'react';
import { useContext, useEffect, useState } from 'react';
import CurrentUserContext from '../store/CurrentUserContext';
import * as Yup from 'yup'
import { useFormik} from "formik";
import { useNavigate } from "react-router-dom";
import Aos from 'aos';
import 'aos/dist/aos.css';
import FadeLoader from "react-spinners/FadeLoader";



function SlanjeKolicina (){
    const navigate = useNavigate();
    const loginCtx = useContext(CurrentUserContext);
    const [stanje, setStanje] = useState(true)

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const formik = useFormik({
        initialValues:{
            kolicina: ""
        },
        validationSchema: Yup.object({
            kolicina: Yup.number().required("Molimo unesite količinu").test('test1', 'Neispravan unos! Maksimalna količina donirane krvi za muškarce je 5.0 dcl, a za žene 3.5 dcl',function(value){
                if(loginCtx.spol=="M"){
                    return value <= 5 && value > 0
                }
                else{
                    return value <= 3.5 && value > 0
                }
            }) //number
        }),
        onSubmit: (e) => {
            setStanje(false);

            var object={
                kolicina: e.kolicina,
                donorId: sessionStorage.getItem("donorId"),
                lokacija: sessionStorage.getItem("lokacija")
            }

            fetch(process.env.REACT_APP_API + '/djelatnik/provedena-donacija', 
            {
                method: 'POST',
                credentials:'include',
                body: JSON.stringify(object),
                headers: {
                    'Content-Type': 'application/json',  
                }
            })/*.then(function(response){
                if (!response.ok) {   
                    throw Error(response.body);  
            } else {
                return response;
            }}).then(()=>{
                loginCtx.setObavijest({text: 'Doniranje uspješno zabilježeno.', redirectTo: '/djelatnik'});
                navigate('/obavijest');
            })
            .catch(function(error){
                console.log(error)
            });*/

            .then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response;
            }}).then(()=>{
                loginCtx.setObavijest({text: 'Doniranje uspješno zabilježeno.', redirectTo: '/djelatnik'});
                fetch(process.env.REACT_APP_API +'/blood-groups', {    
                    method: 'GET',                        
                    credentials: 'include'
                  }).then(function(response) {
                    if (!response.ok) {
                        throw Error(response.statusText);
                    }
                    return response.json();
                  }).then((data)=>{
                    loginCtx.setThisKGrupe(data);
                  })
                navigate('/obavijest');
            })
            .catch(function(error){
                setStanje(true);
                error.json().then(err => {alert(err.message);})
            });

        }
    });

    if(stanje==false){
        return(
          <div className="back">
            <div className="back-2">
              <FadeLoader color="white" loading={true} size={150}/>
            </div>
          </div>
        )
      }

    return (  
        <div className="card w-50" data-aos="fade-up">
        <div className="card-body">
        <h5 class="card-title text-center">KOLIČINA DONIRANE KRVI</h5>
        <form onSubmit={formik.handleSubmit}>
            <div>
                <input
                className="form-control"
                    id="kolicina"
                    name="kolicina"
                    type="number"
                    placeholder="Količina"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.kolicina}
                />
            </div>
                {formik.errors.kolicina && formik.touched.kolicina ? <p className='error'>{formik.errors.kolicina}</p> : null}
            <br></br>
            <div>   
                 <button  className="btn btn-primary" type="submit">
                    Pošalji
                </button>
            </div>
        </form>
        </div>
        </div>
    )
}

export default SlanjeKolicina;