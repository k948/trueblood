import React, { Component } from 'react'
import { useEffect, useState } from 'react'
import { useNavigate } from "react-router-dom";

export default function Deaktiviraj() {

    const navigate = useNavigate();

    const [object, setObject] = useState(null);
    const [wait, setWait] = useState(false);

    useEffect(() => {                       
        fetch(process.env.REACT_APP_API +'/admin/accounts', {    
            method: 'GET',                        
            credentials: 'include'
          }
        ).then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
          })
        .then((data) => {
            setObject(data);
        })
        .then(()=>{
            setWait(true);
        })
        .catch(function(error) {
          console.log(error);
        })
      }, []);

    
    function deactivateCurrent(korisnikId1){
        var object ={
            korisnikId: korisnikId1
        }

        var formBody = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
        formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        
        fetch(process.env.REACT_APP_API +'/admin/deactivate-account', 
        {
            method: 'POST',
            credentials:'include',
            body: formBody,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'  
            }
        })
        window.location.reload(false);
    }

    if(wait==false){
        return <p className='poruka text-center'>Dohvaćanje aktivnih računa...</p>;
    }

    return (
        <div>    
            {
            object.map((obj) => (
                <div key={obj.id}>
                    <div className='card w-50' data-aos="fade-up">
                    <div className="card-body">
                    <p className='poruka text-center'>{obj.ime+" "+obj.prezime}</p>
                    <br></br>
                    <button  className="btn btn-primary center-btn-2" type="submit" disabled={obj.role=="ADMIN"} onClick={() => deactivateCurrent(obj.id)}>
                        Deaktiviraj
                    </button> 
                    <br></br>
                    </div></div>  
                </div>       
            ))
            }
        </div>
    )
}
