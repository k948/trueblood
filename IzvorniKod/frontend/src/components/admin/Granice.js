import React from 'react';
import { useContext, useState, useEffect } from 'react';
import CurrentUserContext from '../store/CurrentUserContext';
import * as Yup from 'yup'
import { useFormik} from "formik";
import { useNavigate } from "react-router-dom";
import Aos from 'aos';
import 'aos/dist/aos.css';

function Granice (){
    const navigate = useNavigate();
    const loginCtx = useContext(CurrentUserContext);

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const [object, setObject] = useState(null);
    const [wait, setWait] = useState(false);

    useEffect(() => {                       
        fetch(process.env.REACT_APP_API +'/blood-groups', {    
            method: 'GET',                        
            credentials: 'include'
          }
        ).then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
          })
        .then((data) => {
            setObject(data);
        })
        .then(()=>{
            setWait(true);
        })
        .catch(function(error) {
          console.log(error);
        })
      }, []);

    const formik = useFormik({
        initialValues:{
            oznakaKgrupe: "",
            donjaGranica: "",
            gornjaGranica: ""
        },
        validationSchema: Yup.object({
            oznakaKgrupe: Yup.string().required("Molimo unesite oznaku krvne grupe"),
            donjaGranica: Yup.number().required("Molimo unesite donju granicu"),
            gornjaGranica: Yup.number().required("Molimo unesite gornju granicu")
        }),
        onSubmit: (e) => {

            fetch(process.env.REACT_APP_API + '/admin/update-boundary', 
            {
                method: 'POST',
                credentials:'include',
                body: JSON.stringify(e),
                headers: {
                    'Content-Type': 'application/json',  
                }
            })/*.then(function(response){
                if (!response.ok) {   
                    throw Error(response.body);  
                } else {
                return response;
            }}).then(()=>{
                loginCtx.setObavijest({text: 'Granice uspješno promijenjene.', redirectTo: '/admin'});
                navigate('/obavijest');
            })
            .catch(function(error){
                console.log(error);
                alert("Nepostojeća krvna grupa.");
            })*/


            .then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response;
            }}).then(()=>{
                loginCtx.setObavijest({text: 'Granice uspješno promijenjene.', redirectTo: '/admin'});
                fetch(process.env.REACT_APP_API +'/blood-groups', {    
                    method: 'GET',                        
                    credentials: 'include'
                  }).then(function(response) {
                    if (!response.ok) {
                        throw Error(response.statusText);
                    }
                    return response.json();
                  }).then((data)=>{
                    loginCtx.setThisKGrupe(data);
                  })
                navigate('/obavijest');
            })
            .catch(function(error){
                error.json().then(err => {alert(err.message);})
            });
        }
    });

    if(wait==false){
        return <p className='pozdrav text-center'>Dohvaćanje trenutnih granica razina krvi...</p>;
    }

    return (  
        <div className="card w-50" data-aos="fade-up">
            <div className="card-body">
            <h5 class="card-title text-center">PROMJENA GRANICA OPTIMALNIH RAZINA ZALIHA KRVI</h5>
            <h6 class="card-title">TRENUTNE GRANICE</h6>
            <div>
                {
                object.map((obj) => (
                    <div key={obj.oznakaKgrupe}>
                        <p className='labele'>{obj.oznakaKgrupe+" : "+"donja granica: "+obj.donjaGranica+", gornja granica: "+obj.gornjaGranica}</p>  
                    </div>       
                ))
                }
            </div>
            <form onSubmit={formik.handleSubmit}>
                <div>
                    <input
                    className="form-control"
                        id="oznakaKgrupe"
                        name="oznakaKgrupe"
                        type="text"
                        placeholder="Oznaka krvne grupe"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.oznakaKgrupe}
                    />
                </div>
                    {formik.errors.oznakaKgrupe && formik.touched.oznakaKgrupe ? <p className='error'>{formik.errors.oznakaKgrupe}</p> : null}
                <br></br>
                <div>
                    <input
                    className="form-control"
                        id="donjaGranica"
                        name="donjaGranica"
                        type="number"
                        placeholder="Donja granica"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.donjaGranica}
                    />
                </div>
                    {formik.errors.donjaGranica && formik.touched.donjaGranica ? <p className='error'>{formik.errors.donjaGranica}</p> : null}
                <br></br>
                <div>
                    <input
                    className="form-control"
                        id="gornjaGranica"
                        name="gornjaGranica"
                        type="number"
                        placeholder="Gornja granica"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.gornjaGranica}
                    />
                </div>
                    {formik.errors.gornjaGranica && formik.touched.gornjaGranica ? <p className='error'>{formik.errors.gornjaGranica}</p> : null}
                <br></br>
                <div>   
                    <button  className="btn btn-primary" type="submit">
                        Promjeni
                    </button>
                </div>
            </form>
            </div>
        </div>
    )
}

export default Granice;