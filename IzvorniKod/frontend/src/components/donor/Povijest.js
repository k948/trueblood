import React, { Component } from 'react'
import { useEffect, useState, useContext } from 'react'
import { useNavigate } from "react-router-dom";
import CurrentUserContext from '../store/CurrentUserContext';
import Aos from 'aos';
import 'aos/dist/aos.css';

export default function Povijest() {

    const userContext = useContext(CurrentUserContext);
    const navigate = useNavigate();

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const [object, setObject] = useState(null);
    const [wait, setWait] = useState(false);

    var object1 ={
        donorId: sessionStorage.getItem("donorIDDD")  //ovo nije username neg id trebam
    }

    var formBody = [];
    for (var property in object1) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(object1[property]);
    formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    useEffect(() => {                       
        fetch(process.env.REACT_APP_API +'/donor/povijest-darivanja', 
        {
            method: 'POST',
            credentials:'include',
            body: formBody,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'  
            }
        }).then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
          })
        .then((data) => {
            setObject(data);
        })
        .then(()=>{
            setWait(true);
        })
        .catch(function(error) {
          console.log(error);
        })
      }, []);

    
    function getPDFforCurrentDonor(sifraDarivanja1){
        var object ={
            sifraDarivanja: sifraDarivanja1
        }

        var formBody = [];
        for (var property in object) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(object[property]);
        formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        
        fetch(process.env.REACT_APP_API +'/donor/povijest-darivanja/pdf'+"?sifra="+sifraDarivanja1, 
        {
            method: 'GET',
            credentials:'include',
        }).then((response)=>{
            window.open(process.env.REACT_APP_API+ '/donor/povijest-darivanja/pdf?sifra='+sifraDarivanja1, '_blank');
        })
    }

    if(wait==false){
        return <p className='poruka text-center'>Dohvaćanje povijesti darivanja...</p>;
    }

    return (
        <div>    
            {
            object.map((obj) => (
                <div key={obj.sifraDarivanja}>
                    <div className='card w-50' data-aos="fade-up">
                    <div className="card-body">
                    <h5 className='card-title text-center'>{"Datum darivanja: "+obj.datumVrijeme.substring(0,10)}</h5>
                    <br></br>
                    <button  className="btn btn-primary center-btn-2" type="submit" disabled={obj.uspjesno==false} onClick={() => getPDFforCurrentDonor(obj.sifraDarivanja)}>
                        Dohvati PDF
                    </button> 
                    <br></br>
                    </div></div> 

                    
                </div>       
            ))
            }
        </div>
    )
}
