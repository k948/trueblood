import React from 'react'
import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from 'yup'
import CurrentUserContext from "../store/CurrentUserContext";
import { useState, useEffect, useContext} from "react";
import Aos from 'aos';
import 'aos/dist/aos.css';


function DonorPodaci() {
    const navigate = useNavigate();
    const context = useContext(CurrentUserContext);

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const [fetchedDonorPodaci, setFetchedDonorPodaci] = useState({           
        id: "",
        role: "",
        email: "",
        ime: "",
        prezime: "",
        oib: "",
        spol: "Ž",
        mjestoRodenja: "",
        adresaStanovanja: "",
        telefonPrivatni: "",
        telefonPoslovni: "",
        mjestoZaposlenja: "",
        datumRodenja: "" 
    });
    const [loading, setLoading] = useState(true);
    const [showPasswordModifier, setShowPasswordModifier] = useState(false);
    const [backendError, setBackendError] = useState({showError: false, errorMessage:""});

    useEffect(() => {
        fetch(process.env.REACT_APP_API + '/osobni-podaci/pregled/' + context.currentUserId,
            {
                method: 'GET',
                credentials: "include"
            }).then(function(response){
                if (!response.ok) {   
                    throw Error(response.body);  
            } else {
                return response.json();
            }}).then((data)=>{
                Object.keys(data).forEach(key => {
                    if (data[key] == null) data[key] = "";
                })
                setFetchedDonorPodaci(data);
                setLoading(false);
            })
            .catch(function(error){
                console.log(error)
            });
    }, [])

    const formik = useFormik({
        initialValues:{
            id: fetchedDonorPodaci.id,
            role: (fetchedDonorPodaci) ? fetchedDonorPodaci.role.charAt(0) + fetchedDonorPodaci.role.toLowerCase().substring(1) : fetchedDonorPodaci.role,
            email: fetchedDonorPodaci.email,
            ime: fetchedDonorPodaci.ime,
            prezime: fetchedDonorPodaci.prezime,
            oib: fetchedDonorPodaci.oib,
            spol: fetchedDonorPodaci.spol,
            mjestoRodenja: fetchedDonorPodaci.mjestoRodenja,
            adresaStanovanja: fetchedDonorPodaci.adresaStanovanja,
            telefonPrivatni: fetchedDonorPodaci.telefonPrivatni,
            telefonPoslovni: fetchedDonorPodaci.telefonPoslovni,
            mjestoZaposlenja: fetchedDonorPodaci.mjestoZaposlenja,
            datumRodenja: fetchedDonorPodaci.datumRodenja,
            lozinka: "",
            lozinkaConfirm: ""
        },

        enableReinitialize:true,

        validationSchema: Yup.object({
            email: Yup.string().email("Email nije u ispravnom formatu").required("Molimo unesite adresu elektroničke pošte"),
            ime: Yup.string().required("Molimo unesite ime"),
            prezime: Yup.string().required("Molimo unesite prezime"),
            oib: Yup.string().matches(/^\d{11}$/, "Oib mora sadržavati 11 znamenki").required("Molimo unesite oib"),
            //datumRodenja: Yup.date().required("Molimo unesite datum rođenja"),
            lozinkaConfirm: Yup.string().nullable()
                .test('passwords-match', 'Lozinka i ponovljena lozinka moraju biti jednake', function(value) {
                    return this.parent.lozinka === value;
                }),
            mjestoRodenja: Yup.string().nullable()
                .test('not-changed-to-null1', 'Jednom postavljeno mjesto rođenja ne može biti obrisano!', function(value) {
                    if (fetchedDonorPodaci.mjestoRodenja != "") {
                        return (value !== undefined && value !== null && value !== "")
                    } else {
                        return true;
                    }
                }),
            adresaStanovanja: Yup.string().nullable()
                .test('not-changed-to-null2', 'Jednom postavljena adresa stanovanja ne može biti obrisana!', function(value) {
                    if (fetchedDonorPodaci.adresaStanovanja != "") {
                        return (value != undefined && value != null && value != "")
                    } else {
                        return true;
                    }
                }),
            telefonPrivatni: Yup.string().nullable()
                .test('not-changed-to-null3', 'Jednom postavljen telefon privatni ne može biti obrisan!', function(value) {
                    if (fetchedDonorPodaci.telefonPrivatni != "") {
                        return (value != undefined && value != null && value != "")
                    } else {
                        return true;
                    }
                }),
            telefonPoslovni: Yup.string().nullable()
                .test('not-changed-to-null4', 'Jednom postavljen telefon poslovni ne može biti obrisan!', function(value) {
                    if (fetchedDonorPodaci.telefonPoslovni != "") {
                        return (value != undefined && value != null && value != "")
                    } else {
                        return true;
                    }
                }),
            mjestoZaposlenja: Yup.string().nullable()
                .test('not-changed-to-null5', 'Jednom postavljeno mjesto zaposlenja ne može biti obrisano!', function(value) {
                    if (fetchedDonorPodaci.mjestoZaposlenja != "") {
                        return (value != undefined && value != null && value != "")
                    } else {
                        return true;
                    }
                })
        }),

        onSubmit: (e) => {
            const redirDest = '/' + fetchedDonorPodaci.role.toLowerCase();

            delete e.lozinkaConfirm;

            Object.keys(e).forEach(key => {
                if ((e[key] == undefined || e[key] == "") && key != "lozinka") e[key] = null;
            })

            //console.log(formik.values);
            fetch(process.env.REACT_APP_API + '/osobni-podaci/azuriraj',
            {
                method: 'POST',
                credentials: "include",
                body: JSON.stringify(e),
                headers: {
                    'Content-Type': 'application/json' ,
                }   
            }).then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response.json();
            }}).then((data)=>{
                context.setCurrentUserName(data.ime + " " + data.prezime);
                context.setObavijest({text: 'Podaci su uspješno izmijenjeni.', redirectTo: redirDest});
                navigate('/obavijest');
            })
            .catch(function(error){
                error.json().then(err => {setBackendError({showError: true, errorMessage: err.message})})
            });
        }
    });

    //console.log("Evo barem da se jednom ispiše točno: " + fetchedDonorPodaci.ime);

    if (fetchedDonorPodaci==null) {
        return (<p>Učitavanje</p>)
    }

    function showPasswordHandler() {
        //e.preventDefault();

        if (showPasswordModifier) 
        {
            setShowPasswordModifier(false)
        } else {
            setShowPasswordModifier(true)
        }
    }

    return (  
        <div className="card w-75" data-aos="fade-up">
        <div className="card-body">
        <h5 class="card-title text-center">OSOBNI PODACI</h5>
        <form onSubmit={formik.handleSubmit}>
           <div>     
              <input className="form-control"
                  id="id"
                  name="id"
                  type="number"
                  placeholder="Korisnikov ID"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  value={formik.values.id}
                  disabled
              />
          </div>
          <br></br>
            <div>
                <input
                className="form-control"
                    id="ime"
                    name="ime"
                    type="text"
                    placeholder="Ime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.ime}
                />
            </div>
            {formik.errors.ime && formik.touched.ime ? <p className='error'>{formik.errors.ime}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="prezime"
                    name="prezime"
                    type="text"
                    placeholder="Prezime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.prezime}
                />
            </div>
            {formik.errors.prezime && formik.touched.prezime ? <p className='error'>{formik.errors.prezime}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="role"
                    name="role"
                    type="text"
                    placeholder="Uloga"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.role}
                    disabled
                />
            </div>
            <br></br>
            <div>
                <input
                className="form-control"
                    id="oib"
                    name="oib"
                    type="text"
                    placeholder="OIB"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oib}
                />
            </div>
            {formik.errors.oib && formik.touched.oib ? <p className='error'>{formik.errors.oib}</p> : null}
            <br></br>
            <div>
            <p className='labele'>Spol:</p>
                <label className="labele-label">
                    M 
                    <input
                        id="spolM"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="M"}
                        value="M"
                        checked={formik.values.spol == "M"}
                        disabled
                    />
                </label>
                <label className="labele-label">
                    Ž
                    <input
                        id="spolŽ"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="Ž"}
                        value="Ž"
                        checked={formik.values.spol == "Ž"}
                        disabled
                    />
                </label>
            </div>
            <div>
                <input
                className="form-control"
                    id="email"
                    name="email"
                    type="text"
                    placeholder="Email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                />
            </div>
            {formik.errors.email && formik.touched.email ? <p className='error'>{formik.errors.email}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="mjestoRodenja"
                    name="mjestoRodenja"
                    type="text"
                    placeholder="Mjesto rođenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoRodenja}
                />
            </div>
            {formik.errors.mjestoRodenja && formik.touched.mjestoRodenja ? <p className='error'>{formik.errors.mjestoRodenja}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="mjestoZaposlenja"
                    name="mjestoZaposlenja"
                    type="text"
                    placeholder="Mjesto zaposlenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoZaposlenja}
                />
            </div>
            {formik.errors.mjestoZaposlenja && formik.touched.mjestoZaposlenja ? <p className='error'>{formik.errors.mjestoZaposlenja}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="adresaStanovanja"
                    name="adresaStanovanja"
                    type="text"
                    placeholder="Adresa stanovanja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.adresaStanovanja}
                />
            </div>
            {formik.errors.adresaStanovanja && formik.touched.adresaStanovanja ? <p className='error'>{formik.errors.adresaStanovanja}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="telefonPrivatni"
                    name="telefonPrivatni"
                    type="text"
                    placeholder="Privatni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPrivatni}
                />
            </div>
            {formik.errors.telefonPrivatni && formik.touched.telefonPrivatni ? <p className='error'>{formik.errors.telefonPrivatni}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="telefonPoslovni"
                    name="telefonPoslovni"
                    type="text"
                    placeholder="Poslovni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPoslovni}
                />
            </div>
            {formik.errors.telefonPoslovni && formik.touched.telefonPoslovni ? <p className='error'>{formik.errors.telefonPoslovni}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    placeholder="Unesite datum rođenja"
                    id="datumRodenja"
                    name="datumRodenja"
                    type="text"
                    onChange={formik.handleChange}
                    onFocus={
                        (e)=> {
                          e.currentTarget.type = "date";
                          e.currentTarget.focus();
                         }
                       }
                    value={formik.values.datumRodenja}
                    disabled
                />
            </div> 
            {formik.errors.datumRodenja && formik.touched.datumRodenja ? <p className='error'>{formik.errors.datumRodenja}</p> : null}
            {backendError.showError ? <p className='error'>{backendError.errorMessage}</p> : null}
            <br /> 
            <div>
                <button className="btn btn-primary" type="button" onClick={showPasswordHandler}>Želim promijeniti i lozinku</button>
            </div>
            <br />
            {showPasswordModifier ? 
            <div>
                <input
                className="form-control"
                    id="lozinka"
                    name="lozinka"
                    type="password"
                    placeholder="Nova lozinka"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lozinka}
                />
            </div>
            : null}
            {formik.errors.lozinka && formik.touched.lozinka ? <p className='error'>{formik.errors.lozinka}</p> : null}
            <br></br>
            {showPasswordModifier ? 
            <div>
                <input
                className="form-control"
                    id="lozinkaConfirm"
                    name="lozinkaConfirm"
                    type="password"
                    placeholder="Potvrdi novu lozinku"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lozinkaConfirm}
                />
                <br />
            </div>
            : null}
            {formik.errors.lozinkaConfirm && formik.touched.lozinkaConfirm ? <p className='error'>{formik.errors.lozinkaConfirm}</p> : null}
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Izmijeni sve promijenjene podatke
                </button>
            </div>
        </form>
        </div>
        </div>
    )
}

export default DonorPodaci
