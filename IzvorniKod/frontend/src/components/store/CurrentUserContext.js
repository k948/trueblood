import { createContext } from "react";
import { useState } from "react";
import { Navigate, useNavigate } from "react-router";
import { useEffect } from "react";
import { useCallback } from "react";
import { string } from "yup/lib/locale";
import { ErrorMessage } from "formik";

const CurrentUserContext = createContext({
    currentUserName: "",
    userType: "",
    currentUserId:-1,
    login: (userName, password) => {},
    setCurrentUserName: (newCurrentUserName) => {},
    setUserType: (newUserType) =>  {},
    loginError: {success: true, errorMessage:""},
    logout: () => {},
    donorId: "",
    setDonorId: (donorId) => {},
    ime: "",
    prezime: "",
    oib: "",
    spol: "",
    mjestoRodenja:"",
    mjestoZaposlenja: "",
    adresaStanovanja: "",
    telefonPrivatni: "",
    telefonPoslovni: "",
    datumRodenja:"",
    kGrupa:"",
    obavijest: {text: "", redirectTo: ""},
    krvneGrupe: [{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1}],
    donorKgrupa: "",
    error: "",
    trajnaZabranaDarivanja: false,
    setIme: (ime)=>{},
    setPrezime: (prezime)=>{},
    setOib: (oib)=>{},
    setSpol: (spol)=>{},
    setMjestoRodenja: (mjestoRodenja)=>{},
    setMjestoZaposlenja: (mjestoZaposlenja)=>{},
    setAdresaStanovanja: (adresaStanovanja)=>{},
    setTelefonPrivatni: (telefonPrivatni)=>{},
    setTelefonPoslovni: (telefonPoslovni)=>{},
    setDatumRodenja: (datum)=>{},
    setKGrupa: (sadad) => {},
    setObavijest:(obavijest)=>{},
    setCurrentUserId: (currentUserId) => {},
    setThisKGrupe: (asdas) => {},
    setDonorKGrupa: (asdsad) => {},
    setError: (sadasd) => {},
    setTrajnaZabranaDarivanja: (asdasdas) => {}

});

export function CurrentUserContextProvider(props) {
    const navigate = useNavigate();

    const [thisCurrentUserName, setThisCurrentUserName] = useState("");  //ovo koristimo da bismo mogli reci "Pozdrav Pero" kad se prijavi 
    const [thisUserType, setThisUserType] = useState("");
    const [error, setError] = useState({success: true, errorMessage: ""});
    const [thisDonorId, setThisDonorId] = useState("");
    const [thisIme, setThisIme]= useState("");
    const [thisPrezime, setThisPrezime]=useState("");
    const [thisOib, setThisOib]=useState("");
    const [thisSpol, setThisSpol]=useState("");
    const [thisMjestoRodenja, setThisMjestoRodenja]=useState("");
    const [thisMjestoZaposlenja, setThisMjestoZaposlenja]=useState("");
    const [thisAdresaStanovanja, setThisAdresaStanovanja]=useState("");
    const [thisTelefonPrivatni, setThisTelefonPrivatni]=useState("");
    const [thisTelefonPoslovni, setThisTelefonPoslovni]=useState("");
    const [thisDatumRodenja, setThisDatumRodenja]=useState("");
    const [thisKGrupa, setThisKGrupa]=useState("");
    const [thisObavijest, setThisObavijest]=useState({text: "", redirectTo:""});
    const [thisCurrentUserId, setThisCurrentUserId] = useState(-1);
    const [thisKrvneGrupe, setThisKrvneGrupe]=useState([{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1},{oznakaKgrupe: "",zaliha: -1, donjaGranica: -1, gornjaGranica: -1}]);
    const [thisDonorKgrupa, setThisDonorKgrupa]=useState("");
    const [thisError, setThisError]=useState("");
    const [thisTrajnaZabranaDarivanja, setThisTrajnaZabranaDarivanja]=useState(false);


    function logoutUser(){       
        fetch(process.env.REACT_APP_API +'/odjava',{
            method: 'GET',
            credentials: 'include'
        }).then(() =>{
            var resObj = {
                success: true,
                errorMessage: ""
            }
            setError(resObj);
            setThisCurrentUserName("");
            setThisUserType("");
            setThisCurrentUserId(-1);
        })
        sessionStorage.clear(); 
    }

    function loginUser(username1, password1) {
        var resObj = {
            success: true,
            errorMessage: ""
        };

        var loginObject ={
            username: username1,
            password: password1
        }

        var formBody = [];
        for (var property in loginObject) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(loginObject[property]);
        formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        fetch(process.env.REACT_APP_API +'/prijava',  // /prijava
            {
                method: 'POST',
                credentials: 'include',
                body: formBody,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })/*.then(function(response) {
                if (!response.ok) {   
                    alert("Neispravan id ili lozinka!") 
                } else {
                    return response.json();
                }})
                .then((data)=>{
                    setThisUserType(data.korisnik.role) 
                    setThisCurrentUserName(data.korisnik.ime + data.korisnik.prezime)
                    setThisCurrentUserId(data.korisnik.id);
                    if(data.korisnik.role=="DONOR"){
                        sessionStorage.setItem("donorIDDD", data.korisnik.id)
                        setThisDonorId(data.korisnik.id)
                    }
                    navigate("/"+data.korisnik.role.toLowerCase());
                })
                .catch(function(error) {
                    console.log("")
                });*/
                
                .then(function(response){
                    if (!response.ok) {   
                        throw response; 
                } else {
                    return response.json();
                }}).then((data)=>{
                    
                    setThisUserType(data.korisnik.role) 
                    setThisCurrentUserName(data.korisnik.ime + " " + data.korisnik.prezime)
                    setThisCurrentUserId(data.korisnik.id);
                    if(data.korisnik.role=="DONOR"){
                        sessionStorage.setItem("donorIDDD", data.korisnik.id);
                        setThisTrajnaZabranaDarivanja(data.korisnik.zabrana);
                        console.log("zabrana"+data.korisnik.zabrana)
                        setThisDonorId(data.korisnik.id);
                        if(data.korisnik.krvnaGrupa){

                            setThisDonorKgrupa(data.korisnik.krvnaGrupa.oznakaKgrupe);
                        }
                    }
                    navigate("/" + data.korisnik.role.toLowerCase());
                })
                .catch(function(error){
                    error.json().then(err => {setThisError(err.message);})
                    
                });
    }

    const context = {
        currentUserName: thisCurrentUserName,
        userType: thisUserType,
        currentUserId: thisCurrentUserId,
        login: loginUser,
        setUserType: setThisUserType,
        setCurrentUserName: setThisCurrentUserName,
        loginError: error,
        logout: logoutUser,
        donorId: thisDonorId,
        setDonorId: setThisDonorId,
        ime: thisIme,
        prezime: thisPrezime,
        oib: thisOib,
        spol: thisSpol,
        mjestoRodenja:thisMjestoRodenja,
        mjestoZaposlenja: thisMjestoZaposlenja,
        adresaStanovanja: thisAdresaStanovanja,
        telefonPrivatni: thisTelefonPrivatni,
        telefonPoslovni: thisTelefonPoslovni,
        datumRodenja: thisDatumRodenja,
        kGrupa: thisKGrupa,
        obavijest: thisObavijest,
        krvneGrupe: thisKrvneGrupe,
        donorKgrupa: thisDonorKgrupa,
        error: thisError,
        trajnaZabranaDarivanja: thisTrajnaZabranaDarivanja,
        setIme:setThisIme,
        setPrezime: setThisPrezime,
        setOib: setThisOib,
        setSpol: setThisSpol,
        setMjestoRodenja: setThisMjestoRodenja,
        setMjestoZaposlenja: setThisMjestoZaposlenja,
        setAdresaStanovanja: setThisAdresaStanovanja,
        setTelefonPrivatni: setThisTelefonPrivatni,
        setTelefonPoslovni: setThisTelefonPoslovni,
        setDatumRodenja: setThisDatumRodenja,
        setKGrupa: setThisKGrupa,
        setObavijest: setThisObavijest,
        setCurrentUserId: setThisCurrentUserId,
        setThisKGrupe: setThisKrvneGrupe,
        setDonorKGrupa: setThisDonorKgrupa,
        setError: setThisError,
        setTrajnaZabranaDarivanja: setThisTrajnaZabranaDarivanja
    }

    return <CurrentUserContext.Provider value={context}>
            {props.children}
        </CurrentUserContext.Provider>
    
}

export default CurrentUserContext; 