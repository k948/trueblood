import React from 'react';
import { Route, Navigate } from 'react-router-dom';
import { useContext } from 'react';
import CurrentUserContext from './CurrentUserContext';

const PrivateRoute = ({ component: Component, redirectTo, role, path, ...props }) => {
    const ctx = useContext(CurrentUserContext);
    
    if(ctx.userType !== role) {
        return <Navigate to={redirectTo} />;
    }
    return <Route path={path} element={<Component />} />
};

export default PrivateRoute;