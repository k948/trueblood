import React, { useContext, useEffect } from 'react';
import CurrentUserContext from './CurrentUserContext';
import { Navigate } from 'react-router-dom';

function RequireAuth({children, userMustNotBeLoggedIn, loadingStill, permittedUser}) {
    const authContext = useContext(CurrentUserContext);

    useEffect(() => {
        if (!loadingStill) {      
            const finalUrl = "/" + authContext.userType.toLowerCase();

            if (userMustNotBeLoggedIn) {
                authContext.setObavijest({text: "Već ste prijavljeni!", redirectTo: finalUrl});
            } else {
                authContext.setObavijest({text: "Pristup odbijen! Nemate ovlasti za pristup ovoj stranici.", redirectTo: finalUrl});
            }
        }
    }, [loadingStill])

    if (loadingStill) {
        return (<p>Učitavanje...</p>);
    }

    return ((() => {return permittedUser.split(" ").some(user => {return authContext.userType === user})
    })()) ? children : <Navigate to="/obavijest"/>;
}

export default RequireAuth;
