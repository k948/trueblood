import React from 'react'
import { Routes, useNavigate } from 'react-router'
import { useContext } from 'react'
import CurrentUserContext from './CurrentUserContext' 
import { Route } from 'react-router'
import { Navigate } from 'react-router'

export default function ProtectedRoute(props) {
    const ctx = useContext(CurrentUserContext);
    
    if(ctx.userType!=="" && props.role==="NEPRIJAVLJEN"){
        return <Navigate to="/prijavljen"/>
    }
    
    else if(ctx.userType==="" && props.role==="NEPRIJAVLJEN"){
        return <Navigate to="/prijava"/>
    }

    else if(props.role !== ctx.userType){
        return <Navigate to="/zabranjeno"/>
    }

    else return (
        <Route {...props} />)
}
