import React from "react";
import styled from 'styled-components';
import { useContext, useEffect } from 'react';
import Aos from 'aos';
import 'aos/dist/aos.css';



function Footer(){


    return(
        <FooterContainer className="main-footer">
        <div className="footer-middle" >
            <div className="container">
                <div className="row">
                    {/*Column 1*/}
                    <div className="col-md-3 col-sm-6">
                        <h4>Contact us:</h4>
                    
                    </div>
                    {/*Column 1*/}
                    <div className="col-md-3 col-sm-6">
                       
                        <ul className="list-unstyled">
                            <li>&nbsp;<i class="fas fa-map-marker-alt"></i> Zagreb, Croatia</li>
                            
                        </ul>
                    </div>
                    {/*Column 1*/}
                    <div className="col-md-3 col-sm-6">
                        
                        <ul className="list-unstyled">
                            
                            <li>&nbsp;<i class="fas fa-phone"></i> 099 1234567</li>
                            
                        </ul>
                    </div>
                    {/*Column 1*/}
                    <div className="col-md-3 col-sm-6">
                        
                        <ul className="list-unstyled">
                           
                            <li>&nbsp;<i class="far fa-envelope"></i> trueblood@gmail.com</li>
                        </ul>
                    </div>
                </div>
                {/*Footer Bottom*/}
                <div className="footer-bottom">
                <p className="text-xs-center">
                        &copy;{new Date().getFullYear()} True Blood app - All Rights Reserved
                    </p>
                </div>
            </div>
            </div>
        </FooterContainer>
    )
}
export default Footer;

const FooterContainer = styled.footer`

.main-footer{
    position: relative;
    margin-top:auto;
    bottom: 0 ;
    
}

.footer-middle{
    background: var(--mainRed);
    padding-top: 1.5rem;
    color: var(--mainWhite);
    width:100%;
    -webkit-box-shadow: 0 -8px 6px -6px rgb(44, 0, 0);
	   -moz-box-shadow: 0 -8px 6px -6px rgb(44, 0, 0);
	        box-shadow: 0 -8px 6px -6px rgb(44, 0, 0);
}
}

.footer-bottom{
    padding-top: 0rem;
    padding-bottom: 0.5rem;
}

ul li a{
    color: var(--linkWhite);
}

ul li a:hover{
    color: var(--mainPink);
}


`;