import {Link} from 'react-router-dom'
import { useContext, useEffect } from 'react';
import CurrentUserContext from '../store/CurrentUserContext';
import { useNavigate } from 'react-router';
import './MainNavigation.css';
import logo from '../../image2vector.svg';
import Aos from 'aos';
import 'aos/dist/aos.css';



function MainNavigation(){   
    const navContext = useContext(CurrentUserContext);
    const navigate = useNavigate();
    
    function logoutHandler(){
        navContext.logout();
        navigate('/');
    }

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);
    

    var content=null;

    if(navContext.userType === "DONOR") {
        content =             
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/donor/povijest-darivanja'>Povijest darivanja</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/osobni-podaci'>Osobni podaci</Link>
                </li>
                <li className="nav-item ">
                    <button className='btn btn-secundary button-nav' onClick={logoutHandler}>Odjava</button>
                </li>
            </ul>
    } else if (navContext.userType === "DJELATNIK") {
        content =             
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/djelatnik'>Novo doniranje</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/osobni-podaci'>Osobni podaci</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/djelatnik/reg-donor'>Registriraj donora</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/djelatnik/slanje-krvi'>Pošalji krv</Link>
                </li>
                <li className="nav-item">
                    <button className='btn btn-secundary button-nav' onClick={logoutHandler}>Odjava</button>
                </li>
            </ul>
    } else if (navContext.userType === "ADMIN") {
        content =             
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/admin/reg-djelatnik' id = "1">Registriraj djelatnika</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/admin/granice'>Granice krvi</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/admin/racuni'>Korisnički računi</Link>
                </li>
                <li className="nav-item">
                    <button  className='btn btn-secundary button-nav' onClick={logoutHandler}>Odjava</button>
                </li>
            </ul>
    } else { //za neregistriranog posjetitelja
        content =             
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/prijava'>Prijavi se</Link>   
                </li>
                <li className="nav-item">
                    <Link className="nav-link ml-5" to='/registracija'>Registriraj se</Link>
                </li>
            </ul>
    }
    

    return <nav className="navbar navbar-expand-sm navbar-light" data-aos="fade-down">
        <div className="container-fluid">
            <Link className="navbar-brand" to='/'> 
                <div className='brand-div'>
                True Blood <img className="logo-img" src={logo} alt="True Blood" />
                </div>
            </Link>
            
            
            <div className="scroll-wrapper" >
                <div className="scroll-text">
                    {
                        navContext.krvneGrupe.map((grupa) => (
                                grupa.zaliha < grupa.donjaGranica &&
                                <a>Krvna grupa {grupa.oznakaKgrupe} je ispod donje optimalne razine. </a>
                            
                        ))
                    }  
                </div>
            </div>

            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse " id="navbarSupportedContent">
                {content}
            </div>
        </div>
    </nav>



    
        
}

export default MainNavigation;