import MainNavigation from './MainNavigation'
import Footer from './Footer';
import './Layout.css'
import { useContext, useEffect } from 'react';
import Aos from 'aos';
import 'aos/dist/aos.css';


function Layout(props){
    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    return <div className="back-img">
        <MainNavigation/>
        <div className="layout-main-wrap" >
        <main data-aos="fade-up">
            {props.children}
        </main>
        </div>
        <Footer/>
    </div>
}

export default Layout;