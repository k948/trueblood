import { Button } from 'bootstrap'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import { useNavigate } from 'react-router'
import { useContext, useEffect, useState } from 'react'
import CurrentUserContext from '../store/CurrentUserContext';
import Aos from 'aos';
import 'aos/dist/aos.css';

function Login(){
    const loginCtx = useContext(CurrentUserContext)
    
    const navigate = useNavigate();

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const formik = useFormik({
        initialValues:{
            username: "",
            password: ""
        },
        validationSchema: Yup.object({
            username: Yup.string().required("Molimo unesite korisničko ime"),
            password: Yup.string().required("Molimo unesite lozinku")
        }),
        onSubmit: (e) => {
            loginCtx.login(e.username, e.password)          
        }
    });

    return (
        <div className="card w-50 " data-aos="fade-up">
        <div className="card-body">
        <h5 className="card-title text-center">PRIJAVA</h5>
        <form onSubmit={formik.handleSubmit}>
            <div className="form-group">
                <input
                    className="form-control"
                    id="username"
                    name="username"
                    type="text"
                    placeholder="Korisničko ime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.username}
                />
            </div>
                {formik.errors.username && formik.touched.username ? <p className="error">{formik.errors.username}</p> : null}
            
            <div className="form-group">                
                <input
                    className="form-control"
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Lozinka"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.password}
                />
            </div>
                {formik.errors.password && formik.touched.password ? <p className="error">{formik.errors.password}</p> : null} 
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Prijavi se
                </button>
                {loginCtx.error ? <p className='text-danger'>{loginCtx.error}</p> : null}
            </div>
        </form>
        </div>
        </div>
    )
};

export default Login;
//nakon logouta moramo spremiti ctx error na null!!!!!!!!!!!!!!!!!!!!!!