import CurrentUserContext from '../store/CurrentUserContext'
import { useContext, useEffect} from 'react'
import Aos from 'aos';
import 'aos/dist/aos.css';

function LoginBasicAuth(){ //ovo se nigdje ne koristi, to je bila ona opcija da ide posebna prijava za svaku ROLU
    const loginCtx = useContext(CurrentUserContext)
    
    function handleSubmit1(){
        loginCtx.login("donor")
    }

    function handleSubmit2(){
        loginCtx.login("djelatnik")
    }

    function handleSubmit3(){
        loginCtx.login("admin")
    }
    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    
    return (
            <div className="card w-75" data-aos="fade-up">
                <div className="card-body">
                <div className="card-title text-center">Kako se želite prijaviti:</div>
                <button id="donor" name="donor" onClick={handleSubmit1}>Kao donor</button>
                <button id="djelatnik" name="djelatnik" onClick={handleSubmit2}>Kao djelatnik</button>
                <button id="admin" name="admin" onClick={handleSubmit3}>Kao admin</button>
                </div>
            </div>
    )
}

export default LoginBasicAuth;