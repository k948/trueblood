import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from 'yup'
import CurrentUserContext from "../store/CurrentUserContext";
import { useContext , useEffect, useState} from "react";
import Aos from 'aos';
import 'aos/dist/aos.css';
import FadeLoader from "react-spinners/FadeLoader";



function Register (){
    const navigate = useNavigate();
    const context = useContext(CurrentUserContext);
    const [stanje, setStanje] = useState(true)

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const formik = useFormik({
        initialValues:{
            email: "",
            ime: "",
            prezime: "",
            oib: "",
            spol: "Ž",
            mjestoRodenja: "",
            adresaStanovanja: "",
            telefonPrivatni: "",
            telefonPoslovni: "",
            mjestoZaposlenja: "",
            datumRodenja: "" 
        },
        validationSchema: Yup.object({
            email: Yup.string().email("Email nije u ispravnom formatu").required("Molimo unesite adresu elektroničke pošte"),
            ime: Yup.string().required("Molimo unesite ime"),
            prezime: Yup.string().required("Molimo unesite prezime"),
            oib: Yup.string().matches(/^\d{11}$/, "Oib mora sadržavati 11 znamenki").required("Molimo unesite oib"),
            datumRodenja: Yup.date().required("Molimo unesite datum rođenja")
        }),
        onSubmit: (e) => {
            setStanje(false);
            console.log(formik.values);
            fetch(process.env.REACT_APP_API + '/register',  // /registracija
            {
                method: 'POST',
                credentials: "include",
                body: JSON.stringify(e),
                headers: {
                    'Content-Type': 'application/json' ,
                }   
            })/*.then(function(response){
                return response.json();
            }).then((data)=>{
                if(data && data.message){
                    alert(data.message);
                } else {
                    context.setObavijest({text: 'Za dovršetak registracijskog postupka, molimo provjerite vašu elektroničku poštu za daljnje upute.', redirectTo: '/'});
                    navigate('/obavijest');
                }
            });*/

            .then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response;
            }}).then(()=>{
                context.setObavijest({text: 'Za dovršetak registracijskog postupka, molimo provjerite vašu elektroničku poštu za daljnje upute.', redirectTo: '/'});
                navigate('/obavijest');
            })
            .catch(function(error){
                setStanje(true);
                error.json().then(err => {alert(err.message);})
            });
        }
    })
        
    /*function radioHandleChangeM(){
        formik.values.spol="M";
    }

    function radioHandleChangeŽ(){
        formik.values.spol="Ž";
    }*/

    if(stanje==false){
        return(
          <div className="back">
            <div className="back-2">
              <FadeLoader color="white" loading={true} size={150}/>
            </div>
          </div>
        )
      }

    return (  
        <div className="card w-75"  data-aos="fade-up">
        <div className="card-body">
        <h5 class="card-title text-center">REGISTRACIJA</h5>
        <form onSubmit={formik.handleSubmit}>
            <div>
                <input
                className="form-control"
                    id="ime"
                    name="ime"
                    type="text"
                    placeholder="Ime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.ime}
                />
            </div>
            {formik.errors.ime && formik.touched.ime ? <p className="error">{formik.errors.ime}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="prezime"
                    name="prezime"
                    type="text"
                    placeholder="Prezime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.prezime}
                />
            </div>
            {formik.errors.prezime && formik.touched.prezime ? <p className="error">{formik.errors.prezime}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="oib"
                    name="oib"
                    type="text"
                    placeholder="Oib"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oib}
                />
            </div>
            {formik.errors.oib && formik.touched.oib ? <p className="error">{formik.errors.oib}</p> : null}
            <div>
            <p className='labele'>Spol:</p>
                <label className="labele-label">
                    M
                    <input
                        id="spolM"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="M"}
                        value="M"
                    />
                </label>
                <label className="labele-label">
                    Ž
                    <input
                        id="spolŽ"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="Ž"}
                        value="Ž"
                        defaultChecked
                    />
                </label>
            </div>
            <div>
                <input
                className="form-control"
                    id="email"
                    name="email"
                    type="text"
                    placeholder="Email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                />
            </div>
            {formik.errors.email && formik.touched.email ? <p className="error">{formik.errors.email}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="mjestoRodenja"
                    name="mjestoRodenja"
                    type="text"
                    placeholder="Mjesto rođenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoRodenja}
                />
            </div>
            <br></br>
            <div>
                <input
                className="form-control"
                    id="mjestoZaposlenja"
                    name="mjestoZaposlenja"
                    type="text"
                    placeholder="Mjesto zaposlenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoZaposlenja}
                />
            </div> 
            <br></br>
            <div>
                <input
                className="form-control"
                    id="adresaStanovanja"
                    name="adresaStanovanja"
                    type="text"
                    placeholder="Adresa stanovanja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.adresaStanovanja}
                />
            </div>
            <br></br>
            <div>
                <input
                className="form-control"
                    id="telefonPrivatni"
                    name="telefonPrivatni"
                    type="text"
                    placeholder="Privatni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPrivatni}
                />
            </div>
            <br></br>
            <div>
                <input
                className="form-control"
                    id="telefonPoslovni"
                    name="telefonPoslovni"
                    type="text"
                    placeholder="Poslovni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPoslovni}
                />
            </div> 
            <br></br>
            <div>
                <input
                className="form-control"
                    placeholder="Unesite datum rođenja"
                    id="datumRodenja"
                    name="datumRodenja"
                    type="text"
                    onChange={formik.handleChange}
                    onFocus={
                        (e)=> {
                          e.currentTarget.type = "date";
                          e.currentTarget.focus();
                         }
                       }
                    value={formik.values.datumRodenja}
                />
            </div> 
            {formik.errors.datumRodenja && formik.touched.datumRodenja ? <p className="error">{formik.errors.datumRodenja}</p> : null}
            <br></br>
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Registriraj se
                </button>
            </div>
        </form>
        </div>
        </div>
    )
}

export default Register;