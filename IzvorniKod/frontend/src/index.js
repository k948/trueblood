import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {CurrentUserContextProvider} from './components/store/CurrentUserContext'


ReactDOM.render(
    <BrowserRouter>
        <CurrentUserContextProvider>   
            <App/>
        </CurrentUserContextProvider>
    </BrowserRouter>, document.getElementById('root')
);


