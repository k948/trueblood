import React from 'react'
import { useContext } from 'react'
import CurrentUserContext from '../components/store/CurrentUserContext';
import { Link } from 'react-router-dom';

export default function Obavijest() {
    const messageCtx = useContext(CurrentUserContext);

    return (
        <div>
            <div className='card w-50' >
                    <div className="card-body">
                <h3 className='message text-center'>{messageCtx.obavijest.text}</h3>
                    <Link className="btn btn-primary" to={messageCtx.obavijest.redirectTo} >Natrag</Link>
                </div>
            </div>
        </div>
        
    )
}
