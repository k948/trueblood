import React from "react"
import { CDBProgress, CDBCircularProgress, CDBContainer} from "cdbreact";

import "./ProgressBar.css"


const ProgressBar=() => {
  return(
    <CDBContainer>
      <CDBProgress
      value={25}
      max={100}
      min={0}
      text={`${25}%`}
      />
    </CDBContainer>
  );
};

export default ProgressBar;




