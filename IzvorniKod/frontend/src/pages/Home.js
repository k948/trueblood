import ProgressBar from './ProgressBar';
import React from 'react';
import { useState, useContext, useEffect } from 'react';
import CurrentUserContext from "../components/store/CurrentUserContext";
import MainNavigation from "../components/layout/MainNavigation";
import { CDBProgress, CDBCircularProgress, CDBContainer} from "cdbreact";
import Aos from 'aos';
import 'aos/dist/aos.css';
import img1 from '../img/header-right-image-54545.png';
import idimg from '../img/id.jpg';
import documentimg from '../img/document.png';
import finger from '../img/finger-blood.jpg';
import docpat from '../img/doc-pat.jpg';
import bloodgif from '../img/Heart-Article-Hero-1200x500.gif';
import donating from '../img/donating.png';
import lunch from '../img/lunch.jpg';



function Home(){
  
  const navContext = useContext(CurrentUserContext);


    useEffect(()=>{
      Aos.init({duration: 2000});
  }, []);

  return (
    
    <div className="blood-groups" >


    <section id="zasto">
      <div className="container">
        <div className="row">
          <div className="col-md-7 col-sm-7">
            <div className="zasto-lijevo" data-aos="fade-up-right">
              <h1>Zašto donirati krv?</h1>
              <br></br>
              <h7>Krv nije moguće proizvesti na umjetan način. Jedini izvor toga lijeka je čovjek-darivatelj krvi. Svi mi, kada nam zatreba krv kao lijek, ovisni smo samo o dobrovoljnim darivateljima krvi.</h7>
              <br></br>
              <br></br>
              <h7>Kako bi se osiguralo brzo, kvalitetno i sigurno liječenje bolesnika potrebno je uvijek imati dovoljan broj darivatelja krvi, a time i dovoljne količine krvi u pričuvi.</h7>
              <br></br>
              <br></br>
              <h7>Program okupljanja dobrovoljnih darivatelja krvi je socijalni program. U Hrvatskoj, kao i u drugim europskim zemljama, odnos između darivatelja i transfuzijske službe osniva se na partnerstvu između darivatelja krvi, društva/zajednice i transfuzijske službe.</h7>
            </div>
          </div>
          <div className="col-md-5 col-sm-5">
            <div className="zasto-desno" data-aos="fade-up-left">
              <img src={img1}></img>
            </div>
            
          </div>
        </div>
      </div>
    </section>

    <section id="postupak">
      <div className="container">
        <h1>Kako donirati krv?</h1>
        <div className="row">
          <div className="col-md-4 col-sm-4">
            <div className="postupak1" data-aos="fade-up-right">
              <img src={idimg}></img>
              <h5>IDENTIFIKACIJA DARIVATELJA</h5>
              <h7>Kod svakog darivanja krvi s nekim identifikacijskim dokumentom sa slikom (osobna iskaznica, putovnica).</h7>
            </div>
          </div>

          <div className="col-md-4 col-sm-4">
            <div className="postupak2" data-aos="fade-up">
            <img src={documentimg}></img>
              <h5>ISPIS PODATAKA</h5>
              <h7>Ispis podataka na evidencijski karton darivatelja.</h7>

            </div>
          </div>

          <div className="col-md-4 col-sm-4">
          <div className="postupak3" data-aos="fade-up-left">
            <img src={finger}></img>
              <h5>PROVJERA HEMOGLOBINA</h5>
              <h7>Sterilnom lancetom tehničar ubode darivatelja u jagodicu prsta i spušta kap krvi u otopinu bakrenog sulfata. Ako kapljica tone, darivatelj može dati krv.</h7>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-4 col-sm-4">
            <div className="postupak1" data-aos="fade-up-right">
              <img src={docpat}></img>
              <h5>LIJEČNIČKI PREGLED</h5>
              <h7>Razgovor sa liječnikom, mjerenje tlaka, provjeru rada srca itd. Kroz postavljanje određenih pitanja liječnik odlučuje može li osoba darovati krv bez škodljivosti na svoje zdravstveno stanje te bez opasnosti za potencijalne primatelje krvi.</h7>
            </div>
          </div>

          <div className="col-md-4 col-sm-4">
            <div className="postupak2" data-aos="fade-up">
            <img src={donating}></img>
              <h5>VAĐENJE KRVI</h5>
              <h7>Iskusan zdravstveni tehničar odabire venu u lakatnoj jami i bezbolno uvodi iglu u venu.</h7>
              <h7>Vađenje krvi traje 10-ak minuta.</h7>
            </div>
          </div>

          <div className="col-md-4 col-sm-4">
          <div className="postupak3" data-aos="fade-up-left">
            <img src={lunch}></img>
              <h5>BESPLATAN RUČAK</h5>
              <h7>Uživajte u zasluženom obroku. Napunite se enerijom za obnovu krvi u Vašem organizmu.</h7>
            </div>
          </div>
        </div>

      </div>
    </section>

    <section id="zasto">
    
    <div className="container">
        <div className="row">
          <div className="col-md-7 col-sm-7">
            <div className="zasto-lijevo" data-aos="fade-up-right">
              <h1>Trenutno stanje zaliha</h1>
              <br></br>
              <h7>Ako je Vaša krvna grupa ispod minimalne razine (označena crvenim tekstom), dođite nam na doniranje!</h7>
              <br></br>
              <br></br>
              <br></br>
            </div>
          </div>

        </div>

        { 
         navContext.krvneGrupe.map((obj) => (

          <div className="showcase" data-aos="fade-up">
          <h1 className="text-center" style={{"color": obj.zaliha >= obj.donjaGranica ? "#fcb8d2" : "red"}} data-toggle="tooltip" title={obj.zaliha.toString().concat("/100")}>{obj.oznakaKgrupe}</h1>
              <CDBContainer>
                <CDBProgress
                  value={obj.zaliha}
                  max={100}
                  min={0}
                  text={`${obj.zaliha}%`}
                />
              </CDBContainer>
            <br></br>
          </div>
         ))
        }
      </div> 
    </section>
    </div>
    
    );

    
}

export default Home;