import React from 'react'

export default function Prijavljen() {
    return (
        <div className="d-flex justify-content-center ">
            Već ste prijavljeni! Vratite se unatrag.
        </div>
    )
}
