import { useContext } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import SendDonorId from "../components/novoDoniranje/SendDonorId";
import slik from '../img/thank-you-to-our-doctors-and-nurses-2769736-2300094.png'

function Djelatnik() {
    const ctx = useContext(CurrentUserContext);

    return <div>
      <br></br>
        <div className="card w-50" data-aos="fade-up">
        <div className="card-body">
            <div className="card-title text-center">Pozdrav, prijavljeni ste kao {ctx.currentUserName} (djelatnik)!</div>  
            </div></div>
            <br></br>
            <SendDonorId/>
            
            <br></br>

            <section id="thank-you-doc" data-aos="fade-up">
        
        <h1 className="pozdrav text-center">Hvala vam</h1>
       
        <h5 className="pozdrav text-center">Svojim trudom i radom pomažete spasiti nečije živote.</h5>
      

      <div className="ty-desno" data-aos="fade-up">
        <img src={slik}></img>
      </div>

    </section>
    </div>
    
}

export default Djelatnik;