import { useNavigate } from "react-router-dom";
import { useFormik} from "formik";
import React, { useContext, useEffect, useState } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import * as Yup from 'yup'
import '../components/novoDoniranje/Donacija.css'
import Aos from 'aos';
import 'aos/dist/aos.css';
import styled from "styled-components";
import FadeLoader from "react-spinners/FadeLoader";


function DjelatnikRegDonor(){
  
    const [stanje, setStanje] = useState(true)
    const loginCtx = useContext(CurrentUserContext);
    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);
    
    const navigate = useNavigate();
    const formik = useFormik({
        
        initialValues:{

            ime: "",
            prezime: "",
            oib: "",
            spol: "Ž",
            mjestoRodenja: "",
            mjestoZaposlenja: "",
            adresaStanovanja: "",
            telefonPrivatni: "",
            telefonPoslovni: "",
            datumRodenja: "",
            email:"",
            kronicneBolesti: "NE",
            izabraneTeskeBolesti: "NE",
            ovisnost: "NE",
            modnosM: "NE",
            odnosProstucija: "NE",
            promiskuitet: "NE",
            drogaIntravenski: "NE",
            spolnoPrenosiveBolesti: "NE",
            HivPozitivan: "NE",
            seksPartnnerRizSkupine: "NE",
            oznakaKGrupe:""  
        },
        validationSchema: Yup.object({
            ime: Yup.string().required("Molimo unesite ime"),
            prezime: Yup.string().required("Molimo unesite prezime"),
            oib: Yup.string().matches(/^\d{11}$/, "Oib mora sadržavati 11 znamenki").required("Molimo unesite oib"),
            datumRodenja: Yup.date().required("Molimo unesite datum rođenja"),
            oznakaKGrupe:Yup.string().required("Molimo unesite krvnu grupu"),
            email:Yup.string().email("Email nije u ispravnom formatu").required("Molimo unesite adresu elektroničke pošte")
        }),
        onSubmit: (e) => {
            setStanje(false);
            console.log(formik.values);

            var object1={
                ime:e.ime,
                prezime:e.prezime,
                oib:e.oib,
                spol:e.spol,
                email:e.email,
                datumRodenja:e.datumRodenja,
                mjestoRodenja:e.mjestoRodenja,
                mjestoZaposlenja:e.mjestoZaposlenja,
                adresaStanovanja:e.adresaStanovanja,
                telefonPrivatni:e.telefonPrivatni,
                telefonPoslovni:e.telefonPoslovni
            };

            var object3={
                kronicneBolesti: e.kronicneBolesti,
                izabraneTeskeBolesti:e.izabraneTeskeBolesti,
                ovisnost:e.ovisnost,
                modnosM:e.modnosM,
                odnosProstucija:e.odnosProstucija,
                promiskuitet:e.promiskuitet,
                drogaIntravenski:e.drogaIntravenski,
                spolnoPrenosiveBolesti:e.spolnoPrenosiveBolesti,
                hivPozitivan:e.HivPozitivan,
                seksPartnnerRizSkupine:e.seksPartnnerRizSkupine
            }

            var object={
                donorDto:object1,
                trajniZdravstveniPodatciDto:object3,
                oznakaKgrupe:e.oznakaKGrupe
            }


            fetch(process.env.REACT_APP_API + '/djelatnik/register-donor',  
            {
                method: 'POST',
                credentials: "include",
                body: JSON.stringify(object),
                headers: {
                    'Content-Type': 'application/json' ,
                }   
                
            })/*.then(function(response){
                if (response.ok) {   
                    loginCtx.setObavijest({text: 'Donor uspješno registriran.', redirectTo: '/djelatnik'});
                    navigate('/obavijest');  
                }else {
                    return response.json();}
                })
                .then((data)=>{
                    if(data){
                        alert(data.message);
                    }
                });*/ 

                .then(function(response){
                    if (!response.ok) {   
                        throw response;  
                } else {
                    return response;
                }}).then(()=>{
                    loginCtx.setObavijest({text: 'Donor uspješno registriran.', redirectTo: '/djelatnik'});
                    navigate('/obavijest');
                })
                .catch(function(error){
                    setStanje(true);
                    error.json().then(err => {alert(err.message);})
                });
        }
    });

    if(stanje==false){
        return(
          <div className="back">
            <div className="back-2">
              <FadeLoader color="white" loading={true} size={150}/>
            </div>
          </div>
        )
      }
    

    return (
        <div className="card w-75" data-aos="fade-up">
        <div className="card-body">
        <h2 class="card-title text-center">REGISTRACIJA DONORA</h2>
        <form onSubmit={formik.handleSubmit}>
        <h5 class="card-title text-center">OSOBNI PODATCI</h5>
            <div className="form-group">                
                <input
                    className="form-control"
                    id="ime"
                    name="ime"
                    type="text"
                    placeholder="ime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.ime}
                />
            </div>
                {formik.errors.ime && formik.touched.ime ? <p className="error">{formik.errors.ime}</p> : null} 
            <div className="form-group">                
                <input
                    className="form-control"
                    id="prezime"
                    name="prezime"
                    type="text"
                    placeholder="prezime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.prezime}
                />
            </div>
                {formik.errors.prezime && formik.touched.prezime ? <p className="error">{formik.errors.prezime}</p> : null}
            <div className="form-group"> 
                <input
                className="form-control"
                    id="oib"
                    name="oib"
                    type="text"
                    placeholder="Oib"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oib}
                />
            </div>
                {formik.errors.oib && formik.touched.oib ? <p className="error">{formik.errors.oib}</p> : null}
            <div>
            <p className='labele'>Spol:</p>
                <label className="labele-label">
                    M 
                    <input
                        id="spolM"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="M"}
                        value="M"
                    />
                </label>
                <label className="labele-label">
                    Ž
                    <input
                        id="spolŽ"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="Ž"}
                        value="Ž"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
                <input
                className="form-control"
                    id="email"
                    name="email"
                    type="text"
                    placeholder="Email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                />
            </div>
            {formik.errors.email && formik.touched.email ? <p className="error">{formik.errors.email}</p> : null}
            <br></br>
            <div className="form-group">
                <input
                className="form-control"
                    id="mjestoRodenja"
                    name="mjestoRodenja"
                    type="text"
                    placeholder="Mjesto rođenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoRodenja}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="mjestoZaposlenja"
                    name="mjestoZaposlenja"
                    type="text"
                    placeholder="Mjesto zaposlenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoZaposlenja}
                />
            </div> 
            <div className="form-group"> 
                <input
                className="form-control"
                    id="adresaStanovanja"
                    name="adresaStanovanja"
                    type="text"
                    placeholder="Adresa stanovanja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.adresaStanovanja}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="telefonPrivatni"
                    name="telefonPrivatni"
                    type="text"
                    placeholder="Privatni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPrivatni}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="telefonPoslovni"
                    name="telefonPoslovni"
                    type="text"
                    placeholder="Poslovni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPoslovni}
                />
            </div> 
            <div>
                <input   //provjerit zasto ovo ne povlaci iz konteksta
                className="form-control"
                    placeholder="Unesite datum rođenja"
                    id="datumRodenja"
                    name="datumRodenja"
                    type="text"
                    onChange={formik.handleChange}
                    onFocus={
                        (e)=> {
                          e.currentTarget.type = "date";
                          e.currentTarget.focus();
                         }
                       }
                    value={formik.values.datumRodenja}
                />
            </div> 
               {formik.errors.datumRodenja && formik.touched.datumRodenja ? <p className="error">{formik.errors.datumRodenja}</p> : null}
            <br></br>
            <h5 class="card-title text-center">TRAJNI ZDRAVSTVENI PODATCI</h5>
            <div>
            <p className='labele'>Osoba ima kroničnu bolest?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="kronicneBolestiDA"
                        name="kronicneBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.kronicneBolesti="DA"}
                        value="DA"
                        
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="kronicneBolestiNE"
                        name="kronicneBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.kronicneBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba boluje od teških bolesti krvožilnog sustava?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="izabraneTeskeBolestiDA"
                        name="izabraneTeskeBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.izabraneTeskeBolesti="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="izabraneTeskeBolestiNE"
                        name="izabraneTeskeBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.izabraneTeskeBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je ovisnik?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="ovisnostDA"
                        name="ovisnost"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.ovisnost="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="ovisnostNE"
                        name="ovisnost"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.ovisnost="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
              {formik.values.spol=="M" ? (
            <div>
            <p className='labele'>Osoba ima spolne odnose sa drugom osobom istog spola?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="modnosMDA"
                        name="modnosM"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.modnosM="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="modnosMNE"
                        name="modnosM"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.modnosM="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>) : null}
            <div>
            <p className='labele'>Osoba je sudjelovala u spolnom odnosu s prostitutkama?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="odnosProstucijaDA"
                        name="odnosProstucija"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.odnosProstucija="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="odnosProstucijaNE"
                        name="odnosProstucija"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.odnosProstucija="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je živjela/živi promiskuitetno?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="promiskuitetDA"
                        name="promiskuitet"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.promiskuitet="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="promiskuitetNE"
                        name="promiskuitet"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.promiskuitet="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba uzima drogu intraventskim putem?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="drogaIntravenskiDA"
                        name="drogaIntravenski"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.drogaIntravenski="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="drogaIntravenskiNE"
                        name="drogaIntravenski"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.drogaIntravenski="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba ima spolno prenosivu bolest?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="spolnoPrenosiveBolestiDA"
                        name="spolnoPrenosiveBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spolnoPrenosiveBolesti="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="spolnoPrenosiveBolestiNE"
                        name="spolnoPrenosiveBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spolnoPrenosiveBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je HIV pozitivna?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="HivPozitivanDA"
                        name="HivPozitivan"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.HivPozitivan="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="HivPozitivanNE"
                        name="HivPozitivan"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.HivPozitivan="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je seksualni partner gore navedenih osoba?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="seksPartnnerRizSkupineDA"
                        name="seksPartnnerRizSkupine"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.seksPartnnerRizSkupine="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="seksPartnnerRizSkupineNE"
                        name="seksPartnnerRizSkupine"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.seksPartnnerRizSkupine="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div className="form-group" > 
                <input
                    className="form-control"
                    id="oznakaKGrupe"
                    name="oznakaKGrupe"
                    type="string"
                    placeholder="Oznaka krvne grupe"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oznakaKGrupe}
                />
            </div>
            {formik.errors.oznakaKGrupe && formik.touched.oznakaKGrupe ? <p className="error">{formik.errors.oznakaKGrupe}</p> : null} 
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Pošalji
                </button>
            </div>
        </form>
        </div>
        </div>
  
        
    )
   
}


export default DjelatnikRegDonor;