import { useNavigate } from "react-router-dom";
import { useFormik} from "formik";
import React, { useContext, useEffect } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import * as Yup from 'yup'
import '../components/novoDoniranje/Donacija.css'
import Aos from 'aos';
import 'aos/dist/aos.css';


function Donacija(){


    const loginCtx = useContext(CurrentUserContext);

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);
    
    
    const navigate = useNavigate();

    const formik = useFormik({
        
        initialValues:{

            donorId: sessionStorage.getItem("donorId"),
            ime: loginCtx.ime,
            prezime: loginCtx.prezime,
            oib: loginCtx.oib,
            spol: loginCtx.spol,
            mjestoRodenja: loginCtx.mjestoRodenja,
            mjestoZaposlenja: loginCtx.mjestoZaposlenja,
            adresaStanovanja: loginCtx.adresaStanovanja,
            telefonPrivatni: loginCtx.telefonPrivatni,
            telefonPoslovni: loginCtx.telefonPoslovni,
            datumRodenja: loginCtx.datumRodenja,
            tezina: "NE",
            temperatura: "NE",
            tlak: "NE",
            puls: "NE",
            hemoglobin: "NE",
            uzimaLijekove: "NE",
            konzumacijaAlkohola: "NE",
            bolesnoStanje: "NE",
            mensTrudDoj: "NE",
            opasniPoslovi: "NE",
            kronicneBolesti: "NE",
            izabraneTeskeBolesti: "NE",
            ovisnost: "NE",
            modnosM: "NE",
            odnosProstucija: "NE",
            promiskuitet: "NE",
            drogaIntravenski: "NE",
            spolnoPrenosiveBolesti: "NE",
            HivPozitivan: "NE",
            seksPartnnerRizSkupine: "NE",
            lokacija:"",
            oznakaKGrupe: loginCtx.kGrupa
            
        },
        validationSchema: Yup.object({
            ime: Yup.string().required("Molimo unesite ime"),
            prezime: Yup.string().required("Molimo unesite prezime"),
            oib: Yup.string().matches(/^\d{11}$/, "Oib mora sadržavati 11 znamenki").required("Molimo unesite oib"),
            datumRodenja: Yup.date().required("Molimo unesite datum rođenja"),
            lokacija:Yup.string().required("Molimo unesite lokaciju darivanja"),
            oznakaKGrupe:Yup.string().required("Molimo unesite krvnu grupu")
        }),
        onSubmit: (e) => {
            console.log(formik.values);

            sessionStorage.setItem("lokacija", e.lokacija);

            var object1={
                donorId:sessionStorage.getItem("donorId"),
                ime:e.ime,
                prezime:e.prezime,
                oib:e.oib,
                mjestoRodenja:e.mjestoRodenja,
                mjestoZaposlenja:e.mjestoZaposlenja,
                adresaStanovanja:e.adresaStanovanja,
                telefonPrivatni:e.telefonPrivatni,
                telefonPoslovni:e.telefonPoslovni
            };

            var object2={
                tezina: e.tezina,
                temperatura:e.temperatura,
                tlak:e.tlak,
                puls:e.puls,
                hemoglobin:e.hemoglobin,
                uzimaLijekove:e.uzimaLijekove,
                konzumacijaAlkohola:e.konzumacijaAlkohola,
                bolesnoStanje:e.bolesnoStanje,
                mensTrudDoj:e.mensTrudDoj,
                opasniPoslovi:e.opasniPoslovi
            }

            var object3={
                kronicneBolesti: e.kronicneBolesti,
                izabraneTeskeBolesti:e.izabraneTeskeBolesti,
                ovisnost:e.ovisnost,
                modnosM:e.modnosM,
                odnosProstucija:e.odnosProstucija,
                promiskuitet:e.promiskuitet,
                drogaIntravenski:e.drogaIntravenski,
                spolnoPrenosiveBolesti:e.spolnoPrenosiveBolesti,
                hivPozitivan:e.HivPozitivan,
                seksPartnnerRizSkupine:e.seksPartnnerRizSkupine
            }

            var object={
                donacijaDonorDto:object1,
                trenutniZdravstveniPodatciDto:object2,
                trajniZdravstveniPodatciDto:object3,
                lokacija:e.lokacija,
                oznakaKgrupe:e.oznakaKGrupe
            }


            fetch(process.env.REACT_APP_API + '/djelatnik/donacija-podatci',  
            {
                method: 'POST',
                credentials: "include",
                body: JSON.stringify(object),
                headers: {
                    'Content-Type': 'application/json' ,
                }   
                
            })/*.then(function(response){
                if (response.ok) {   
                    navigate('/djelatnik/slanje-kolicina') 
                }  
                else{
                    return response.json();
                }
            }).then((data)=>{
                if(data && data.message){
                    console.log(data.message);  
                    loginCtx.setObavijest({text: data.message, redirectTo: '/djelatnik'});
                    navigate("/obavijest")
                }
            });*/

            .then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response;
            }}).then(()=>{
                navigate('/djelatnik/slanje-kolicina') 
            })
            .catch(function(error){
                error.json().then(err => {loginCtx.setObavijest({text: err.message, redirectTo: '/djelatnik'});})
                navigate("/obavijest")
            });
        }
    });
    

    return (
        <div className="card w-75" data-aos="fade-up">
        <div className="card-body">
        <h2 class="card-title text-center">NOVO DONIRANJE KRVI</h2>
        <form onSubmit={formik.handleSubmit}>
        <h5 class="card-title text-center">OSOBNI PODATCI</h5>
            <div className="form-group" > 
              
                <input
                    disabled
                    className="form-control"
                    id="donorId"
                    name="donorId"
                    type="number"
                    placeholder="donorov ID"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.donorId}
                />
            </div>
            <div className="form-group">                
                <input
                    className="form-control"
                    id="ime"
                    name="ime"
                    type="text"
                    placeholder="ime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.ime}
                />
            </div>
                {formik.errors.ime && formik.touched.ime ? <p className="error">{formik.errors.ime}</p> : null} 
            <div className="form-group">                
                <input
                    className="form-control"
                    id="prezime"
                    name="prezime"
                    type="text"
                    placeholder="prezime"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.prezime}
                />
            </div>
                {formik.errors.prezime && formik.touched.prezime ? <p className="error">{formik.errors.prezime}</p> : null}
            <div className="form-group"> 
                <input
                className="form-control"
                    disabled
                    id="oib"
                    name="oib"
                    type="text"
                    placeholder="Oib"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oib}
                />
            </div>
                {formik.errors.oib && formik.touched.oib ? <p className="error">{formik.errors.oib}</p> : null}
            <div>
            <p className='labele'>Spol:</p>
                <label className="labele-label">
                    M
                    <input
                        id="spolM"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="M"}
                        value="M"
                        checked={formik.values.spol=="M"}
                    />
                </label>
                <label className="labele-label">
                    Ž
                    <input
                        id="spolŽ"
                        name="spol"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spol="Ž"}
                        value="Ž"   
                        checked={formik.values.spol=="Ž"}
                    />
                </label>
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="mjestoRodenja"
                    name="mjestoRodenja"
                    type="text"
                    placeholder="Mjesto rođenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoRodenja}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="mjestoZaposlenja"
                    name="mjestoZaposlenja"
                    type="text"
                    placeholder="Mjesto zaposlenja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.mjestoZaposlenja}
                />
            </div> 
            <div className="form-group"> 
                <input
                className="form-control"
                    id="adresaStanovanja"
                    name="adresaStanovanja"
                    type="text"
                    placeholder="Adresa stanovanja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.adresaStanovanja}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="telefonPrivatni"
                    name="telefonPrivatni"
                    type="text"
                    placeholder="Privatni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPrivatni}
                />
            </div>
            <div className="form-group">
                <input
                className="form-control"
                    id="telefonPoslovni"
                    name="telefonPoslovni"
                    type="text"
                    placeholder="Poslovni broj telefona"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telefonPoslovni}
                />
            </div> 
            <div>
                <input 
                    disabled
                    className="form-control"
                    placeholder="Unesite datum rođenja"
                    id="datumRodenja"
                    name="datumRodenja"
                    type="text"
                    onChange={formik.handleChange}
                    onFocus={
                        (e)=> {
                          e.currentTarget.type = "date";
                          e.currentTarget.focus();
                         }
                       }
                    value={formik.values.datumRodenja}
                />
            </div> 
            <h5 class="card-title text-center">TRENUTNI ZDRAVSTVENI PODATCI</h5>
            <div>
            <p className='labele'>Težina van granica normale?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="tezinaDA"
                        name="tezina"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.tezina="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="tezinaNE"
                        name="tezina"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.tezina="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Temperatura van granica normale?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="temperaturaDA"
                        name="temperatura"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.temperatura="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="temperaturaNE"
                        name="temperatura"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.temperatura="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Tlak van granica normale?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="tlakDA"
                        name="tlak"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.tlak="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="tlakNE"
                        name="tlak"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.tlak="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Puls van granica normale?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="pulsDA"
                        name="puls"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.puls="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="pulsNE"
                        name="puls"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.puls="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Hemoglobin van granica normale?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="hemoglobinDA"
                        name="hemoglobin"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.hemoglobin="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="nemoglobinNE"
                        name="hemoglobin"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.hemoglobin="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba uzima ljekove?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="uzimaLijekoveDA"
                        name="uzimaLijekove"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.uzimaLijekove="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="uzimaLijekoveNE"
                        name="uzimaLijekove"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.uzimaLijekove="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je konzumirala alkohol unutar protekla 24h?</p>
                <label className="labele-label"> 
                    DA 
                    <input
                        id="konzumacijaAlkoholaDA"
                        name="konzumacijaAlkohola"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.konzumacijaAlkohola="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="konzumacijaAlkoholaNE"
                        name="konzumacijaAlkohola"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.konzumacijaAlkohola="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je trenutno bolesna?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="bolesnoStanjeDA"
                        name="bolesnoStanje"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.bolesnoStanje="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="bolesnoStanjeNE"
                        name="bolesnoStanje"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.bolesnoStanje="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            {formik.values.spol == "Ž" ? (
            <div>
            <p className='labele'>Osoba ima menstruaciju, trudna je ili doji?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="mendTrudDojDA"
                        name="mensTrudDoj"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.mensTrudDoj="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="mensTrudDojNE"
                        name="mensTrudDoj"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.mensTrudDoj="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            )
            : null}
            <div>
            <p className='labele'>Osoba danas treba obavljati opasne poslove?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="opasniPosloviDA"
                        name="opasniPoslovi"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.opasniPoslovi="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="opasniPosloviNE"
                        name="opasniPoslovi"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.opasniPoslovi="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <h5 class="card-title text-center">TRAJNI ZDRAVSTVENI PODATCI</h5>
            <div>
            <p className='labele'>Osoba ima kroničnu bolest?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="kronicneBolestiDA"
                        name="kronicneBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.kronicneBolesti="DA"}
                        value="DA"
                        
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="kronicneBolestiNE"
                        name="kronicneBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.kronicneBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba boluje od teških bolesti krvožilnog sustava?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="izabraneTeskeBolestiDA"
                        name="izabraneTeskeBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.izabraneTeskeBolesti="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="izabraneTeskeBolestiNE"
                        name="izabraneTeskeBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.izabraneTeskeBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je ovisnik?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="ovisnostDA"
                        name="ovisnost"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.ovisnost="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="ovisnostNE"
                        name="ovisnost"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.ovisnost="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
              {formik.values.spol=="M" ? (
            <div>
            <p className='labele'>Osoba ima spolne odnose sa drugom osobom istog spola?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="modnosMDA"
                        name="modnosM"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.modnosM="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="modnosMNE"
                        name="modnosM"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.modnosM="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>) : null}
            <div>
            <p className='labele'>Osoba je sudjelovala u spolnom odnosu s prostitutkama?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="odnosProstucijaDA"
                        name="odnosProstucija"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.odnosProstucija="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="odnosProstucijaNE"
                        name="odnosProstucija"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.odnosProstucija="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je živjela/živi promiskuitetno?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="promiskuitetDA"
                        name="promiskuitet"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.promiskuitet="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="promiskuitetNE"
                        name="promiskuitet"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.promiskuitet="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba uzima drogu intraventskim putem?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="drogaIntravenskiDA"
                        name="drogaIntravenski"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.drogaIntravenski="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="drogaIntravenskiNE"
                        name="drogaIntravenski"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.drogaIntravenski="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba ima spolno prenosivu bolest?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="spolnoPrenosiveBolestiDA"
                        name="spolnoPrenosiveBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spolnoPrenosiveBolesti="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="spolnoPrenosiveBolestiNE"
                        name="spolnoPrenosiveBolesti"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.spolnoPrenosiveBolesti="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je HIV pozitivna?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="HivPozitivanDA"
                        name="HivPozitivan"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.HivPozitivan="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="HivPozitivanNE"
                        name="HivPozitivan"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.HivPozitivan="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div>
            <p className='labele'>Osoba je seksualni partner gore navedenih osoba?</p>
                <label className="labele-label">
                    DA 
                    <input
                        id="seksPartnnerRizSkupineDA"
                        name="seksPartnnerRizSkupine"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.seksPartnnerRizSkupine="DA"}
                        value="DA"
                    />
                </label>
                <label className="labele-label">
                    NE
                    <input
                        id="seksPartnnerRizSkupineNE"
                        name="seksPartnnerRizSkupine"
                        type="radio"
                        onChange={formik.handleChange}
                        onClick={() => formik.values.seksPartnnerRizSkupine="NE"}
                        value="NE"   
                        defaultChecked
                    />
                </label>
            </div>
            <div className="form-group" > 
                <input
                    className="form-control"
                    id="lokacija"
                    name="lokacija"
                    type="text"
                    placeholder="Lokacija darivanja"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lokacija}
                />
            </div>
            {formik.errors.lokacija && formik.touched.lokacija ? <p className="error">{formik.errors.lokacija}</p> : null} 
            <div className="form-group" > 
                <input
                    disabled={loginCtx.kGrupa}
                    className="form-control"
                    id="oznakaKGrupe"
                    name="oznakaKGrupe"
                    type="text"
                    placeholder="Oznaka krvne grupe"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.oznakaKGrupe}
                />
            </div>
            {formik.errors.oznakaKGrupe && formik.touched.oznakaKGrupe ? <p className="error">{formik.errors.oznakaKGrupe}</p> : null} 
            <div>   
                 <button className="btn btn-primary" type="submit">
                    Pošalji
                </button>
            </div>
        </form>
        </div>
        </div>
  
        
    )
   
}
    



export default Donacija;