import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { useContext, useEffect } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import * as Yup from 'yup'
import Aos from 'aos';
import 'aos/dist/aos.css';

function RegFinish (){
    const navigate = useNavigate();
    const loginCtx = useContext(CurrentUserContext)

    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);


    const formik = useFormik({
        initialValues:{
            lozinka: "",
            ponovljenaLozinka: ""
        },
        validationSchema: Yup.object({
            lozinka: Yup.string().required("Molimo unesite lozinku"),
            ponovljenaLozinka: Yup.string()
                .test('passwords-match', 'Lozinka i ponovljena lozinka moraju biti jednake', function(value) {
                    return this.parent.lozinka === value;
                })
        }),
        onSubmit: (e) => {
            const fetchURL = process.env.REACT_APP_API + '/register/confirm-account/res' + window.location.search;
            var pass = {lozinka: formik.values.lozinka}
            
            fetch(fetchURL,  // /registracija
            {
                method: 'POST',
                credentials:'include',
                body: JSON.stringify(pass),
                headers: {
                    'Content-Type': 'application/json'  
                }    //handleanje errora  ---PITANJEEEEEEEEEEEEEEE
            }).then(() => {
                navigate('/prijava')
            })
        }
    });

    return (  //ovdje ide unos dvije lozinke 
        <div className="card w-75" data-aos="fade-up">
        <div className="card-body">
        <h5 class="card-title text-center">DOVRŠETAK REGISTRACIJE</h5>
        <form onSubmit={formik.handleSubmit}>
            <div>
                <input
                className="form-control"
                    id="lozinka"
                    name="lozinka"
                    type="password"
                    placeholder="Lozinka"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.lozinka}
                />
            </div>
                {formik.errors.lozinka && formik.touched.lozinka ? <p className="error">{formik.errors.lozinka}</p> : null}
            <br></br>
            <div>
                <input
                className="form-control"
                    id="ponovljenaLozinka"
                    name="ponovljenaLozinka"
                    type="password"
                    placeholder="Ponovljena lozinka"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.ponovljenaLozinka}
                />
            </div>
            <br></br>
            <div>   
                 <button  className="btn btn-primary" type="submit">
                    Registriraj se
                </button>
                {formik.errors.ponovljenaLozinka && formik.touched.ponovljenaLozinka ? <p className="error">{formik.errors.ponovljenaLozinka}</p> : null}
            </div>
        </form>
        </div>
        </div>
    )
}

export default RegFinish;