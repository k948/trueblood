import { useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { useContext, useEffect } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import * as Yup from 'yup'
import Aos from 'aos';
import 'aos/dist/aos.css';


function SlanjeKrvi (){
    const navigate = useNavigate();
    const loginCtx = useContext(CurrentUserContext)
    useEffect(()=>{
        Aos.init({duration: 2000});
    }, []);

    const formik = useFormik({
        initialValues:{
            oznakaKgrupe: "",
            kolicina: ""
        },
        validationSchema: Yup.object({
            oznakaKgrupe: Yup.string().required("Molimo unesite oznaku krvne grupe"),
            kolicina: Yup.string().required("Molimo unesite količinu"),
        }),
        onSubmit: (e) => {
            
            fetch(process.env.REACT_APP_API + '/djelatnik/transfer-blood', 
            {
                method: 'POST',
                credentials:'include',
                body: JSON.stringify(e),
                headers: {
                    'Content-Type': 'application/json'  
                } 
            })/*.then(function(response){
                if (response.ok) {   
                    loginCtx.setObavijest({text: 'Krv uspješno poslana.', redirectTo: '/djelatnik'});
                    navigate('/obavijest');  
                }else {
                    return response.json();}
                }).then((data)=>{
                    if(data){
                        alert(data.message);
                    }
            });*/

            .then(function(response){
                if (!response.ok) {   
                    throw response;  
            } else {
                return response;
            }}).then(()=>{
                loginCtx.setObavijest({text: 'Krv uspješno poslana.', redirectTo: '/djelatnik'});
                fetch(process.env.REACT_APP_API +'/blood-groups', {    
                    method: 'GET',                        
                    credentials: 'include'
                  }).then(function(response) {
                    if (!response.ok) {
                        throw Error(response.statusText);
                    }
                    return response.json();
                  }).then((data)=>{
                    loginCtx.setThisKGrupe(data);
                  })
                navigate('/obavijest'); 
            })
            .catch(function(error){
                error.json().then(err => {alert(err.message);})
            });

            
        }
    });

    return (  //ovdje ide unos dvije lozinke 
        <div className="card w-75" data-aos="fade-up">
            <div className="card-body">
            <h5 class="card-title text-center">SLANJE KRVI U VANJSKU JEDINICU</h5>
            <form onSubmit={formik.handleSubmit}>
                <div>
                    <input
                    className="form-control"
                        id="oznakaKgrupe"
                        name="oznakaKgrupe"
                        type="text"
                        placeholder="Oznaka krvne grupe"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.oznakaKgrupe}
                    />
                </div>
                    {formik.errors.oznakaKgrupe && formik.touched.oznakaKgrupe ? <p className="error">{formik.errors.oznakaKgrupe}</p> : null}
                <br></br>
                <div>
                    <input
                    className="form-control"
                        id="kolicina"
                        name="kolicina"
                        type="text"
                        placeholder="Količina"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.kolicina}
                    />
                </div>
                {formik.errors.kolicina && formik.touched.kolicina ? <p className="error">{formik.errors.kolicina}</p> : null}
                <br></br>
                <div>   
                    <button  className="btn btn-primary" type="submit">
                        Pošalji
                    </button>
                </div>
            </form>
            </div>
        </div>
    )
}

export default SlanjeKrvi;