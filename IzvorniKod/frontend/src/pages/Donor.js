import { useContext,useEffect } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import { CDBProgress, CDBCircularProgress, CDBContainer} from "cdbreact";
import Aos from 'aos';
import 'aos/dist/aos.css';
import img1 from '../img/header-right-image-54545.png';
import idimg from '../img/id.jpg';
import documentimg from '../img/document.png';
import finger from '../img/finger-blood.jpg';
import docpat from '../img/doc-pat.jpg';
import bloodgif from '../img/Heart-Article-Hero-1200x500.gif';
import donating from '../img/donating.png';
import lunch from '../img/lunch.jpg';
import ProgressBar from "./ProgressBar"

function Donor() {
    const ctx = useContext(CurrentUserContext);
    const navContext = useContext(CurrentUserContext);
    var dovoljneZalihe=false;
    var vrijednost=0;
    var poruka="";


    for ( var grupa of navContext.krvneGrupe){
      if (ctx.donorKgrupa===grupa.oznakaKgrupe){
        if (grupa.zaliha <grupa.donjaGranica){
          dovoljneZalihe=false;
        }
        else if (grupa.zaliha>grupa.donjaGranica || grupa.zaliha<grupa.gornjaGranica){
          dovoljneZalihe=true;
        }

        if (grupa.zaliha<grupa.donjaGranica){
          poruka="Stanje zaliha Vaše krvne grupe je ispod donje optimalne granice."
        }
        else if (grupa.zaliha>grupa.donjaGranica && grupa.zaliha<grupa.gornjaGranica){
          poruka="Stanje zaliha Vaše krvne grupe je unutar optimalnih granica."
        }
        else if (grupa.zaliha>grupa.gornjaGranica){
          poruka="Stanje zaliha Vaše krvne grupe je iznad gornje optimalne granice."
        }
        var postotak=grupa.zaliha
        postotak=Math.trunc(postotak)
        var hoverValue = postotak.toString().concat("/100")
        vrijednost=postotak

      }
    }
    return( <div className="blood-groups" >
      <div className="card w-50" data-aos="fade-up">
        <div className="card-body">
    <div className="card-title text-center ">Pozdrav, prijavljeni ste kao {ctx.currentUserName}!</div>
    </div></div>
        {ctx.donorKgrupa && ctx.trajnaZabranaDarivanja!=true ?
        <div>
        <h2 className="text-center" data-aos="zoom-in-up">Stanje zaliha Vaše krvne grupe:</h2>
        <div className="middle" data-aos="fade-up">
            <div className="col-sm">
            <h1 className="text-center"style={{"color": dovoljneZalihe ? "#fcb8d2" : "red"}} data-toggle="tooltip" title={hoverValue}>{ctx.donorKgrupa} </h1>
            <CDBContainer>
              <CDBProgress
                value={vrijednost}
                max={100}
                min={0}
                text={`${vrijednost}%`}
              />
            </CDBContainer>
            <br></br>          
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-7 col-sm-7">
                <div className="zasto-lijevo">
                  <h7 className="text-center">{poruka}</h7>
                </div>
              </div>
            </div>
          </div>
        </div>


    
        <section id="zasto">
          <div className="container">
            <div className="row">
              <div className="col-md-7 col-sm-7">
                <div className="zasto-lijevo" data-aos="fade-up-right">
                  <h1>Zašto donirati krv?</h1>
                  <br></br>
                  <h7>Krv nije moguće proizvesti na umjetan način. Jedini izvor toga lijeka je čovjek-darivatelj krvi. Svi mi, kada nam zatreba krv kao lijek, ovisni smo samo o dobrovoljnim darivateljima krvi.</h7>
                  <br></br>
                  <br></br>
                  <h7>Kako bi se osiguralo brzo, kvalitetno i sigurno liječenje bolesnika potrebno je uvijek imati dovoljan broj darivatelja krvi, a time i dovoljne količine krvi u pričuvi.</h7>
                  <br></br>
                  <br></br>
                  <h7>Program okupljanja dobrovoljnih darivatelja krvi je socijalni program. U Hrvatskoj, kao i u drugim europskim zemljama, odnos između darivatelja i transfuzijske službe osniva se na partnerstvu između darivatelja krvi, društva/zajednice i transfuzijske službe.</h7>
                </div>
              </div>
              <div className="col-md-5 col-sm-5">
                <div className="zasto-desno" data-aos="fade-up-left">
                  <img src={img1}></img>
                </div>
                
              </div>
            </div>
          </div>
        </section>
    
        <section id="postupak">
          <div className="container">
            <h1 data-aos="fade-right">Kako donirati krv?</h1>
            <div className="row">
              <div className="col-md-4 col-sm-4">
                <div className="postupak1" data-aos="fade-up-right">
                  <img src={idimg}></img>
                  <h5>IDENTIFIKACIJA DARIVATELJA</h5>
                  <h7>Kod svakog darivanja krvi sa nekim identifikacijskim dokumentom sa slikom (osobna iskaznica, putovnica).</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
                <div className="postupak2" data-aos="fade-up">
                <img src={documentimg}></img>
                  <h5>ISPIS PODATAKA</h5>
                  <h7>Ispis podataka na evidencijski karton darivatelja.</h7>
    
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
              <div className="postupak3" data-aos="fade-up-left">
                <img src={finger}></img>
                  <h5>PROVJERA HEMOGLOBINA</h5>
                  <h7>Sterilnom lancetom tehničar ubode darivatelja u jagodicu prsta i spušta kap krvi u otopinu bakrenog sulfata. Kada kapljica tone, darivatelj može dati krv.</h7>
                </div>
              </div>
            </div>
    
            <div className="row">
              <div className="col-md-4 col-sm-4">
                <div className="postupak1" data-aos="fade-up-right">
                  <img src={docpat}></img>
                  <h5>LIJEČNIČKI PREGLED</h5>
                  <h7>Razgovor sa liječnikom, mjerenje tlaka, provjeru rada srca itd. Kroz postavljanje određenih pitanja liječnik odlučuje da li osoba može darovati krv bez škodljivosti za svoje zdravstveno stanje te bez opasnosti za potencijalne primatelje krvi.</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
                <div className="postupak2" data-aos="fade-up">
                <img src={donating}></img>
                  <h5>VAĐENJE KRVI</h5>
                  <h7>Iskusan zdravstveni tehničar odabire venu u lakatnoj jami i bezbolno uvodi iglu u venu.</h7>
                  <h7> Vađenje krvi traje 10-tak minuta.</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
              <div className="postupak3" data-aos="fade-up-left">
                <img src={lunch}></img>
                  <h5>BESPLATAN RUČAK</h5>
                  <h7>Uživajte u zasluženom obroku. Napunite se enerijom za obnovu krvi u Vašemu organizmu.</h7>
                </div>
              </div>
            </div>
    
          </div>
        </section></div> : <div> <section id="zasto">
          <div className="container">
            <div className="row">
              <div className="col-md-7 col-sm-7">
                <div className="zasto-lijevo" data-aos="fade-up-right">
                  <h1>Zašto donirati krv?</h1>
                  <br></br>
                  <h7>Krv nije moguće proizvesti na umjetan način. Jedini izvor toga lijeka je čovjek-darivatelj krvi. Svi mi, kada nam zatreba krv kao lijek, ovisni smo samo o dobrovoljnim darivateljima krvi.</h7>
                  <br></br>
                  <br></br>
                  <h7>Kako bi se osiguralo brzo, kvalitetno i sigurno liječenje bolesnika potrebno je uvijek imati dovoljan broj darivatelja krvi, a time i dovoljne količine krvi u pričuvi.</h7>
                  <br></br>
                  <br></br>
                  <h7>Program okupljanja dobrovoljnih darivatelja krvi je socijalni program. U Hrvatskoj, kao i u drugim europskim zemljama, odnos između darivatelja i transfuzijske službe osniva se na partnerstvu između darivatelja krvi, društva/zajednice i transfuzijske službe.</h7>
                </div>
              </div>
              <div className="col-md-5 col-sm-5">
                <div className="zasto-desno" data-aos="fade-up-left">
                  <img src={img1}></img>
                </div>
                
              </div>
            </div>
          </div>
        </section>
    
        <section id="postupak">
          <div className="container">
            <h1 data-aos="fade-right">Kako donirati krv?</h1>
            <div className="row">
              <div className="col-md-4 col-sm-4">
                <div className="postupak1" data-aos="fade-up-right">
                  <img src={idimg}></img>
                  <h5>IDENTIFIKACIJA DARIVATELJA</h5>
                  <h7>Kod svakog darivanja krvi sa nekim identifikacijskim dokumentom sa slikom (osobna iskaznica, putovnica).</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
                <div className="postupak2" data-aos="fade-up">
                <img src={documentimg}></img>
                  <h5>ISPIS PODATAKA</h5>
                  <h7>Ispis podataka na evidencijski karton darivatelja.</h7>
    
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
              <div className="postupak3" data-aos="fade-up-left">
                <img src={finger}></img>
                  <h5>PROVJERA HEMOGLOBINA</h5>
                  <h7>Sterilnom lancetom tehničar ubode darivatelja u jagodicu prsta i spušta kap krvi u otopinu bakrenog sulfata. Kada kapljica tone, darivatelj može dati krv.</h7>
                </div>
              </div>
            </div>
    
            <div className="row">
              <div className="col-md-4 col-sm-4">
                <div className="postupak1" data-aos="fade-up-right">
                  <img src={docpat}></img>
                  <h5>LIJEČNIČKI PREGLED</h5>
                  <h7>Razgovor sa liječnikom, mjerenje tlaka, provjeru rada srca itd. Kroz postavljanje određenih pitanja liječnik odlučuje da li osoba može darovati krv bez škodljivosti za svoje zdravstveno stanje te bez opasnosti za potencijalne primatelje krvi.</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
                <div className="postupak2" data-aos="fade-up">
                <img src={donating}></img>
                  <h5>VAĐENJE KRVI</h5>
                  <h7>Iskusan zdravstveni tehničar odabire venu u lakatnoj jami i bezbolno uvodi iglu u venu.</h7>
                  <h7> Vađenje krvi traje 10-tak minuta.</h7>
                </div>
              </div>
    
              <div className="col-md-4 col-sm-4">
              <div className="postupak3" data-aos="fade-up-left">
                <img src={lunch}></img>
                  <h5>BESPLATAN RUČAK</h5>
                  <h7>Uživajte u zasluženom obroku. Napunite se enerijom za obnovu krvi u Vašemu organizmu.</h7>
                </div>
              </div>
            </div>
    
          </div>
        </section> </div>}
      </div>
        );
    
}

export default Donor;