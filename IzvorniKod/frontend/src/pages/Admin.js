import { useContext } from "react";
import CurrentUserContext from "../components/store/CurrentUserContext";
import slikica from '../img/Heart-Article-Hero-1200x500.gif'

function Admin() {
    const ctx = useContext(CurrentUserContext);

    return<div>
       <br></br>
        <div className="card w-50" data-aos="fade-up">
        <div className="card-body">
    <div className="card-title text-center">Pozdrav, prijavljeni ste kao {ctx.currentUserName} (administrator)!</div>
    </div></div>
    <section id="thank-you" data-aos="fade-up">
        
              <h1 className="pozdrav text-center">Hvala vam</h1>
              <br></br>
              <h5 className="pozdrav text-center">Svojim trudom i radom pomažete spasiti nečije živote.</h5>
            

            <div className="ty-desno" data-aos="fade-up">
              <img src={slikica}></img>
            </div>

      
    </section>
    <br></br>

    </div>
}

export default Admin;