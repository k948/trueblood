import React from 'react'

export default function Zabranjeno() {
    return (
        <div className="d-flex justify-content-center ">
            <h1>
                Pristup odbijen!
            </h1>
            <p>
                Nemate ovlasti za pristup ovoj stranici.
                Vratite se unatrag.
            </p>
        </div>
    )
}
